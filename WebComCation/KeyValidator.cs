﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace WebComCation
{
    public partial class KeyValidator : Form
    {
        public static bool IsClose;
        Registry_Handler reg = new Registry_Handler();
        ServerCommunicator web = new ServerCommunicator();
        ServerOutInMsg regmsg = new ServerOutInMsg();
        ServerOutInMsg srvrmsg = new ServerOutInMsg();
        string jsonQuery = "";
        keyRequest req;
        public KeyValidator()
        {
            InitializeComponent();
            //

           // regmsg = reg.GetStoredRegistryMsg();
           // srvrmsg = web.SendAndGetResponse();
            jsonQuery = reg.RegToJsonRawMsgToSend(regmsg);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = "";
            if (txtLicKey.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please provide Activation key", "GSPORT");
                return;
            }
            textBox1.Clear();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" +keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + txtLicKey.Text + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                webRequest.Method = "POST";                
                webRequest.ContentType = "application/json";               
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try { 
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            System.Windows.Forms.MessageBox.Show("This PC is not connected with internet");
                            return ;
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();


                #region License_Expired_Case
                if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);
                    MessageBox.Show("Activation Key Expired");//+ p1);
                    textBox1.Text = "Activation Key Expired";//+ p1;
                    return;
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid"))
                {

                   string holdrenewMsg= LicenseValidator.GetRenewedComputerKey("", "");

                    MessageBox.Show("Invalid Activation Key");
                    //Application.Exit();
                    textBox1.Text = "Invalid Activation Key";
                    return;
                }
                #endregion


                #region Valid_License
                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");

                string CPK = sw;
                string strrm = sw;
                CPK = CPK.Replace("Status-ComputerKey ", "");
                CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");

                #region Store_Computer_Key_Locally

                //Keys.ReadKey();

                //QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //Keys.ActivationKey = txtLicKey.Text.Trim();
                //Keys.ComputerKey = CPK;
                //=========================
                LicenseStatus st = LicenseStatus.Unknown;
                //Fetch locally stored values first
                string LicenseKey = txtLicKey.Text; // Registry_Handler.GetActivationKeyFromReg();
                string ComputerKey = CPK;//Registry_Handler.GetComputerKeyFromReg();
                string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                string ak = LicenseKey; //Validation Key
                string ck = ComputerKey; // Computer Key
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeys(ak, ck);
                //=================
                //Registry_Handler.SaveComputerKeyToRegistry(CPK);
                //Registry_Handler.SaveActivationKeyToRegistry(txtLicKey.Text.Trim());
                //Keys.StoreAKey();
                //Keys.ReadKey();
                //Registry_Handler.SaveCurrentRunDateToReg();
                #endregion


                #region Not_In_Use
                //if (strrm.Contains("invalid"))
                //{
                //    //keyStatus = LicenseStatus.Invalid;
                //    strrm = strrm.Replace("invalid", "");
                //    System.Windows.Forms.MessageBox.Show("Key is Invalid");
                //    textBox1.Text = "Key is Invalid";
                //    return;
                //}
                //else if (strrm.Contains("valid"))
                //{
                //keyStatus = LicenseStatus.Valid;
                //msg.Status = LicenseStatus.Valid.ToString();
                //msg.keyStatus = LicenseStatus.Valid.ToString();
                //msg.ServerMessage = "License Key is valid";
                //strrm = strrm.Replace("valid", "");
                //                }
                //                //System.Windows.Forms.MessageBox.Show(strrm);
                //                //ServerOutInMsg msg = new ServerOutInMsg();

                //                try
                //                {
                //                    //msg.ActivationDate = actdt;//dcValuePairs["ActivationDate"];
                //                    //msg.ComputerID = CPK;//dcValuePairs["ComputerID"];
                //                    //msg.ComputerName = cpname;//dcValuePairs["ComputerName"];
                //                    //msg.EndDate = eddt; //dcValuePairs["EndDate"];
                //                    //msg.LicenseKey = txtLicKey.Text;// dcValuePairs["LicenseKey"];
                //                    //msg.ServerMessage = strrm;//dcValuePairs["Message"];
                //                    //msg.StartDate = Stdt;//dcValuePairs["StartDate"];
                //                    //msg.Status = keyStatus.ToString();//dcValuePairs["Status"];
                //                    //msg.keyStatus = keyStatus.ToString();//dcValuePairs["keyStatus"];
                //                    //msg.LastRunDate = Utility.DateTimeToString(DateTime.Now); //dcValuePairs["LastRunDate"];
                //                    //Registry_Handler.SaveToRegistry(msg);
                //                }
                #endregion
                //                catch (Exception ex)
                //                {
                //                    throw new Exception("Message from server is not in correct Format" + Environment.NewLine + ex.Message);

                //                }
                #endregion

                System.Windows.Forms.MessageBox.Show("Application activated successfully", "GSPORT");
                textBox1.Text = "Application activated successfully";
                // close everything
                webResponseSReader.Close();
                webResponse.Close();
                webDataStream.Close();
                this.Close();
                #endregion
               
            }
            catch (WebException we) //Any how key is invalid
            {
                #region Commented_Out
                //using (WebResponse response = we.Response)
                //{
                //    HttpWebResponse httpResponse = (HttpWebResponse)response;
                //    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                //    string messageFromServer = string.Empty;
                //    using (Stream data = response.GetResponseStream())
                //    using (var reader = new StreamReader(data))
                //    {
                //        // text is the response body
                //        messageFromServer = reader.ReadToEnd();
                //    }

                //    // You now have the JSON text in the responseFromServer variable, use it :)
                //    string[] responseFromServer_1 = messageFromServer.Split(','); //.Replace(',', '\\');
                //    string sl = "";
                //    string servermsg = string.Empty;
                //    foreach (string s in responseFromServer_1)
                //    {
                //        sl += s + Environment.NewLine;

                //        if (s.Contains("\"errormessage\":"))
                //        {
                //            servermsg = s.Replace("\"errormessage\":", "");
                //        }

                //    }
                //    if (servermsg.Trim().Length > 0)
                //        MessageBox.Show(servermsg.Replace("\"", ""));
                //    sl = sl.Replace("\"", " ");
                //    sl = sl.Replace("{", " ");
                //    sl = sl.Replace("}", " ");
                //    sl = sl.Replace("[", " ");
                //    sl = sl.Replace("]", " ");

                //Message to show to user.
                //string msgResponse = responseFromServer_1[5];                   
#endregion
                textBox1.Text = textBox1.Text + Environment.NewLine + we.Message + Environment.NewLine + we.StackTrace;

                
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void KeyValidator_Load(object sender, EventArgs e)
        {
            IsClose = false;
        }

        private void txtLicKey_TextChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
            //IsClose = true;
            //this.Close();
            if (textBox1.Text.Contains("successfully")) 
            {
                //let the application launch
            }
            else
            {
                Environment.Exit(1);
            }
        }

        private void KeyValidator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (textBox1.Text.Contains("successfully"))
            {
                //let the application launch
            }
            else
            {
                Environment.Exit(1);
            }
        }
    }
}
