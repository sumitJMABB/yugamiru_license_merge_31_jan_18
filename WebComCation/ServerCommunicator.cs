﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using MongoDB.Bson;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace WebComCation
{
    class ServerCommunicator
    {
        Registry_Handler registry = new Registry_Handler();
        ServerOutInMsg msg = new ServerOutInMsg();
        List<string> lstMsgLines = new List<string>();
        Dictionary<string, string> dcValuePairs = new Dictionary<string, string>();
        public ServerOutInMsg SendAndGetResponse(string  activationKey)
        {
            try
            {
                //For Testing
                ServerOutInMsg m = new ServerOutInMsg();
                m.ActivationDate = Utility.DateTimeToString(DateTime.Now);
                m.ComputerID = keyRequest.GetComputerID();
                m.ComputerName = keyRequest.GetComputerName();
                m.EndDate = Utility.DateTimeToString(DateTime.Now.AddYears(1));
                m.keyStatus = LicenseStatus.Invalid.ToString();
                m.Status = LicenseStatus.Invalid.ToString();
                //For testing end

                if (activationKey != "")
                {
                    string licKey = registry.GetStoredRegistryMsg().LicenseKey;
                    string jsonQuery = "[{\"Computer_id\": \"" + keyRequest.GetComputerID() +
                                   "\",\"Computer_name\": \"" + keyRequest.GetComputerName() +
                                   "\",\"Activation_key\": \"" + licKey + "\"}]";//activationKey//BNGT0E0D0091A13C8G88351S9TSKIN5
                    String queryString = jsonQuery;



                //==================================================
                //queryString = "[{\"Computer_id\": \"BFEBFBFF000406E4\",\"Computer_name\": \"DESKTOP-9QLKVGO\",\"Activation_key\": \"BWHP0T0A00AG712J8Q8Z391HUG8KTH6\"}]";

                //=====================================================

                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);


                string data = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();

                //================================================================================
                //#region WebRequest                   
                //Connect to our Yugamiru Web Server
                #region old_Url
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/getsyncdata/");
                #endregion
                WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;

                    // create our stram to send
                    Stream webDataStream=null;
                    try
                    {
                        webDataStream = webRequest.GetRequestStream();
                        webDataStream.Write(requestByte, 0, requestByte.Length);
                    }
                    catch(Exception ex)
                    {
                        using (var client = new WebClient())
                        {
                            try
                            {
                                using (client.OpenRead("http://clients3.google.com/generate_204"))
                                {
                                    //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                                }
                            }
                            catch (Exception ex1)
                            {
                                System.Windows.Forms.MessageBox.Show("You are not connected to internet","GSPORT");
                                return m;
                            }
                        }
                    }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                    try
                    {
                        webDataStream = webResponse.GetResponseStream();
                    }
                    catch(Exception ex)
                    {

                    }

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();
                    if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                    {
                        int n1 = responseFromServer.IndexOf("Message");
                        string p1 = responseFromServer.Substring(n1 + 10);
                        p1 = p1.Replace("\0", "");
                        p1 = p1.Replace(@"\u", "");
                        p1 = p1.Replace(@"�", "");//                    
                        p1 = p1.Replace(@":", "");
                        p1 = p1.Substring(0, 112);
                        System.Windows.Forms.MessageBox.Show("Invalid Activation Key: " + p1,"GSPORT");
                        msg.Status = LicenseStatus.Invalid.ToString();
                        msg.keyStatus = LicenseStatus.Invalid.ToString();
                        msg.LicenseKey = licKey;
                        msg.ServerMessage = "Invalid Activation Key: " + p1;
                        return msg;
                        //Application.Exit();
                    }
                    else
                   if (responseFromServer.Contains("invalid"))
                    {
                        msg.Status = LicenseStatus.Invalid.ToString();
                        msg.keyStatus = LicenseStatus.Invalid.ToString();
                        msg.LicenseKey = licKey;
                        msg.ServerMessage = "Invalid Activation Key";
                        return msg;
                    }


                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace(sw[0].ToString(), "");//
                sw = sw.Replace(@":", "");
                string cpID = sw;
                cpID = sw.Substring(18, 16);

                string cpname = sw;
                cpname = sw.Substring(47, 15);

                string CPKey = sw;
                CPKey = sw.Substring(74, 31);

                string Stdt = sw.Substring(116, 10);
                string eddt = sw;
                eddt = sw.Substring(135, 10);
                string actdt = sw;
                actdt = sw.Substring(161, 10);
                string ltype = sw;
                ltype = sw.Substring(184, 1);
                string strrm = sw;
                strrm = sw.Substring(193);
                LicenseStatus keyStatus = LicenseStatus.Unknown;

                if (strrm.Contains("invalid"))
                {
                    keyStatus = LicenseStatus.Invalid;
                    strrm = strrm.Replace("invalid", "");
                }
                else if (strrm.Contains("valid"))
                {
                    keyStatus = LicenseStatus.Valid;
                    msg.Status = LicenseStatus.Valid.ToString();
                    msg.keyStatus = LicenseStatus.Valid.ToString();
                    msg.ServerMessage = "Activation Key is valid";
                    strrm = strrm.Replace("valid", "");
                }
                    //System.Windows.Forms.MessageBox.Show(strrm);
                    //ServerOutInMsg msg = new ServerOutInMsg();

                    try
                    {
                        msg.ActivationDate = actdt;//dcValuePairs["ActivationDate"];
                        msg.ComputerID = cpID;//dcValuePairs["ComputerID"];
                        msg.ComputerName = cpname;//dcValuePairs["ComputerName"];
                        msg.EndDate = eddt; //dcValuePairs["EndDate"];
                        msg.LicenseKey = licKey; //dcValuePairs["LicenseKey"];
                        msg.ServerMessage = strrm;//dcValuePairs["Message"];
                        msg.StartDate = Stdt;//dcValuePairs["StartDate"];
                        msg.Status = keyStatus.ToString();//dcValuePairs["Status"];
                        msg.keyStatus = keyStatus.ToString();//dcValuePairs["keyStatus"];
                        msg.LastRunDate = Utility.DateTimeToString(DateTime.Now);// dcValuePairs["LastRunDate"];
                        Registry_Handler.SaveToRegistry(msg);
                    }

                    catch (Exception ex)
                    {
                        throw new Exception("Message from server is not in correct Format" + Environment.NewLine + ex.Message);

                    }
                }
                else
                {
                    //throw new Exception("Valid License Key Not Found");
                    msg.keyStatus = "Invalid";
                    msg.ServerMessage = "Valid Activation Key Not Found";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return msg;
        }

        public static string ToBson<T>(T value)
        {
            using (MemoryStream ms = new MemoryStream())
            using (BsonDataWriter datawriter = new BsonDataWriter(ms))
            {

                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(datawriter, value);
                return Convert.ToBase64String(ms.ToArray());
            }

        }

        public static T FromBson<T>(string base64data)
        {

            byte[] data=Convert.FromBase64String(base64data);

            using (MemoryStream ms = new MemoryStream(data))
            using (BsonDataReader reader = new BsonDataReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                return serializer.Deserialize<T>(reader);
            }
        }

        public static Byte[] GetBytesFromBinaryString(String binary)
        {
            var list = new List<byte>();

            for (int i = 0; i < binary.Length; i += 8)
            {
                String t = binary.Substring(i, 8);

                list.Add(Convert.ToByte(t, 2));
            }

            return list.ToArray();
        }



    }
}
