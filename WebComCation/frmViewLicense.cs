﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebComCation
{
    public partial class frmViewLicense : Form
    {
        public static bool ViewLicenseActive = false;
        string licenseKey = string.Empty;
        public frmViewLicense()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmViewLicense_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            //string licenseKey = Registry_Handler.GetActivationKeyFromReg();
            //if(licenseKey.Length>0)
            try
            {

                //Fetch locally stored values first
                string LicenseKey = "";// Registry_Handler.GetActivationKeyFromReg();
                string ComputerKey = "";// Registry_Handler.GetComputerKeyFromReg();
                string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                //string ak = LicenseKey; //Validation Key
                //string ck = ComputerKey; // Computer Key
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                //ql.StoreKeys(ak, ck);
                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                if (LicenseKey.Trim() == "" || ComputerKey.Trim() == "")
                {      
                    if (LicenseKey.Trim().Length>0 && ComputerKey.Trim() == "")
                    {
                        string renewedkey= LicenseValidator.GetRenewedComputerKey(LicenseKey, "");
                        if (renewedkey.Trim().ToUpper() != licenseKey.Trim().ToUpper())
                        {
                            MessageBox.Show("Activation Key has been updated", "gsport", MessageBoxButtons.OK);
                            ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());
                            ql.GetStatus();
                            string EndDate = ql.ExpiryDate.ToString("dd/MMM/yyyy"); //Utility.DateTimeToString(ql.ExpiryDate);
                            txtLicense.Text = LicenseKey;
                            txtExpiryDate.Text = EndDate;
                            int DaysLeft = ql.DaysLeft;
                            var lctype = ql.LicenseType;
                            txtType.Text = lctype.ToString();
                            txtActivationDate.Text = ql.DaysLeft.ToString();
                        }
                    }
                    else
                    {
                        //MessageBox.Show("No License found on the PC", "gsport", MessageBoxButtons.OK);
                    }

                                          
                    //return ;
                }
                else if (LicenseKey.Trim() != "" && ComputerKey.Trim() != "")
                {
                    //Check Offline                   
                    ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());                    
                    ql.GetStatus();
                    string EndDate = ql.ExpiryDate.ToString("dd/MMM/yyyy"); //Utility.DateTimeToString(ql.ExpiryDate);
                    txtLicense.Text = LicenseKey;
                    txtExpiryDate.Text = EndDate;
                    int DaysLeft = ql.DaysLeft;
                    var lctype = ql.LicenseType;
                    txtType.Text = lctype.ToString();
                    txtActivationDate.Text = ql.DaysLeft.ToString();

                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            
            //else
            //{
             //   MessageBox.Show("No License found on the system", "gsport", MessageBoxButtons.OK);
            //}
           
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                ViewLicenseActive = true;
                LicenseValidator.GetRenewedComputerKey("", "");
                frmViewLicense_Load(null, null);
            }
            catch
            {

            }
            finally
            {

            }
        }

        private void frmViewLicense_FormClosing(object sender, FormClosingEventArgs e)
        {
            ViewLicenseActive = false;
        }
    }
}
