﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using QlmLicenseLib;

namespace WebComCation
{
    public static class LicenseValidator
    {
        static Registry_Handler rh = new Registry_Handler();
        static ServerOutInMsg valueSetReg = new ServerOutInMsg();
        static ServerOutInMsg valueSetServer = new ServerOutInMsg();
        static ServerCommunicator sc = new ServerCommunicator();

        public static LicenseStatus IsResponseValid(ServerOutInMsg registryMSG, ServerOutInMsg srverMSG)
        {
            LicenseStatus lt = LicenseStatus.Unknown;
            //if(registryMSG.ActivationDate==srverMSG.ActivationDate &&
            //    registryMSG.ComputerID == srverMSG.ActivationDate &&
            //    registryMSG.ComputerName == srverMSG.ActivationDate &&
            //        registryMSG.EndDate == srverMSG.ActivationDate &&
            //    registryMSG.LicenseKey == srverMSG.ActivationDate &&
            //    registryMSG.ServerMessage == srverMSG.ActivationDate &&
            //    registryMSG.StartDate == srverMSG.ActivationDate &&
            //    registryMSG.Status  == srverMSG.ActivationDate)
            if (srverMSG.Status.ToUpper().Contains("invalid".ToUpper()))
            {
                lt = LicenseStatus.Invalid;
            }
            else
            {
                lt = LicenseStatus.Valid;
            }


            return lt;
        }
        public static LicenseStatus IsDateValid(ServerOutInMsg registryMSG)
        {
            LicenseStatus lstt = LicenseStatus.Unknown;
            if (registryMSG.EndDate == null || registryMSG.EndDate.Length == 0)
            {
                registryMSG.EndDate = Utility.DateTimeToString(DateTime.Now.AddHours(1));
            }
            DateTime dtEndCheck = Utility.StringToDateTime(registryMSG.EndDate);
            DateTime dtLastRun = (Registry_Handler.GetLastRunDateFromReg());
            DateTime dt = DateTime.Now;
            bool isDateCheckOK = false;
            if (Utility.IsFirstDateGreaterOrEqual(dtEndCheck, dt))
            {
                //lstt = LicenseStatus.Valid;
                isDateCheckOK = true;
            }
            else
            {
                //lstt = LicenseStatus.Invalid;
                isDateCheckOK = false;
            }
            if (Utility.IsFirstDateGreaterOrEqual(dtLastRun, DateTime.Now.AddDays(0)))
            {
                isDateCheckOK = false;
                System.Windows.Forms.MessageBox.Show("PC Date is Invalid", "GSPORT");
                Application.Exit();
                System.Environment.Exit(1);
                //throw new Exception(" System Date is Invalid");
            }





            //Check system date
            if (isDateCheckOK)
            {
                return LicenseStatus.Valid;
            }
            else
            {
                return LicenseStatus.Invalid;
            }
        }
        public static LicenseValidationResult IsPresentStoredLicenseValid()
        {

            LicenseValidationResult result = new LicenseValidationResult();
            result.status = LicenseStatus.Valid;
            valueSetReg = rh.GetStoredRegistryMsg();
            LicenseStatus lst = LicenseStatus.Valid;
            //If App running first time.
            if (valueSetReg.ActivationDate == "FirstTime")
            {
                result.Message = "FirstTime";
                result.status = LicenseStatus.Valid;
                return result;
            }



            //Check last run basis if system date is valid



            try
            {
                if (IsDateValid(valueSetReg) != LicenseStatus.Valid && lst == LicenseStatus.Invalid)
                {
                    //throw new Exception("system date is not valid");
                    result.Message = "PC date is not valid";
                    result.status = LicenseStatus.Invalid;
                    return result;
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                Application.Exit();
            }
            if (valueSetReg.LicenseKey == null || valueSetReg.LicenseKey.Length == 0)
            {
                //License not found It may be due to the application on the  is running first time.
                //Launch License Wizard and ask user for License Key
                //System.Windows.Forms.MessageBox.Show("No valid License found for the application");

                result.Message = "No valid License found for the application.";// + Environment.NewLine + "Do you want to provide a valid Activation key?";
                result.status = LicenseStatus.Invalid;
                //return LicenseStatus.Invalid;
                return result;
            }



            valueSetServer = sc.SendAndGetResponse(valueSetReg.LicenseKey);


            if (/*result.status!=LicenseStatus.Valid && */valueSetServer.keyStatus.ToUpper().Trim().Contains("INVALID"))
            {
                //lst = LicenseStatus.Invalid;
                result.Message = "Activation key is Invalid";
                result.status = LicenseStatus.Invalid;

                //throw new Exception("License key is not valid");
            }

            if (!(valueSetServer.LicenseKey != null || valueSetServer.LicenseKey.Trim().ToUpper() != valueSetReg.LicenseKey.Trim().ToUpper() || lst != LicenseStatus.Valid))
            {
                //throw new Exception("Application's License is not valid");
                result.Message = "Activation key is not valid";
                result.status = LicenseStatus.Invalid;
            }

            if (result.Message == null)
            {
                result.Message = "";
            }

            // return lst;
            return result;
        }

        public static void HandleInvalidLicenseEvent(LicenseValidationResult rs)
        {
            if (rs.status != LicenseStatus.Valid)
            {
                //DialogResult dr = System.Windows.Forms.MessageBox.Show(rs.Message + Environment.NewLine + "Do you want to provide a Valid Activation Key?", "GSPORT",
                //    MessageBoxButtons.YesNo);
                //if (dr == DialogResult.No)
                //{
                //    System.Windows.Forms.Application.Exit();
                //}
                //else if (dr == DialogResult.Yes)
                //{
                //    KeyValidator kv = new KeyValidator();
                //    kv.ShowDialog();
                //}
                KeyValidator kv = new KeyValidator();
                kv.ShowDialog();
            }
        }

        public static LicenseStatus ValidateLicenseAtLogonQlm()
        {
            LicenseStatus ret = LicenseStatus.Unknown;
            //===============DateValidation
            ServerOutInMsg dateMessage = new ServerOutInMsg();
            //=================now read here the end date and from stored CK=======VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV






            //=============================




            //fetch CK CN and AK from system
            Keys.ReadKey();
            string CK = Keys.ComputerKey;
            string AK = Keys.ActivationKey;
            string endDate = Keys.EndDate;

            //=================
            Cursor.Current = Cursors.WaitCursor;

            string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                "\",\"Activation_key\":\"" + AK + "\"}]";
            String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
            //3.
            JsonSerializer jsonSerializer = new JsonSerializer();
            //4.
            MemoryStream objBsonMemoryStream = new MemoryStream();
            //5.
            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
            //6.
            jsonSerializer.Serialize(bsonWriterObject, studentObject);
            //text = queryString;
            byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



            #region WebRequest

            //Connect to our Yugamiru Web Server
            WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {
                using (var client = new WebClient())
                {
                    try
                    {
                        using (client.OpenRead("http://clients3.google.com/generate_204"))
                        {
                            //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                        }
                    }
                    catch (Exception ex1)
                    {
                        System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                        return LicenseStatus.Unknown;
                    }
                }
            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            webDataStream = webResponse.GetResponseStream();

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();
            #endregion

            #region License_Expired_Case
            if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
            {
                int n1 = responseFromServer.IndexOf("Message");
                string p1 = responseFromServer.Substring(n1 + 10);
                p1 = p1.Replace("\0", "");
                p1 = p1.Replace(@"\u", "");
                p1 = p1.Replace(@"�", "");//                    
                p1 = p1.Replace(@":", "");
                p1 = p1.Substring(0, 112);
                MessageBox.Show("Activation Key Expired");//+ p1);
                //textBox1.Text = "License Key Expired";//+ p1;
                return LicenseStatus.Expired;
                //Application.Exit();
            }
            #endregion


            #region Invalid_License
            else if (responseFromServer.Contains("invalid"))
            {
                MessageBox.Show("Invalid Activation Key");
                //Application.Exit();
                //textBox1.Text = "Invalid License Key";
                return LicenseStatus.Invalid;
            }
            #endregion


            #region Valid_License
            string sw = responseFromServer.Replace("?", "");
            sw = sw.Replace("\0", "");
            sw = sw.Replace(@"\u", "");
            sw = sw.Replace(@"�", "");//
                                      //sw = sw.Replace(sw[7].ToString(), "");//
            sw = sw.Replace("\u0002", "-");
            sw = sw.Replace("\u0016", "");
            sw = sw.Replace("\u0006", "");
            sw = sw.Replace("\u0010", "");

            string CPK = sw;
            string strrm = sw;
            CPK = CPK.Replace("Status-ComputerKey ", "");
            CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");
#endregion
            #region Store_Computer_Key_Locally

            Keys.ReadKey();

            //QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            //Keys.ActivationKey = txtLicKey.Text.Trim();
            //Keys.ComputerKey = CPK;
            //Keys.StoreAKey();
            //Registry_Handler.SaveCurrentRunDateToReg();
            #endregion

            //=================

            return ret;
        }
        /// <summary>
        /// Call this method after activation on before each launch
        /// </summary>
        /// <returns></returns>
        public static LicenseStatus VerifyPreActivated()
        {
            int runcount = 0;
            ReCheck:
            runcount++;
            LicenseStatus st = LicenseStatus.Unknown;
            LicenseValidator.GetRenewedComputerKey("", "");
            //Fetch locally stored values first
            string LicenseKey = "";// Registry_Handler.GetActivationKeyFromReg();
            string ComputerKey = "";//Registry_Handler.GetComputerKeyFromReg();
            string computerID = keyRequest.GetComputerID();
            QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            //store keys first.
            string ak = LicenseKey; //Validation Key
            string ck = ComputerKey; // Computer Key
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            //ql.StoreKeys(ak, ck);
           
            //EndDate = Utility.DateTimeToString(ql.ExpiryDate);

            //DaysLeft = ql.DaysLeft;
//========================================================================


            ql.ReadKeys(ref LicenseKey, ref ComputerKey);
            if(LicenseKey.Trim()=="" || ComputerKey.Trim()=="")
            {
                //redirect to License Activation Screen
                st = LicenseStatus.Unknown;

                
                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                if (LicenseKey.Trim().Length > 0 && ComputerKey.Trim() == "")
                {
                    MessageBox.Show("Application Activation Key has been expired.", "gsport", MessageBoxButtons.OK);
                }
                else if(LicenseKey.Trim().Length > 0 && ComputerKey.Trim().Length>0)
                {
                    //MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
                    MessageBox.Show("Activation key has been updated ", "gsport", MessageBoxButtons.OK);
                }
                //WebComCation.KeyValidator kv = new KeyValidator();
                //kv.ShowDialog();
                //goto ReCheck;
                return st;
            }
            else if(LicenseKey.Trim() != "" && ComputerKey.Trim() != "")
            {
                //Check Offline
                bool licensevalid = false;
                ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());
                
                ELicenseStatus status = ql.GetStatus();
                //If system date tempered

                if(status==ELicenseStatus.EKeyTampered)
                {
                    MessageBox.Show("Cannot launch Application" + Environment.NewLine + "Something is wrong with this system. Most probably system date & time is not correct. ", "gsport");
                    Environment.Exit(0);
                }


                //if(status==ELicenseStatus.EKeyDemo || ELicenseStatus.EKeyPermanent || ELicenseStatus.)
                string EndDate = Utility.DateTimeToString(ql.ExpiryDate);

                int DaysLeft = ql.DaysLeft;
                if(DaysLeft>0)
                {
                    st = LicenseStatus.Valid;
                }
                //else
                {
                    //Check Online If License renewed.

                    string rCK= GetRenewedComputerKey(LicenseKey, ComputerKey);
                    if (rCK.Trim().ToUpper().Contains("EXPIRED"))
                    {
                        st = LicenseStatus.Expired;
                        MessageBox.Show("Existing license has been expired.", "gsport");
                    }
                    else
                    {
                        if (runcount < 2)
                            goto ReCheck;
                    }
                }

            }

            
            return st;
        }
        /// <summary>
        /// Call this method if existing license has been expired. It will check from server if same license
        /// has been renewed then it will update local system with new ComputerKey
        /// </summary>
        /// <returns></returns>
        public static string GetRenewedComputerKey(string ActivationKey, string ComputerKey)
        {

            string renewedCompKey = string.Empty;
            //=============GSP-304 start==============
            if(ActivationKey==null || ActivationKey.Trim().Length==0)
            {
                //Read Activation key using soraco APIs
                string LicenseKey = "";// Registry_Handler.GetActivationKeyFromReg();
                //string ComputerKey = "";//Registry_Handler.GetComputerKeyFromReg();
                //string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                string ak = ""; //Validation Key
                string ck = ""; // Computer Key
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                //ql.StoreKeys(ak, ck);

                //EndDate = Utility.DateTimeToString(ql.ExpiryDate);

                //DaysLeft = ql.DaysLeft;
                //========================================================================


                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                ActivationKey = LicenseKey;
            }


            //================GSP-304 end=======


            //===================
            LicenseStatus st = LicenseStatus.Unknown;
            string APIUrl = string.Empty;
            string ComputerName = keyRequest.GetComputerName();
            string computerID = keyRequest.GetComputerID();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + ActivationKey + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                //text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/validatelicense");
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))//for checking internet connectivity.
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            // Commented by sumit GSP-356-----------START

                            if (frmViewLicense.ViewLicenseActive)
                                //System.Windows.Forms.MessageBox.Show("PC is not connected to internet");
                                System.Windows.Forms.MessageBox.Show("PC is not connected to internet");
                                
                            //by sumit GSP-356-----------END
                                return "Connection Error";
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();


                #region License_Expired_Case
                if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);
                    MessageBox.Show("Activation Key Expired");//+ p1);
                    //textBox1.Text = "License Key Expired";//+ p1;
                    return "expired";
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid"))
                {
                    MessageBox.Show("Invalid Activation Key");
                    ////Application.Exit();
                    //textBox1.Text = "Invalid Activation Key";
                    return "invalid";
                }
                #endregion


                #region Valid_License
                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");
                sw = sw.Replace("~", "");

                string CPK = sw;
                string strrm = sw;
                CPK = CPK.Replace("BStatus-ComputerKey ", ""); //"B\0\0\0\u0010Status\0�\0\0\0\u0002ComputerKey\0 \0\0\0V5HA050Z00BGA11H8U85321RP2VSTNN\0\0"
                CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");
                CPK = CPK.Replace("-LicenseType\u0001-Message\u000eValid License-KeyValid", "");
                #region Store_Computer_Key_Locally

                //Keys.ReadKey();

                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                Keys.ActivationKey = ActivationKey;

                Keys.ComputerKey = CPK;
                renewedCompKey = CPK;
                //LicenseStatus st = LicenseStatus.Unknown;
                //Fetch locally stored values first
                //string LicenseKey = txtLicKey.Text; // Registry_Handler.GetActivationKeyFromReg();
               
                ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                string ak = Keys.ActivationKey; //Validation Key
                string ck = renewedCompKey; // Computer Key
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeys(ak, ck);

                if (ak == "")
                    return "invalid";

                #endregion
                #endregion

                if (ComputerKey.Length>0 && CPK.Trim().ToUpper() != ComputerKey && CPK!="")
                    System.Windows.Forms.MessageBox.Show("Activation Key renewed successfully", "GSPORT");
                //textBox1.Text = "Key activated successfully";
                // close everything
                webResponseSReader.Close();
                webResponse.Close();
                webDataStream.Close();
                //this.Close();
                //return LicenseStatus.Valid;
                #endregion
            }
            catch
            {

            }
            finally
            {

            }
            //==================
            return renewedCompKey;
        }

        public static LicenseStatus ActiavteLicense(string licenseKey)
        {
            //string computerKey = string.Empty;
            LicenseStatus st = LicenseStatus.Unknown;
            string APIUrl = string.Empty;
            string ComputerName = keyRequest.GetComputerName();
            string computerID = keyRequest.GetComputerID();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + licenseKey + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                //text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            System.Windows.Forms.MessageBox.Show("PC is not connected to internet");
                            return LicenseStatus.Error;
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();


                #region License_Expired_Case
                if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);
                    MessageBox.Show("Activation Key Expired");//+ p1);
                    //textBox1.Text = "License Key Expired";//+ p1;
                    return LicenseStatus.Expired;
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid"))
                {
                    MessageBox.Show("Invalid Activation Key");
                    ////Application.Exit();
                    //textBox1.Text = "Invalid License Key";
                    return LicenseStatus.Invalid;
                }
                #endregion


                #region Valid_License
                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");

                string CPK = sw;
                string strrm = sw;
                CPK = CPK.Replace("Status-ComputerKey ", "");
                CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");

                #region Store_Computer_Key_Locally

                //Keys.ReadKey();

                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                Keys.ActivationKey = licenseKey;
                Keys.ComputerKey = CPK;
                Registry_Handler.SaveComputerKeyToRegistry(CPK);
                //Keys.StoreAKey();
                Keys.ReadKey();
                Registry_Handler.SaveCurrentRunDateToReg();
                #endregion
                #endregion

                System.Windows.Forms.MessageBox.Show("Activation Key activated successfully", "GSPORT");
                Cursor.Current = Cursors.WaitCursor;
                //textBox1.Text = "Key activated successfully";
                // close everything
                webResponseSReader.Close();
                webResponse.Close();
                webDataStream.Close();
                //this.Close();
                return LicenseStatus.Valid;
                #endregion
            }
            catch
            {

            }
            finally
            {

            }


            return st;
        }

    }
}
