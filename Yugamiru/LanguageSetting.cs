﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Resources;
using Yugamiru.Properties;
using System.Collections;
using System.IO;

namespace Yugamiru
{
    public partial class LanguageSetting : Form
    {
        JointEditDoc m_JointEditDoc;
        
        public LanguageSetting(JointEditDoc GetDocument)
        {
            InitializeComponent();
            if(GetDocument.GetLanguage() != string.Empty)
            m_Combo_Language.SelectedIndex = m_Combo_Language.Items.IndexOf(GetDocument.GetLanguage());
            else
            m_Combo_Language.SelectedIndex = 0;
            m_JointEditDoc = GetDocument;
           
        }

        private void LanguageSetting_Load(object sender, EventArgs e)
        {

        }

        private void ID_OK_Click(object sender, EventArgs e)
        {            
            var selectedLanguage = m_Combo_Language.SelectedItem.ToString();
            //Yugamiru.Properties.Resources.ResourceManager.ReleaseAllResources();
            string Selected_Resource = string.Empty;
            switch (selectedLanguage)
            {
                case "Japanese":                 
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
                    Selected_Resource = "ja-JP";
                    break;
                case "Thai":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("th-TH");
                    Selected_Resource = "th-TH";
                    break;
                case "Korean":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ko-KR");
                    Selected_Resource = "ko-KR";
                    break;
                case "Chinese":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("zh-CN");
                    Selected_Resource = "zh-CN";
                    break;
                default:
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en");
                    Selected_Resource = "en";
                    break;
            }
            var t = CultureInfo.DefaultThreadCurrentUICulture;
            
            File.WriteAllText(@"Resources\Languageconfig.txt", "LANGUAGE,"+"\"" + Selected_Resource + "\"" + "\r\n");

            //Reload all the controls
            m_JointEditDoc.SetLanguage(selectedLanguage);
            //Finally refresh the parent form
            // FunctiontoChangeLanguage(EventArgs.Empty);
            if (m_JointEditDoc.GetMeasurementViewMode() == Constants.MEASUREMENTVIEWMODE_INITIALIZE || 
                m_JointEditDoc.GetMeasurementViewMode() == Constants.MEASUREMENTVIEWMODE_RETAIN)
            {
                m_JointEditDoc.GetMeasurementDlg().RefreshForm();
            }
            else if (m_JointEditDoc.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN ||
                m_JointEditDoc.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING ||
                m_JointEditDoc.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING)
            {
                m_JointEditDoc.GetMeasurementStart().RefreshForm();
            }
            else if (m_JointEditDoc.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP ||
                m_JointEditDoc.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_KNEE ||
                m_JointEditDoc.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_UPPERBODY)
            {
                m_JointEditDoc.GetJointEditView().RefreshForm();

            }
            else if (m_JointEditDoc.GetSettingMode() == Constants.SETTING_SCREEN_MODE_TRUE)
            {
                m_JointEditDoc.GetSettingView().RefreshForms();

            }
            else if (m_JointEditDoc.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_SIDE)
            {
                m_JointEditDoc.GetSideJointEditView().RefreshForms();
            }
            else if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_TRUE)
            {
                FunctiontoChangeLanguage(EventArgs.Empty);
            }
            else if (m_JointEditDoc.GetLanguage() != null)
                m_JointEditDoc.GetInitialScreen().RefreshForms();
            

                this.Close();
        }

        private void ID_Cancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }
        public event EventHandler EventToLangChange; // creating event handler - step1
        public void FunctiontoChangeLanguage(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToLangChange;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
    }
}
