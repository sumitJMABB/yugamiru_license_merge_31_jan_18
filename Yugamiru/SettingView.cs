﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class SettingView : Form
    {
        JointEditDoc m_JointEditDoc;
        public SettingView(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;
            pictureBox1.Size = new Size(Yugamiru.Properties.Resources.Mainpic7.Size.Width,
                Yugamiru.Properties.Resources.Mainpic7.Size.Height);
            pictureBox1.BackColor = Color.Transparent;
            this.Controls.Add(pictureBox1);
            pictureBox1.Image = Yugamiru.Properties.Resources.Mainpic7;

            GetDocument.LoadSetup5File();
            if (m_JointEditDoc.GetDisplayArrow() == 1)
            {
                IDC_RADIO_ARROW_ON.Image = Yugamiru.Properties.Resources.radio_on;
                IDC_RADIO_ARROW_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            }
            else
            {
                IDC_RADIO_ARROW_ON.Image = Yugamiru.Properties.Resources.radio_off;
                IDC_RADIO_ARROW_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            }
            if (m_JointEditDoc.GetDisplayCenterLine() == 1)
            {
                IDC_RADIO_CENTERLINE_OFF.Image = Yugamiru.Properties.Resources.radio_off;
                IDC_RADIO_CENTERLINE_ON.Image = Yugamiru.Properties.Resources.radio_on;
            }
            else
            {
                IDC_RADIO_CENTERLINE_OFF.Image = Yugamiru.Properties.Resources.radio_on;
                IDC_RADIO_CENTERLINE_ON.Image = Yugamiru.Properties.Resources.radio_off;
            }
            if (m_JointEditDoc.GetDisplayCentroid() == 1)
            {
                IDC_RADIO_CENTROID_OFF.Image = Yugamiru.Properties.Resources.radio_off;
                IDC_RADIO_CENTROID_ON.Image = Yugamiru.Properties.Resources.radio_on;
            }
            else
            {
                IDC_RADIO_CENTROID_OFF.Image = Yugamiru.Properties.Resources.radio_on;
                IDC_RADIO_CENTROID_ON.Image = Yugamiru.Properties.Resources.radio_off;
            }
            if (m_JointEditDoc.GetDisplayLabel() == 1)
            {
                IDC_RADIO_LABEL_OFF.Image = Yugamiru.Properties.Resources.radio_off;
                IDC_RADIO_LABEL_ON.Image = Yugamiru.Properties.Resources.radio_on;
            }
            else
            {
                IDC_RADIO_LABEL_OFF.Image = Yugamiru.Properties.Resources.radio_on;
                IDC_RADIO_LABEL_ON.Image = Yugamiru.Properties.Resources.radio_off;
            }
            if (m_JointEditDoc.GetDisplayMuscleReport() == 1)
            {
                IDC_RADIO_MUSCLEREPORT_OFF.Image = Yugamiru.Properties.Resources.radio_off;
                IDC_RADIO_MUSCLEREPORT_ON.Image = Yugamiru.Properties.Resources.radio_on;
            }
            else
            {
                IDC_RADIO_MUSCLEREPORT_OFF.Image = Yugamiru.Properties.Resources.radio_on;
                IDC_RADIO_MUSCLEREPORT_ON.Image = Yugamiru.Properties.Resources.radio_off;

            }
            if (m_JointEditDoc.GetDisplayScore() == 1)
            {
                IDC_RADIO_SCORE_OFF.Image = Yugamiru.Properties.Resources.radio_off;
                IDC_RADIO_SCORE_ON.Image = Yugamiru.Properties.Resources.radio_on;
            }
            else
            {
                IDC_RADIO_SCORE_OFF.Image = Yugamiru.Properties.Resources.radio_on;
                IDC_RADIO_SCORE_ON.Image = Yugamiru.Properties.Resources.radio_off;

            }

            IDOK.Image = Yugamiru.Properties.Resources.setting_down;
            IDCANCEL.Image = Yugamiru.Properties.Resources.cancel_down;

            
            comboBox1.SelectedIndex = 0;
            
            switch (GetDocument.GetMarkerSizeType())
            {
                case 0:
                    comboBox1.SelectedIndex = 0;
                    break;
                case 1:
                    comboBox1.SelectedIndex = 1;
                    break;
                case 2:
                    comboBox1.SelectedIndex = 2;
                    break;
                default:
                    comboBox1.SelectedIndex = 0;
                    break;
            }
        }

        public void SettingView_SizeChanged(object sender, EventArgs e)
        {
            //--to centre the picture box while resizing the form
            pictureBox1.Left = (this.ClientSize.Width - pictureBox1.Width) / 2;
            //picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2;
            pictureBox1.Top = 1;

            comboBox1.Size = new Size(120, 200);
            comboBox1.Location = new Point(570,202);
            comboBox1.Left = (this.Width - comboBox1.Width) / 2 - 58;

            IDC_RADIO_ARROW_ON.Size = new Size(13,13);
            IDC_RADIO_ARROW_ON.Location = new Point(574, 277);
            IDC_RADIO_ARROW_ON.Left = comboBox1.Left + 1;

            IDC_RADIO_ARROW_OFF.Size = new Size(13, 13);
            IDC_RADIO_ARROW_OFF.Location = new Point(400 + 219, 277);
            IDC_RADIO_ARROW_OFF.Left = comboBox1.Left + 54;

            IDC_RADIO_CENTERLINE_ON.Size = new Size(13, 13);
            IDC_RADIO_CENTERLINE_ON.Location = new Point(400 + 166, 350);
            IDC_RADIO_CENTERLINE_ON.Left = comboBox1.Left +1;

            IDC_RADIO_CENTERLINE_OFF.Size = new Size(13, 13);
            IDC_RADIO_CENTERLINE_OFF.Location = new Point(400 + 219 , 350);
            IDC_RADIO_CENTERLINE_OFF.Left = comboBox1.Left + 54;

            IDC_RADIO_LABEL_ON.Size = new Size(13, 13);
            IDC_RADIO_LABEL_ON.Location = new Point(400 + 166, 422);
            IDC_RADIO_LABEL_ON.Left = comboBox1.Left + 1;

            IDC_RADIO_LABEL_OFF.Size = new Size(13, 13);
            IDC_RADIO_LABEL_OFF.Location = new Point(400 + 219,422);
            IDC_RADIO_LABEL_OFF.Left = comboBox1.Left + 54;

            IDC_RADIO_SCORE_ON.Size = new Size(13, 13);
            IDC_RADIO_SCORE_ON.Location = new Point(932, 277);
            IDC_RADIO_SCORE_ON.Left = comboBox1.Left + comboBox1.Width + 247;

            IDC_RADIO_SCORE_OFF.Size = new Size(13, 13);
            IDC_RADIO_SCORE_OFF.Location = new Point(985, 277);
            IDC_RADIO_SCORE_OFF.Left = comboBox1.Left + comboBox1.Width + 247 + 53;

            IDC_RADIO_MUSCLEREPORT_ON.Size = new Size(13, 13);
            IDC_RADIO_MUSCLEREPORT_ON.Location = new Point(932, 350);
            IDC_RADIO_MUSCLEREPORT_ON.Left = comboBox1.Left + comboBox1.Width + 247;

            IDC_RADIO_MUSCLEREPORT_OFF.Size = new Size(13, 13);
            IDC_RADIO_MUSCLEREPORT_OFF.Location = new Point(985, 350);
            IDC_RADIO_MUSCLEREPORT_OFF.Left = comboBox1.Left + comboBox1.Width + 247 + 53;

            IDC_RADIO_CENTROID_ON.Size = new Size(13, 13);
            IDC_RADIO_CENTROID_ON.Location = new Point(932,422);
            IDC_RADIO_CENTROID_ON.Left =  comboBox1.Left + comboBox1.Width + 247;

            IDC_RADIO_CENTROID_OFF.Size = new Size(13, 13);
            IDC_RADIO_CENTROID_OFF.Location = new Point(985, 422);
            IDC_RADIO_CENTROID_OFF.Left = comboBox1.Left + comboBox1.Width + 247 + 53;

            IDOK.Size = new Size(112, 42);
            IDOK.Location = new Point(400 + 50,600 - 70);
            IDOK.Left = comboBox1.Left +200 +30;

            IDCANCEL.Size = new Size(112, 42);
            IDCANCEL.Location = new Point(800 - 4,600 - 70);
            IDCANCEL.Left = comboBox1.Left - IDCANCEL.Width -3;
            //--end
        }

        private void IDC_RADIO_CENTROID_OFF_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void IDC_RADIO_ARROW_ON_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayArrow(1);
            IDC_RADIO_ARROW_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            IDC_RADIO_ARROW_ON.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void IDC_RADIO_ARROW_OFF_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayArrow(0);
            IDC_RADIO_ARROW_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            IDC_RADIO_ARROW_ON.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void IDC_RADIO_CENTERLINE_ON_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayCenterLine(1);
            IDC_RADIO_CENTERLINE_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            IDC_RADIO_CENTERLINE_ON.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void IDC_RADIO_CENTERLINE_OFF_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayCenterLine(0);
            IDC_RADIO_CENTERLINE_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            IDC_RADIO_CENTERLINE_ON.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void IDC_RADIO_LABEL_ON_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayLabel(1);
            IDC_RADIO_LABEL_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            IDC_RADIO_LABEL_ON.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void IDC_RADIO_LABEL_OFF_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayLabel(0);
            IDC_RADIO_LABEL_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            IDC_RADIO_LABEL_ON.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void IDC_RADIO_SCORE_ON_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayScore(1);
            IDC_RADIO_SCORE_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            IDC_RADIO_SCORE_ON.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void IDC_RADIO_SCORE_OFF_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayScore(0);
            IDC_RADIO_SCORE_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            IDC_RADIO_SCORE_ON.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void IDC_RADIO_MUSCLEREPORT_ON_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayMuscleReport(1);
            IDC_RADIO_MUSCLEREPORT_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            IDC_RADIO_MUSCLEREPORT_ON.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void IDC_RADIO_MUSCLEREPORT_OFF_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayMuscleReport(0);
            IDC_RADIO_MUSCLEREPORT_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            IDC_RADIO_MUSCLEREPORT_ON.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void IDC_RADIO_CENTROID_ON_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayCentroid(1);
            IDC_RADIO_CENTROID_OFF.Image = Yugamiru.Properties.Resources.radio_off;
            IDC_RADIO_CENTROID_ON.Image = Yugamiru.Properties.Resources.radio_on;

        }

        private void IDC_RADIO_CENTROID_OFF_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetDisplayCentroid(0);
            IDC_RADIO_CENTROID_OFF.Image = Yugamiru.Properties.Resources.radio_on;
            IDC_RADIO_CENTROID_ON.Image = Yugamiru.Properties.Resources.radio_off;

        }

        private void IDOK_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetSettingMode(Constants.SETTING_SCREEN_MODE_NONE);
            comboBox1.GetItemText(this.comboBox1.SelectedItem).ToString();
            int iMarkerSizeIndex = comboBox1.SelectedIndex;
            switch (iMarkerSizeIndex)
            {
                case 0:
                    m_JointEditDoc.SetMarkerSizeType(0);
                    break;
                case 1:
                    m_JointEditDoc.SetMarkerSizeType(1);
                    break;
                case 2:
                    m_JointEditDoc.SetMarkerSizeType(2);
                    break;
                default:
                    m_JointEditDoc.SetMarkerSizeType(0);
                    break;
            }
            
            m_JointEditDoc.SaveSetup5File();
            //FunctionToGoInitialScreen(EventArgs.Empty);
            this.Visible = false;
            m_JointEditDoc.GetInitialScreen().Visible = true;
            m_JointEditDoc.GetInitialScreen().RefreshForms();

        }
        public void RefreshForms()
        {
            pictureBox1.Image = Yugamiru.Properties.Resources.Mainpic7;
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);

            IDOK.Image = Yugamiru.Properties.Resources.setting_down;
            IDCANCEL.Image = Yugamiru.Properties.Resources.cancel_down;
        }

        private void IDCANCEL_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetSettingMode(Constants.SETTING_SCREEN_MODE_NONE);
            //FunctionToGoInitialScreen(EventArgs.Empty);
            this.Visible = false;
            m_JointEditDoc.GetInitialScreen().Visible = true;
            m_JointEditDoc.GetInitialScreen().RefreshForms();
        }
        public event EventHandler EventToGoInitialScreen; // creating event handler - step1
        public void FunctionToGoInitialScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToGoInitialScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
    }
}
