﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Windows;
using System.Windows.Input;
using static Yugamiru.stretchDIBbits;
using System.IO;
using System.Windows.Forms;

namespace Yugamiru
{
    public class SideJointEditWnd
    {

        /*
        #define JOINTS_COLOR	(RGB(255,0,0))
        #define INVALID_JOINTS_COLOR (RGB(108,108,30))
        #define BENCHMARK_COLOR	(RGB(0,0,255))
        #define BENCHMARK_LINE_COLOR (RGB(0,255,0))

        #define SKELETON_COLOR	(RGB(255,255,0))
        #define CURVES_COLOR	(RGB(255,0,0))
        */
        public byte[] m_pbyteBits;
        stretchDIBbits.BITMAPINFO m_bmi;
        public Graphics m_pDCOffscreen;
        public Bitmap m_pbmOffscreen;
        public Bitmap m_pbmOffscreenOld;
        public int m_iOffscreenWidth;
        public int m_iOffscreenHeight;
        public int m_iBackgroundWidth;
        public int m_iBackgroundHeight;
        public int m_iSrcX;
        public int m_iSrcY;
        public int m_iSrcWidth;
        public int m_iSrcHeight;

        public int m_iMouseCaptureMode;
        public int m_iMousePointXOnDragStart;
        public int m_iMousePointYOnDragStart;
        public int m_iScreenWidthOnDragStart;
        public int m_iScreenHeightOnDragStart;
        public int m_iSrcXOnDragStart;
        public int m_iSrcYOnDragStart;
        public int m_iSrcWidthOnDragStart;
        public int m_iSrcHeightOnDragStart;

        //	CPoint	m_aptEdit[18];
        //	bool m_isValidPoint[18];
        //	bool m_isValidEditPoint[18];

        public Point m_ptBenchmark1OnDragStart;
        public Point m_ptBenchmark2OnDragStart;

        public int m_Marker;       // ƒ}[ƒJ[ƒTƒCƒY
        public int m_iMarkerSizeBase;
        public int m_iEditPtID;
        public bool m_iEditingBenchmarkBar;
        public int m_iDataVersion;
        public int m_iShoulderOffset0;
        public int m_iShoulderOffset1;
        public int m_iShoulderOffset2;
        public bool m_bStyleLine;
        public uint m_uiStyleLineStyle;
        public Color m_crStyleLineColor;
        public uint m_uiStyleLineWidth;

        public Point m_ptEar;
        public Point m_ptShoulder;
        public Point m_ptRightBelt;
        public Point m_ptLeftBelt;
        //CPoint	m_ptKnee;
        //CPoint	m_ptAnkle;
        public Point m_ptBenchmark1;
        public Point m_ptBenchmark2;
        public Point m_ptKneeRightBelt;
        public Point m_ptKneeLeftBelt;
        public Point m_ptAnkleRightBelt;
        public Point m_ptAnkleLeftBelt;
        public Point m_ptChin;
        public Point m_ptGlabella;
        // CSideJointEditWnd
        public SideJointEditWnd()
        {

            m_pbyteBits = null;

            m_pDCOffscreen = null;

            m_pbmOffscreen = null;

            m_pbmOffscreenOld = null;

            m_iOffscreenWidth = 423;

            m_iOffscreenHeight = 470;

            m_iBackgroundWidth = 0;

            m_iBackgroundHeight = 0;

            m_iSrcX = 0;

            m_iSrcY = 0;

            m_iSrcWidth = 0;

            m_iSrcHeight = 0;

            m_iMouseCaptureMode = 0;

            m_iMousePointXOnDragStart = 0;

            m_iMousePointYOnDragStart = 0;

            m_iScreenWidthOnDragStart = 0;

            m_iScreenHeightOnDragStart = 0;

            m_iSrcXOnDragStart = 0;

            m_iSrcYOnDragStart = 0;

            m_iSrcWidthOnDragStart = 0;

            m_iSrcHeightOnDragStart = 0;

            m_Marker = 0;

            m_iMarkerSizeBase = 0;

            m_iDataVersion = 0;

            m_iShoulderOffset0 = 0;

            m_iShoulderOffset1 = 0;

            m_iShoulderOffset2 = 0;

            m_bStyleLine = false;

            m_uiStyleLineStyle = 0;



            m_uiStyleLineWidth = 0;

            m_iEditPtID = Constants.SIDEJOINTEDITWND_POINTID_NONE;

            m_iEditingBenchmarkBar = false;


            m_ptEar = new Point(0, 0);

            m_ptShoulder = new Point(0, 0);

            m_ptRightBelt = new Point(0, 0);

            m_ptLeftBelt = new Point(0, 0);
            //m_ptKnee( 0, 0 ),
            //m_ptAnkle( 0, 0 ),
            m_ptBenchmark1 = new Point(0, 0);

            m_ptBenchmark2 = new Point(0, 0);


            m_ptKneeRightBelt = new Point(0, 0);

            m_ptKneeLeftBelt = new Point(0, 0);

            m_ptAnkleRightBelt = new Point(0, 0);

            m_ptAnkleLeftBelt = new Point(0, 0);


        }
        // CSideJointEditWnd ƒƒbƒZ[ƒW ƒnƒ“ƒhƒ‰
        public bool SetBackgroundBitmap(int iWidth, int iHeight, byte[] pbyteBits)
        {
            m_bmi.bmiHeader.biSize = 40;//sizeof(BITMAPINFOHEADER);
            m_bmi.bmiHeader.biWidth = iWidth;
            m_bmi.bmiHeader.biHeight = -iHeight;
            m_bmi.bmiHeader.biPlanes = 1;
            m_bmi.bmiHeader.biBitCount = 24;
            m_bmi.bmiHeader.biCompression = 0;
            m_bmi.bmiHeader.biSizeImage = 0;
            m_bmi.bmiHeader.biXPelsPerMeter = 0;
            m_bmi.bmiHeader.biYPelsPerMeter = 0;
            m_bmi.bmiHeader.biClrUsed = 0;
            m_bmi.bmiHeader.biClrImportant = 0;

            m_bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            m_bmi.bmiColors[0].rgbBlue = 255;
            m_bmi.bmiColors[0].rgbGreen = 255;
            m_bmi.bmiColors[0].rgbRed = 255;
            m_bmi.bmiColors[0].rgbReserved = 255;

            m_iBackgroundWidth = iWidth;
            m_iBackgroundHeight = iHeight;

            int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * iHeight;
            if (iBmpBitsSize <= 0)
            {
                return false;
            }

            m_iSrcX = 0;
            m_iSrcY = 0;
            m_iSrcWidth = m_iBackgroundWidth;
            m_iSrcHeight = m_iBackgroundHeight;

            return true;
        }

        public void UpdateOffscreen(Graphics m_pDCOffscreen)
        {
            if (m_pDCOffscreen == null)
            {
                return;
            }

            if (m_pbyteBits != null)
            {
                stretchDIBbits.SetStretchBltMode(m_pDCOffscreen.GetHdc(), StretchBltMode.STRETCH_HALFTONE);
                m_pDCOffscreen.ReleaseHdc();

                stretchDIBbits.StretchDIBits(
            m_pDCOffscreen.GetHdc(),
            0, 0, m_iOffscreenWidth, m_iOffscreenHeight,
            m_iSrcX, (m_iBackgroundHeight - 1 - (m_iSrcY + m_iSrcHeight - 1)),
            m_iSrcWidth, m_iSrcHeight,
            m_pbyteBits,
            ref m_bmi,
            Constants.DIB_RGB_COLORS,
            Constants.SRCCOPY);

                m_pDCOffscreen.ReleaseHdc();
            }
            /*
            #if 0
                // ƒtƒbƒgƒvƒŒ[ƒg
                if ( theApp.m_CalibInf.isCaliblated ){
                    CPoint fpl[4];
                    fpl[0].x = ( theApp.m_CalibInf.FPLpos[0][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                    fpl[0].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[0][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
                    fpl[1].x = ( theApp.m_CalibInf.FPLpos[1][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                    fpl[1].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[1][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
                    fpl[2].x = ( theApp.m_CalibInf.FPLpos[2][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                    fpl[2].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[2][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
                    fpl[3].x = ( theApp.m_CalibInf.FPLpos[3][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                    fpl[3].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[3][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;

                    MyDrawLine(m_pDCOffscreen, fpl[0], fpl[1], PS_SOLID, RGB(255,0,0));
                    MyDrawLine(m_pDCOffscreen, fpl[0], fpl[2], PS_SOLID, RGB(255,0,0));
                    MyDrawLine(m_pDCOffscreen, fpl[2], fpl[3], PS_SOLID, RGB(255,0,0));
                    MyDrawLine(m_pDCOffscreen, fpl[1], fpl[3], PS_SOLID, RGB(255,0,0));
                }
            #endif
                */
            m_Marker = (int)(m_iMarkerSizeBase * m_iBackgroundWidth / (m_iSrcWidth * Constants.IMG_SCALE_MAX));


            // Update positions of uneditable joints	
            PointF ptHip = new PointF(
             ((m_ptRightBelt.X + m_ptLeftBelt.X) / 2.0f),
            ((m_ptRightBelt.Y + m_ptLeftBelt.Y) / 2.0f));
            PointF ptKnee = new PointF(
            ((m_ptKneeRightBelt.X + m_ptKneeLeftBelt.X) / 2.0f),
            ((m_ptKneeRightBelt.Y + m_ptKneeLeftBelt.Y) / 2.0f));
            PointF ptAnkle = new PointF(
    ((m_ptAnkleRightBelt.X + m_ptAnkleLeftBelt.X) / 2.0f),
    ((m_ptAnkleRightBelt.Y + m_ptAnkleLeftBelt.Y) / 2.0f));

            // ŠÖßA’†Sü...
            Point ptEarDisplay = new Point(0, 0);
            Point ptShoulderDisplay = new Point(0, 0);
            Point ptRightBeltDisplay = new Point(0, 0);
            Point ptLeftBeltDisplay = new Point(0, 0);
            Point ptHipDisplay = new Point(0, 0);
            Point ptKneeDisplay = new Point(0, 0);
            Point ptAnkleDisplay = new Point(0, 0);
            Point ptBenchmark1Display = new Point(0, 0);
            Point ptBenchmark2Display = new Point(0, 0);
            Point ptKneeRightBeltDisplay = new Point(0, 0);
            Point ptKneeLeftBeltDisplay = new Point(0, 0);
            Point ptAnkleRightBeltDisplay = new Point(0, 0);
            Point ptAnkleLeftBeltDisplay = new Point(0, 0);

            ptEarDisplay.X = (m_ptEar.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptEarDisplay.Y = (m_ptEar.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptShoulderDisplay.X = (m_ptShoulder.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptShoulderDisplay.Y = (m_ptShoulder.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptRightBeltDisplay.X = (m_ptRightBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptRightBeltDisplay.Y = (m_ptRightBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptLeftBeltDisplay.X = (m_ptLeftBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptLeftBeltDisplay.Y = (m_ptLeftBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptHipDisplay.X = (int)((ptHip.X - m_iSrcX)) * m_iOffscreenWidth / m_iSrcWidth;
            ptHipDisplay.Y = (int)((ptHip.Y - m_iSrcY)) * m_iOffscreenHeight / m_iSrcHeight;
            ptKneeDisplay.X = (int)((ptKnee.X - m_iSrcX)) * m_iOffscreenWidth / m_iSrcWidth;
            ptKneeDisplay.Y = (int)(ptKnee.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptAnkleDisplay.X = (int)(ptAnkle.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptAnkleDisplay.Y = (int)(ptAnkle.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptBenchmark1Display.X = (m_ptBenchmark1.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptBenchmark1Display.Y = (m_ptBenchmark1.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptBenchmark2Display.X = (m_ptBenchmark2.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptBenchmark2Display.Y = (m_ptBenchmark2.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

            ptKneeRightBeltDisplay.X = (m_ptKneeRightBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptKneeRightBeltDisplay.Y = (m_ptKneeRightBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptKneeLeftBeltDisplay.X = (m_ptKneeLeftBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptKneeLeftBeltDisplay.Y = (m_ptKneeLeftBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptAnkleRightBeltDisplay.X = (m_ptAnkleRightBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptAnkleRightBeltDisplay.Y = (m_ptAnkleRightBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            ptAnkleLeftBeltDisplay.X = (m_ptAnkleLeftBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            ptAnkleLeftBeltDisplay.Y = (m_ptAnkleLeftBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

            // Draw skeleton LINES
            MyDrawLine(m_pDCOffscreen, ptShoulderDisplay, ptHipDisplay, Constants.PS_SOLID, Color.FromArgb(255, 255, 0), 5);
            MyDrawLine(m_pDCOffscreen, ptHipDisplay, ptKneeDisplay, Constants.PS_SOLID, Color.FromArgb(255, 255, 0), 5);
            MyDrawLine(m_pDCOffscreen, ptKneeDisplay, ptAnkleDisplay, Constants.PS_SOLID, Color.FromArgb(255, 255, 0), 5);

            // Draw CURVES
            Pen penCurve = new Pen(Color.FromArgb(255, 0, 0), 2); //(PS_SOLID, 2, CURVES_COLOR);
                                                                  /*  CPen* ppenOld = NULL;
                                                                    if (ppenOld != NULL)
                                                                    {
                                                                        m_pDCOffscreen->SelectObject(ppenOld);
                                                                        ppenOld = NULL;
                                                                    }
                                                                    ppenOld = m_pDCOffscreen->SelectObject(&penCurve);*/

            Point pt1 = new Point(0, 0);
            Point pt2 = new Point(0, 0);
            Point pt3 = new Point(0, 0);
            Point[] pt = new Point[4];

            //#if !defined(TOYOTA)
            //‰EŒ¨ shoulder
            pt1.X = ptShoulderDisplay.X - (int)(m_iShoulderOffset0 * m_iOffscreenWidth / m_iSrcWidth * -1);
            pt1.Y = ptShoulderDisplay.Y - (int)(m_iShoulderOffset2 * m_iOffscreenHeight / m_iSrcHeight);
            pt2.X = ptShoulderDisplay.X;
            pt2.Y = ptShoulderDisplay.Y - (int)(m_iShoulderOffset2 * m_iOffscreenHeight / m_iSrcHeight);
            pt3.X = ptShoulderDisplay.X + (int)(m_iShoulderOffset1 * m_iOffscreenWidth / m_iSrcWidth * -1);
            pt3.Y = ptShoulderDisplay.Y;

            MyDrawLine(m_pDCOffscreen, pt1, pt2, Constants.PS_SOLID, /*CURVES_COLOR*/Color.FromArgb(255, 0, 0), 3);
            //#endif

            pt[0].X = pt2.X;
            pt[0].Y = pt2.Y;
            pt[1].X = (int)(pt3.X - 5 * m_iOffscreenWidth / m_iSrcWidth);
            pt[1].Y = (int)(pt2.Y - 5 * m_iOffscreenHeight / m_iSrcHeight);
            pt[2].X = (int)(pt3.X - 15 * m_iOffscreenWidth / m_iSrcWidth);
            pt[2].Y = (int)(pt3.Y + 50 * m_iOffscreenHeight / m_iSrcHeight);

            //#if !defined(TOYOTA)
            /*  m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
              m_pDCOffscreen->PolyBezierTo(pt, 3);*/
            m_pDCOffscreen.DrawBezier(penCurve, pt[0], pt[1], pt[2], pt[2]);
            //#endif

            //‰EŽ¨	
            pt[0].X = (int)(ptEarDisplay.X + 10 * m_iOffscreenWidth / m_iSrcWidth);
            pt[0].Y = (int)(ptEarDisplay.Y + 10 * m_iOffscreenHeight / m_iSrcHeight);
            pt[1].X = (int)(ptEarDisplay.X + 5 * m_iOffscreenWidth / m_iSrcWidth);
            pt[1].Y = (int)(ptEarDisplay.Y + 15 * m_iOffscreenHeight / m_iSrcHeight);
            pt[2].X = (int)(ptEarDisplay.X - 2 * m_iOffscreenWidth / m_iSrcWidth);
            pt[2].Y = (int)(ptEarDisplay.Y + 10 * m_iOffscreenHeight / m_iSrcHeight);

            pt1.X = pt[2].X;
            pt1.Y = pt[2].Y;
            Point Start = new Point(pt[0].X, pt[0].Y);
            /*  m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
              m_pDCOffscreen->PolyBezierTo(pt, 3);*/
            m_pDCOffscreen.DrawBezier(penCurve, pt[0], pt[1], pt[2], pt[2]);

            pt[0].X = (int)(ptEarDisplay.X - 7 * m_iOffscreenWidth / m_iSrcWidth);
            pt[0].Y = (int)(ptEarDisplay.Y - 5 * m_iOffscreenHeight / m_iSrcHeight);
            pt[1].X = (int)(ptEarDisplay.X - 2 * m_iOffscreenWidth / m_iSrcWidth);
            pt[1].Y = (int)(ptEarDisplay.Y - 18 * m_iOffscreenHeight / m_iSrcHeight);
            pt[2].X = (int)(ptEarDisplay.X + 5 * m_iOffscreenWidth / m_iSrcWidth);
            pt[2].Y = (int)(ptEarDisplay.Y - 10 * m_iOffscreenHeight / m_iSrcHeight);

            pt2.X = pt[0].X;
            pt2.Y = pt[0].Y;
            m_pDCOffscreen.DrawLine(penCurve, pt1, pt2);
            /*  m_pDCOffscreen->LineTo(pt2.x, pt2.y);
              m_pDCOffscreen->PolyBezierTo(pt, 3);*/
            m_pDCOffscreen.DrawBezier(penCurve, pt[0], pt[1], pt[2], pt[2]);
            /*
            #if 0
            #if !defined(TOYOTA)
                //‰E knee
                pt[0].x = int(ptKneeDisplay.x);
                pt[0].y = int(ptKneeDisplay.y - 20 * m_iOffscreenHeight / m_iSrcHeight );
                pt[1].x = int(ptKneeDisplay.x + 17 * m_iOffscreenWidth / m_iSrcWidth);
                pt[1].y = ptKneeDisplay.y;
                pt[2].x = int(ptKneeDisplay.x - 5 * m_iOffscreenWidth / m_iSrcWidth);
                pt[2].y = int(ptKneeDisplay.y + 20 * m_iOffscreenHeight / m_iSrcHeight);

                m_pDCOffscreen->MoveTo( int(ptKneeDisplay.x - 5 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeDisplay.y - 20 * m_iOffscreenHeight / m_iSrcHeight) );		
                m_pDCOffscreen->PolyBezierTo( pt, 3);

                m_pDCOffscreen->MoveTo( int(ptKneeDisplay.x - 18 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeDisplay.y - 13 * m_iOffscreenHeight / m_iSrcHeight));		
                m_pDCOffscreen->LineTo( int(ptKneeDisplay.x), int(ptKneeDisplay.y - 15 * m_iOffscreenHeight / m_iSrcHeight));	
                m_pDCOffscreen->MoveTo( int(ptKneeDisplay.x - 21 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeDisplay.y + 13 * m_iOffscreenHeight / m_iSrcHeight));		
                m_pDCOffscreen->LineTo( int(ptKneeDisplay.x - 5 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeDisplay.y + 15 * m_iOffscreenHeight / m_iSrcHeight));	
            #endif
            #endif
            */
            // Belt
            //#if !defined(TOYOTA)
            /*
             * m_pDCOffscreen->MoveTo(int(ptRightBeltDisplay.x - int(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth), int(ptRightBeltDisplay.y + int(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight));
            m_pDCOffscreen->LineTo(int(ptRightBeltDisplay.x + int(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth), int(ptRightBeltDisplay.y - int(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight));
            */
            m_pDCOffscreen.DrawLine(penCurve, (int)(ptRightBeltDisplay.X - (int)(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth),
                                    (int)(ptRightBeltDisplay.Y + (int)(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight),
                                    (int)(ptRightBeltDisplay.X + (int)(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth),
                                    (int)(ptRightBeltDisplay.Y - (int)(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight));

            //#endif
            MyDrawPoint(m_pDCOffscreen, (int)(ptRightBeltDisplay.X + 6 * m_iOffscreenWidth / m_iSrcWidth),
                (int)(ptRightBeltDisplay.Y /*+ 3 * m_iOffscreenHeight / m_iSrcHeight*/),
                m_Marker, Color.FromArgb(255, 0, 0), true);
            /*   if (ppenOld != NULL)
               {
                   m_pDCOffscreen->SelectObject(ppenOld);
                   ppenOld = NULL;
               }
               ppenOld = m_pDCOffscreen->SelectObject(&penCurve);*/

            //#if !defined(TOYOTA)
            /*   m_pDCOffscreen->MoveTo(int(ptLeftBeltDisplay.x - int(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth), int(ptLeftBeltDisplay.y + int(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight));
               m_pDCOffscreen->LineTo(int(ptLeftBeltDisplay.x + int(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth), int(ptLeftBeltDisplay.y - int(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight));
               */
            m_pDCOffscreen.DrawLine(penCurve, (int)(ptLeftBeltDisplay.X - (int)(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth),
                (int)(ptLeftBeltDisplay.Y + (int)(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight),
                (int)(ptLeftBeltDisplay.X + (int)(3 * 0.6) * m_iOffscreenWidth / m_iSrcWidth),
                (int)(ptLeftBeltDisplay.Y - (int)(12 * 0.6) * m_iOffscreenHeight / m_iSrcHeight));

            //#endif
            MyDrawPoint(m_pDCOffscreen, (int)(ptLeftBeltDisplay.X - 6 * m_iOffscreenWidth / m_iSrcWidth), (int)(ptLeftBeltDisplay.Y /*-3 * m_iOffscreenHeight / m_iSrcHeight*/), m_Marker, Color.FromArgb(255, 0, 0), true);
            /*   if (ppenOld != NULL)
               {
                   m_pDCOffscreen->SelectObject(ppenOld);
                   ppenOld = NULL;
               }
               ppenOld = m_pDCOffscreen->SelectObject(&penCurve);
           */
            // KneeBelt
            //  m_pDCOffscreen->MoveTo(int(ptKneeRightBeltDisplay.x /*- 3*5/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptKneeRightBeltDisplay.y + 12 * 5 / 2 * m_iOffscreenHeight / m_iSrcHeight));
            //  m_pDCOffscreen->LineTo(int(ptKneeRightBeltDisplay.x /*+ 3*5/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptKneeRightBeltDisplay.y - 12 * 5 / 2 * m_iOffscreenHeight / m_iSrcHeight));
            m_pDCOffscreen.DrawLine(penCurve, ptKneeRightBeltDisplay.X,
                (int)(ptKneeRightBeltDisplay.Y + 12 * 5 / 2 * m_iOffscreenHeight / m_iSrcHeight),
                ptKneeRightBeltDisplay.X,
                (int)(ptKneeRightBeltDisplay.Y - 12 * 5 / 2 * m_iOffscreenHeight / m_iSrcHeight)
                );
            MyDrawPoint(m_pDCOffscreen, (int)(ptKneeRightBeltDisplay.X + 6 * m_iOffscreenWidth / m_iSrcWidth), (int)(ptKneeRightBeltDisplay.Y /*+ 3 * m_iOffscreenHeight / m_iSrcHeight*/), m_Marker, Color.FromArgb(255, 0, 0), true);
            /*  if (ppenOld != NULL)
              {
                  m_pDCOffscreen->SelectObject(ppenOld);
                  ppenOld = NULL;
              }
              ppenOld = m_pDCOffscreen->SelectObject(&penCurve);
              */
            pt[0].X = (int)(ptKneeLeftBeltDisplay.X);
            pt[0].Y = (int)(ptKneeLeftBeltDisplay.Y - 20 * m_iOffscreenHeight / m_iSrcHeight);
            pt[1].X = (int)(ptKneeLeftBeltDisplay.X + 17 * m_iOffscreenWidth / m_iSrcWidth);
            pt[1].Y = ptKneeLeftBeltDisplay.Y;
            pt[2].X = (int)(ptKneeLeftBeltDisplay.X - 5 * m_iOffscreenWidth / m_iSrcWidth);
            pt[2].Y = (int)(ptKneeLeftBeltDisplay.Y + 20 * m_iOffscreenHeight / m_iSrcHeight);

            // m_pDCOffscreen->MoveTo(int(ptKneeLeftBeltDisplay.x - 5 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeLeftBeltDisplay.y - 20 * m_iOffscreenHeight / m_iSrcHeight));
            // m_pDCOffscreen->PolyBezierTo(pt, 3);
            m_pDCOffscreen.DrawBezier(penCurve, pt[0], pt[1], pt[2], pt[2]);

            //  m_pDCOffscreen->MoveTo(int(ptKneeLeftBeltDisplay.x - 18 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeLeftBeltDisplay.y - 13 * m_iOffscreenHeight / m_iSrcHeight));
            //  m_pDCOffscreen->LineTo(int(ptKneeLeftBeltDisplay.x), int(ptKneeLeftBeltDisplay.y - 15 * m_iOffscreenHeight / m_iSrcHeight));

            m_pDCOffscreen.DrawLine(penCurve, (int)(ptKneeLeftBeltDisplay.X - 18 * m_iOffscreenWidth / m_iSrcWidth),
                (int)(ptKneeLeftBeltDisplay.Y - 13 * m_iOffscreenHeight / m_iSrcHeight),
                (int)(ptKneeLeftBeltDisplay.X),
                (int)(ptKneeLeftBeltDisplay.Y - 15 * m_iOffscreenHeight / m_iSrcHeight)
                );
            //  m_pDCOffscreen->MoveTo(int(ptKneeLeftBeltDisplay.x - 21 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeLeftBeltDisplay.y + 13 * m_iOffscreenHeight / m_iSrcHeight));
            //  m_pDCOffscreen->LineTo(int(ptKneeLeftBeltDisplay.x /*- 5 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptKneeLeftBeltDisplay.y + 15 * m_iOffscreenHeight / m_iSrcHeight));
            m_pDCOffscreen.DrawLine(penCurve, (int)(ptKneeLeftBeltDisplay.X - 21 * m_iOffscreenWidth / m_iSrcWidth),
                (int)(ptKneeLeftBeltDisplay.Y + 13 * m_iOffscreenHeight / m_iSrcHeight),
                ptKneeLeftBeltDisplay.X,
                (int)(ptKneeLeftBeltDisplay.Y + 15 * m_iOffscreenHeight / m_iSrcHeight)
                );

            //#if 0
            //m_pDCOffscreen->MoveTo( int(ptKneeLeftBeltDisplay.x /*- 3*5/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptKneeLeftBeltDisplay.y + 12*5/2 * m_iOffscreenHeight / m_iSrcHeight));
            //m_pDCOffscreen->LineTo( int(ptKneeLeftBeltDisplay.x /*+ 3*5/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptKneeLeftBeltDisplay.y - 12*5/2 * m_iOffscreenHeight / m_iSrcHeight));
            //MyDrawPoint( m_pDCOffscreen, int(ptKneeLeftBeltDisplay.x - 6 * m_iOffscreenWidth / m_iSrcWidth), int(ptKneeLeftBeltDisplay.y -3 * m_iOffscreenHeight / m_iSrcHeight), m_Marker, CURVES_COLOR, true);
            //if ( ppenOld != NULL ){
            //	m_pDCOffscreen->SelectObject( ppenOld );
            //	ppenOld = NULL;
            //}
            //ppenOld = m_pDCOffscreen->SelectObject( &penCurve );
            //#endif

            // AnkleBelt
            // m_pDCOffscreen->MoveTo(int(ptAnkleRightBeltDisplay.x /*- 3*3/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptAnkleRightBeltDisplay.y + int(12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight));
            // m_pDCOffscreen->LineTo(int(ptAnkleRightBeltDisplay.x /*+ 3*3/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptAnkleRightBeltDisplay.y - int(12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight));

            m_pDCOffscreen.DrawLine(penCurve, ptAnkleRightBeltDisplay.X,
                (int)(ptAnkleRightBeltDisplay.Y + (int)(12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight),
                ptAnkleRightBeltDisplay.X,
                (int)(ptAnkleRightBeltDisplay.Y - (int)(12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight)
                );
            MyDrawPoint(m_pDCOffscreen, (int)(ptAnkleRightBeltDisplay.X + 6 * m_iOffscreenWidth / m_iSrcWidth), (int)(ptAnkleRightBeltDisplay.Y /*+ 3 * m_iOffscreenHeight / m_iSrcHeight*/), m_Marker, Color.FromArgb(255, 0, 0), true);
            /*
                if (ppenOld != NULL)
                {
                    m_pDCOffscreen->SelectObject(ppenOld);
                    ppenOld = NULL;
                }
                ppenOld = m_pDCOffscreen->SelectObject(&penCurve);
                */
            //    m_pDCOffscreen->MoveTo(int(ptAnkleLeftBeltDisplay.x /*- 3*3/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptAnkleLeftBeltDisplay.y + (12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight));
            //   m_pDCOffscreen->LineTo(int(ptAnkleLeftBeltDisplay.x /*+ 3*3/2 * m_iOffscreenWidth / m_iSrcWidth*/), int(ptAnkleLeftBeltDisplay.y - (12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight));

            m_pDCOffscreen.DrawLine(penCurve, ptAnkleLeftBeltDisplay.X,
                (int)(ptAnkleLeftBeltDisplay.Y + (12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight),
                ptAnkleLeftBeltDisplay.X,
                (int)(ptAnkleLeftBeltDisplay.Y - (12 * 3 / 2 * 0.9) * m_iOffscreenHeight / m_iSrcHeight)
                );
            MyDrawPoint(m_pDCOffscreen, (int)(ptAnkleLeftBeltDisplay.X - 6 * m_iOffscreenWidth / m_iSrcWidth), (int)(ptAnkleLeftBeltDisplay.Y /*-*/ + 3 * m_iOffscreenHeight / m_iSrcHeight), m_Marker, Color.FromArgb(255, 0, 0), true);
            /*  if (ppenOld != NULL)
              {
                  m_pDCOffscreen->SelectObject(ppenOld);
                  ppenOld = NULL;
              }
              ppenOld = m_pDCOffscreen->SelectObject(&penCurve);*/

            DrawArrowLine(m_pDCOffscreen, ptBenchmark1Display, ptBenchmark2Display, Constants.PS_SOLID, /*BENCHMARK_LINE_COLOR*/ Color.FromArgb(0, 255, 0), 5);

            // Draw JOINTS
            MyDrawPoint(m_pDCOffscreen, ptShoulderDisplay.X, ptShoulderDisplay.Y, /*int(*/m_Marker/**1.2)*/, Color.FromArgb(255, 0, 0), true);
            MyDrawPoint(m_pDCOffscreen, ptEarDisplay.X, ptEarDisplay.Y, /*int(*/m_Marker /*/1.5)*/, Color.FromArgb(255, 0, 0), true);
            MyDrawPoint(m_pDCOffscreen, ptKneeDisplay.X, ptKneeDisplay.Y, /*int(*/m_Marker /*/2)*/, Color.FromArgb(255, 0, 0), true);
            MyDrawPoint(m_pDCOffscreen, ptKneeLeftBeltDisplay.X, ptKneeLeftBeltDisplay.Y, /*int(*/m_Marker /*/2)*/, Color.FromArgb(255, 0, 0), true);
            MyDrawPoint(m_pDCOffscreen, ptHipDisplay.X, ptHipDisplay.Y, m_Marker, Color.FromArgb(255, 0, 0), true);
            MyDrawPoint(m_pDCOffscreen, ptAnkleDisplay.X, ptAnkleDisplay.Y, m_Marker, Color.FromArgb(255, 0, 0), true);
        }


        public void SetSrcPos(int iXPos, int iYPos)
        {
            m_iSrcX = iXPos;
            m_iSrcY = iYPos;
        }

        public void SetSrcSize(int iWidth, int iHeight)
        {
            m_iSrcWidth = iWidth;
            m_iSrcHeight = iHeight;
        }

        public void OnRButtonDown(MouseEventArgs point)
        {
            //CWnd::OnRButtonDown(nFlags, point);

            if (m_iMouseCaptureMode != 0)
            {
                return;
            }

            //SetCapture();
            m_iMouseCaptureMode = 2;

            //CRect rcClient;
            //GetClientRect(&rcClient);

            m_iMousePointXOnDragStart = point.X;
            m_iMousePointYOnDragStart = point.Y;
            m_iScreenWidthOnDragStart = 423;//rcClient.Width();
            m_iScreenHeightOnDragStart = 470;//rcClient.Height();
            m_iSrcXOnDragStart = m_iSrcX;
            m_iSrcYOnDragStart = m_iSrcY;
            m_iSrcWidthOnDragStart = m_iSrcWidth;
            m_iSrcHeightOnDragStart = m_iSrcHeight;
            m_iEditPtID = -1;
            m_iEditingBenchmarkBar = false;
            /*    if (GetParent() != NULL)
                {
                    GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                }
                */
        }

        public void OnRButtonUp(MouseEventArgs point)
        {
            // CWnd::OnRButtonUp(nFlags, point);
            if (m_iMouseCaptureMode != 2)
            {
                return;
            }
            //ReleaseCapture();
            m_iMouseCaptureMode = 0;
            m_iEditPtID = -1;
            m_iEditingBenchmarkBar = false;
            /*  if (GetParent() != NULL)
              {
                  GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
              }
              */
        }

        public void OnLButtonDown(MouseEventArgs point)
        {
            //  CWnd::OnLButtonDown(nFlags, point);

            if (m_iMouseCaptureMode != 0)
            {
                return;
            }
            m_iEditPtID = SearchEditPoint(point.Location, 15 * 15);
            if (m_iEditPtID == Constants.SIDEJOINTEDITWND_POINTID_NONE)
            {
                Point ptBenchmark1 = new Point(0, 0);
                Point ptBenchmark2 = new Point(0, 0);
                ptBenchmark1.X = (m_ptBenchmark1.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptBenchmark1.Y = (m_ptBenchmark1.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
                ptBenchmark2.X = (m_ptBenchmark2.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptBenchmark2.Y = (m_ptBenchmark2.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
                m_iEditingBenchmarkBar = IsPointInArrowShaft(point.Location, ptBenchmark1, ptBenchmark2, 5);
                if (!m_iEditingBenchmarkBar)
                {
                    // OutputDebugString("EditPoint = 0 \n");
                    return;
                }
                m_iEditingBenchmarkBar = true;
                //OutputDebugString("Editing Benchmarkbar \n");
            }
            else
            {
                // char szMessage[100];
                // wsprintf(&(szMessage[0]), "EditPoint = %d\n", m_iEditPtID);
                //OutputDebugString(&(szMessage[0]));
            }

            //SetCapture();
            m_iMouseCaptureMode = 1;

            // CRect rcClient;
            // GetClientRect(&rcClient);

            m_iMousePointXOnDragStart = point.X;
            m_iMousePointYOnDragStart = point.Y;
            m_iScreenWidthOnDragStart = 423;//rcClient.Width();
            m_iScreenHeightOnDragStart = 470;//rcClient.Height();
            m_iSrcXOnDragStart = m_iSrcX;
            m_iSrcYOnDragStart = m_iSrcY;
            m_iSrcWidthOnDragStart = m_iSrcWidth;
            m_iSrcHeightOnDragStart = m_iSrcHeight;
            m_ptBenchmark1OnDragStart = m_ptBenchmark1;
            m_ptBenchmark2OnDragStart = m_ptBenchmark2;
            /*
                if (GetParent() != NULL)
                {
                    GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                }
                */
        }

        public void OnLButtonUp(MouseEventArgs point)
        {
            // CWnd::OnLButtonUp(nFlags, point);
            if (m_iMouseCaptureMode != 1)
            {
                return;
            }
            // ReleaseCapture();
            m_iMouseCaptureMode = 0;
            m_iEditPtID = -1;
            m_iEditingBenchmarkBar = false;
            /*
    if (GetParent() != NULL)
    {
        GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
    }
    */
        }

        public void OnMouseMove(MouseEventArgs point)
        {
            if (m_iMouseCaptureMode == 1)
            {
                if (m_iEditPtID != Constants.SIDEJOINTEDITWND_POINTID_NONE)
                {
                    // ŠÖßˆÊ’uC³
                    Point ptEdit = new Point(0, 0);
                    ptEdit.X = point.X * m_iSrcWidth / m_iOffscreenWidth + m_iSrcX;
                    ptEdit.Y = point.Y * m_iSrcHeight / m_iOffscreenHeight + m_iSrcY;
                    if (ptEdit.X < 0)
                    {
                        ptEdit.X = 0;
                    }
                    if (ptEdit.Y < 0)
                    {
                        ptEdit.Y = 0;
                    }
                    if (ptEdit.X >= m_iBackgroundWidth)
                    {
                        ptEdit.X = m_iBackgroundWidth - 1;
                    }
                    if (ptEdit.Y >= m_iBackgroundHeight)
                    {
                        ptEdit.Y = m_iBackgroundHeight - 1;
                    }
                    switch (m_iEditPtID)
                    {
                        case Constants.SIDEJOINTEDITWND_POINTID_NONE:
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_EAR:
                            m_ptEar = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_SHOULDER:
                            m_ptShoulder = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_RIGHTBELT:
                            m_ptRightBelt = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_LEFTBELT:
                            m_ptLeftBelt = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_KNEE:
                            //m_ptKnee		= ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_ANKLE:
                            //m_ptAnkle		= ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK1:
                            m_ptBenchmark1 = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK2:
                            m_ptBenchmark2 = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_KNEE_RIGHTBELT:
                            m_ptKneeRightBelt = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_KNEE_LEFTBELT:
                            m_ptKneeLeftBelt = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_ANKLE_RIGHTBELT:
                            m_ptAnkleRightBelt = ptEdit;
                            break;
                        case Constants.SIDEJOINTEDITWND_POINTID_ANKLE_LEFTBELT:
                            m_ptAnkleLeftBelt = ptEdit;
                            break;
                        default:
                            break;
                    }
                }
                else if (m_iEditingBenchmarkBar)
                {
                    // Šî€“_Ú‘±üC³.
                    m_ptBenchmark1 = m_ptBenchmark1OnDragStart;
                    m_ptBenchmark2 = m_ptBenchmark2OnDragStart;
                    Point ptOffset1 = new Point(
                        (point.X - m_iMousePointXOnDragStart) * m_iSrcWidth / m_iOffscreenWidth,
                        (point.Y - m_iMousePointYOnDragStart) * m_iSrcHeight / m_iOffscreenHeight);
                    Point ptBecnchmark1Work1 = new Point(m_ptBenchmark1OnDragStart.X + ptOffset1.X,
                        m_ptBenchmark1OnDragStart.Y + ptOffset1.Y);
                    Point ptBecnchmark2Work1 = new Point(m_ptBenchmark2OnDragStart.X + ptOffset1.X,
                        m_ptBenchmark2OnDragStart.Y + ptOffset1.Y);
                    if (ptBecnchmark1Work1.X < 0)
                    {
                        ptBecnchmark1Work1.X = 0;
                    }
                    if (ptBecnchmark1Work1.Y < 0)
                    {
                        ptBecnchmark1Work1.Y = 0;
                    }
                    if (ptBecnchmark1Work1.X >= m_iBackgroundWidth)
                    {
                        ptBecnchmark1Work1.X = m_iBackgroundWidth;
                    }
                    if (ptBecnchmark1Work1.Y >= m_iBackgroundHeight)
                    {
                        ptBecnchmark1Work1.Y = m_iBackgroundHeight;
                    }
                    if (ptBecnchmark2Work1.X < 0)
                    {
                        ptBecnchmark2Work1.X = 0;
                    }
                    if (ptBecnchmark2Work1.Y < 0)
                    {
                        ptBecnchmark2Work1.Y = 0;
                    }
                    if (ptBecnchmark2Work1.X >= m_iBackgroundWidth)
                    {
                        ptBecnchmark2Work1.X = m_iBackgroundWidth;
                    }
                    if (ptBecnchmark2Work1.Y >= m_iBackgroundHeight)
                    {
                        ptBecnchmark2Work1.Y = m_iBackgroundHeight;
                    }
                    Point ptBecnchmark1Work2 = new Point(ptBecnchmark1Work1.X - m_ptBenchmark1OnDragStart.X,
                       ptBecnchmark1Work1.Y - m_ptBenchmark1OnDragStart.Y);
                    Point ptBecnchmark2Work2 = new Point(ptBecnchmark2Work1.X - m_ptBenchmark2OnDragStart.X,
                       ptBecnchmark2Work1.Y - m_ptBenchmark2OnDragStart.Y);
                    double[] adRatio = new double[4];
                    if (ptOffset1.X != 0)
                    {
                        adRatio[0] = (double)ptBecnchmark1Work2.X / ptOffset1.X;
                        adRatio[1] = (double)ptBecnchmark2Work2.X / ptOffset1.X;
                    }
                    else
                    {
                        adRatio[0] = 1.0;
                        adRatio[1] = 1.0;
                    }
                    if (ptOffset1.Y != 0)
                    {
                        adRatio[2] = (double)ptBecnchmark1Work2.Y / ptOffset1.Y;
                        adRatio[3] = (double)ptBecnchmark2Work2.Y / ptOffset1.Y;
                    }
                    else
                    {
                        adRatio[2] = 1.0;
                        adRatio[3] = 1.0;
                    }
                    double dMin = 1.0;
                    int i = 0;
                    for (i = 0; i < 4; i++)
                    {
                        if (adRatio[i] < dMin)
                        {
                            dMin = adRatio[i];
                        }
                    }
                    Point ptOffset2 = new Point((int)(dMin * ptOffset1.X), (int)(dMin * ptOffset1.Y));
                    m_ptBenchmark1.X = m_ptBenchmark1OnDragStart.X + ptOffset2.X;
                    m_ptBenchmark1.Y = m_ptBenchmark1OnDragStart.Y + ptOffset2.Y;
                    m_ptBenchmark2.X = m_ptBenchmark2OnDragStart.X + ptOffset2.X;
                    m_ptBenchmark2.Y = m_ptBenchmark2OnDragStart.Y + ptOffset2.Y;
                }
                else
                {
                    return;
                }
                /*   if (GetParent() != NULL)
                   {
                       GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                   }
                   */
            }
            else if (m_iMouseCaptureMode == 2)
            {
                int iDiffX = point.X - m_iMousePointXOnDragStart;
                int iDiffY = point.Y - m_iMousePointYOnDragStart;
                m_iSrcX = m_iSrcXOnDragStart - iDiffX * m_iSrcWidthOnDragStart / m_iScreenWidthOnDragStart;
                m_iSrcY = m_iSrcYOnDragStart - iDiffY * m_iSrcHeightOnDragStart / m_iScreenHeightOnDragStart;
                if (m_iSrcX < 0)
                {
                    m_iSrcX = 0;
                }
                if (m_iSrcY < 0)
                {
                    m_iSrcY = 0;
                }
                if (m_iSrcX + m_iSrcWidthOnDragStart >= m_iBackgroundWidth)
                {
                    m_iSrcX = m_iBackgroundWidth - m_iSrcWidthOnDragStart;
                }
                if (m_iSrcY + m_iSrcHeightOnDragStart >= m_iBackgroundHeight)
                {
                    m_iSrcY = m_iBackgroundHeight - m_iSrcHeightOnDragStart;
                }
                /*       if (GetParent() != NULL)
                       {
                           GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                       }
                       */
            }
            else
            {
                return;
            }

            // UpdateOffscreen();
            /* CRect rcClient;
             GetClientRect(&rcClient);
             InvalidateRect(&rcClient);
             UpdateWindow();*/
        }

        public void GetSideBodyPosition(ref SideBodyPosition SideBodyPosition)
        {
            Point ptHip = new Point(0, 0);
            ptHip.X = (m_ptRightBelt.X + m_ptLeftBelt.X) / 2;
            ptHip.Y = (m_ptRightBelt.Y + m_ptLeftBelt.Y) / 2;
            SideBodyPosition.SetHipPosition(ptHip);
            Point ptKnee = new Point(0, 0);
            ptKnee.X = (m_ptKneeRightBelt.X + m_ptKneeLeftBelt.X) / 2;
            ptKnee.Y = (m_ptKneeRightBelt.Y + m_ptKneeLeftBelt.Y) / 2;
            SideBodyPosition.SetKneePosition(ptKnee);
            Point ptAnkle = new Point(0, 0);
            ptAnkle.X = (m_ptAnkleRightBelt.X + m_ptAnkleLeftBelt.X) / 2;
            ptAnkle.Y = (m_ptAnkleRightBelt.Y + m_ptAnkleLeftBelt.Y) / 2;
            SideBodyPosition.SetAnklePosition(ptAnkle);

            SideBodyPosition.SetRightBeltPosition(m_ptRightBelt);
            SideBodyPosition.SetLeftBeltPosition(m_ptLeftBelt);
            SideBodyPosition.SetShoulderPosition(m_ptShoulder);
            SideBodyPosition.SetEarPosition(m_ptEar);

            SideBodyPosition.SetBenchmark1Position(m_ptBenchmark1);
            SideBodyPosition.SetBenchmark2Position(m_ptBenchmark2);

            SideBodyPosition.SetKneeRightBeltPosition(m_ptKneeRightBelt);
            SideBodyPosition.SetKneeLeftBeltPosition(m_ptKneeLeftBelt);
            SideBodyPosition.SetAnkleRightBeltPosition(m_ptAnkleRightBelt);
            SideBodyPosition.SetAnkleLeftBeltPosition(m_ptAnkleLeftBelt);
        }

        public void SetSideBodyPosition(SideBodyPosition SideBodyPosition)
        {
            //SideBodyPosition.GetKneePosition( m_ptKnee );
            //SideBodyPosition.GetAnklePosition( m_ptAnkle );
            //SideBodyPosition.GetRightBeltPosition(m_ptRightBelt);
            m_ptRightBelt = SideBodyPosition.m_ptRightBelt;
            //SideBodyPosition.GetLeftBeltPosition(m_ptLeftBelt);
            m_ptLeftBelt = SideBodyPosition.m_ptLeftBelt;
            //SideBodyPosition.GetShoulderPosition(m_ptShoulder);
            m_ptShoulder = SideBodyPosition.m_ptShoulder;
            //SideBodyPosition.GetEarPosition(m_ptEar);
            m_ptEar = SideBodyPosition.m_ptEar;
            //SideBodyPosition.GetBenchmark1Position(m_ptBenchmark1);
            m_ptBenchmark1 = SideBodyPosition.m_ptBenchmark1;
            //SideBodyPosition.GetBenchmark2Position(m_ptBenchmark2);
            m_ptBenchmark2 = SideBodyPosition.m_ptBenchmark2;

            //SideBodyPosition.GetKneeRightBeltPosition(m_ptKneeRightBelt);
            m_ptKneeRightBelt = SideBodyPosition.m_ptKneeRightBelt;
            //SideBodyPosition.GetKneeLeftBeltPosition(m_ptKneeLeftBelt);
            m_ptKneeLeftBelt = SideBodyPosition.m_ptKneeLeftBelt;
            //SideBodyPosition.GetAnkleRightBeltPosition(m_ptAnkleRightBelt);
            m_ptAnkleRightBelt = SideBodyPosition.m_ptAnkleRightBelt;
            //SideBodyPosition.GetAnkleLeftBeltPosition(m_ptAnkleLeftBelt);
            m_ptAnkleLeftBelt = SideBodyPosition.m_ptAnkleLeftBelt;
        }

        // st‚©‚çend‚Ü‚ÅaplliŽw’è‚Ì’¼ü‚ð•`‰æ
        public void MyDrawLineStyl(Graphics pDC, Point st, Point end)
        {
            MyDrawLine(pDC, st, end, (int)m_uiStyleLineStyle, m_crStyleLineColor, (int)m_uiStyleLineWidth);
        }

        // st‚©‚çend‚Ü‚ÅAüŽístyleAFrgbA‘¾‚³width‚Ì’¼ü‚ð•`‰æ
        public void MyDrawLine(Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
        {
            //	MoveToEx(hDC, st.x, st.y, NULL);
            //	LineTo(hDC, end.x, end.y);

            byte[] type = new byte[8];
            /*int c = CDashLine::GetPattern(type, false, width, style);
            CDashLine line(*pDC, type, c);
            CPen penNew(style, width, rgb);*/
            Pen penNew = new Pen(rgb, width);
            pDC.DrawLine(penNew, st, end);

            /*  CPen* ppenOld = pDC->SelectObject(&penNew);
              line.MoveTo(st.x, st.y);
              line.LineTo(end.x, end.y);

              pDC->SelectObject(ppenOld);*/
        }

        public void CalcArrowPolygonPoints(ref Point[] pptArrowHeadStart, ref Point[] pptArrowHeadEnd, ref Point[] pptArrowShaft, Point st, Point end, int width)
        {
            int iDiffX = end.X - st.X;
            int iDiffY = end.Y - st.Y;

            double dAngle0 = Math.Atan2((double)iDiffY, (double)iDiffX);
            double dAngle1 = dAngle0 + (3.141592 * 60.0) / 180.0;
            double dAngle2 = dAngle0 - (3.141592 * 60.0) / 180.0;

            Point ptArrowHeadVec1 = new Point((int)(m_Marker * 3 * Math.Cos(dAngle1)), (int)(m_Marker * 3 * Math.Sin(dAngle1)));
            Point ptArrowHeadVec2 = new Point((int)(m_Marker * 3 * Math.Cos(dAngle2)), (int)(m_Marker * 3 * Math.Sin(dAngle2)));

            pptArrowHeadStart[0] = new Point(st.X + ptArrowHeadVec1.X, st.Y + ptArrowHeadVec1.Y);
            pptArrowHeadStart[1] = new Point(st.X, st.Y);
            pptArrowHeadStart[2] = new Point(st.X + ptArrowHeadVec2.X, st.Y + ptArrowHeadVec2.Y);

            pptArrowHeadEnd[0] = new Point(end.X - ptArrowHeadVec1.X, end.Y - ptArrowHeadVec1.Y);
            pptArrowHeadEnd[1] = new Point(end.X, end.Y);
            pptArrowHeadEnd[2] = new Point(end.X - ptArrowHeadVec2.X, end.Y - ptArrowHeadVec2.Y);

            Point ptArrowHeadStartCenter = new Point(pptArrowHeadStart[0].X + pptArrowHeadStart[2].X,
                pptArrowHeadStart[0].Y + pptArrowHeadStart[2].Y);
            Point ptArrowHeadEndCenter = new Point(pptArrowHeadEnd[0].X + pptArrowHeadEnd[2].X,
                pptArrowHeadEnd[0].Y + pptArrowHeadEnd[2].Y);
            ptArrowHeadStartCenter.X /= 2;
            ptArrowHeadStartCenter.Y /= 2;
            ptArrowHeadEndCenter.X /= 2;
            ptArrowHeadEndCenter.Y /= 2;

            Point ptArrowHeadVec3 = new Point(ptArrowHeadVec2.X - ptArrowHeadVec1.X,
                ptArrowHeadVec2.Y - ptArrowHeadVec1.Y);
            double dNorm = Math.Sqrt((double)(ptArrowHeadVec3.X * ptArrowHeadVec3.X + ptArrowHeadVec3.Y * ptArrowHeadVec3.Y));
            ptArrowHeadVec3.X = (int)((double)width * ptArrowHeadVec3.X / (2 * dNorm));
            ptArrowHeadVec3.Y = (int)((double)width * ptArrowHeadVec3.Y / (2 * dNorm));

            pptArrowShaft[0] = new Point(ptArrowHeadStartCenter.X + ptArrowHeadVec3.X,
                ptArrowHeadStartCenter.Y + ptArrowHeadVec3.Y);
            pptArrowShaft[1] = new Point(ptArrowHeadEndCenter.X + ptArrowHeadVec3.X,
                ptArrowHeadEndCenter.Y + ptArrowHeadVec3.Y);
            pptArrowShaft[2] = new Point(ptArrowHeadEndCenter.X - ptArrowHeadVec3.X,
                ptArrowHeadEndCenter.Y - ptArrowHeadVec3.Y);
            pptArrowShaft[3] = new Point(ptArrowHeadStartCenter.X - ptArrowHeadVec3.X,
                ptArrowHeadStartCenter.Y - ptArrowHeadVec3.Y);
        }

        public bool IsPointInArrowShaft(Point ptCheck, Point st, Point end, int width)
        {
            Point[] aptArrowHeadStart = new Point[3];
            Point[] aptArrowHeadEnd = new Point[3];
            Point[] aptArrowShaft = new Point[4];


            CalcArrowPolygonPoints(ref (aptArrowHeadStart), ref (aptArrowHeadEnd), ref (aptArrowShaft), st, end, width);

            int i = 0;
            for (i = 0; i < 4; i++)
            {
                Point ptWork1 = new Point(aptArrowShaft[(i + 1) % 4].X - aptArrowShaft[i].X,
                    aptArrowShaft[(i + 1) % 4].Y - aptArrowShaft[i].Y);
                Point ptWork2 = new Point(ptCheck.X - aptArrowShaft[i].X,
                    ptCheck.Y - aptArrowShaft[i].Y);
                if (ptWork1.X * ptWork2.Y - ptWork2.X * ptWork1.Y < 0)
                {
                    return false;
                }
            }
            return true;
        }

        public void DrawArrowLine(Graphics pDC, Point st, Point end, int style /* = PS_SOLID */, Color rgb /* = RGB(0,0,0)*/, int width /* =1 */ )
        {
            Point[] aptArrowHeadStart = new Point[3];
            Point[] aptArrowHeadEnd = new Point[3];
            Point[] aptArrowShaft = new Point[4];

            CalcArrowPolygonPoints(ref (aptArrowHeadStart), ref (aptArrowHeadEnd), ref (aptArrowShaft), st, end, width);

            Pen penNew = new Pen(rgb, width);
            SolidBrush brNew = new SolidBrush(rgb);
            /*
                CPen* ppenOld = pDC->SelectObject(&penNew);
                CBrush* pbrOld = pDC->SelectObject(&brNew);
                */

            pDC.FillPolygon(brNew, aptArrowHeadStart);
            pDC.FillPolygon(brNew, aptArrowHeadEnd);
            pDC.FillPolygon(brNew, aptArrowShaft);
            /*
                if (ppenOld != NULL)
                {
                    pDC->SelectObject(ppenOld);
                    ppenOld = NULL;
                }
                if (pbrOld != NULL)
                {
                    pDC->SelectObject(pbrOld);
                    pbrOld = NULL;
                }
                */
        }

        // ’†Sx,yA”¼ŒasizeAFrgbA‚Ì“_‚ð•`‰æ
        // fill‚ªtrue‚Ì‚Æ‚«‚Í“h‚è‚Â‚Ô‚µ
        public void MyDrawPoint(Graphics pDC, int x, int y, int size, Color rgb /* =RGB(0,0,0) */, bool fill /*=false*/)
        {
            Pen penNew = new Pen(rgb);
            SolidBrush brNew = new SolidBrush(rgb);
            /*
                CPen* ppenOld = pDC->SelectObject(&penNew);
                CBrush* pbrOld = NULL;
                HBRUSH hbrOld = NULL;*/
            if (fill)
            {
                // pbrOld = pDC->SelectObject(&brNew);
                pDC.FillEllipse(brNew, x - size / 2, y - size / 2, size + 1, size + 1);
            }
            else
            {
                //hbrOld = (HBRUSH)(pDC->SelectObject((HBRUSH)GetStockObject(NULL_BRUSH)));
                pDC.DrawEllipse(penNew, x - size / 2, y - size / 2, size + 1, size + 1);
            }
            //pDC->Ellipse(x - size, y - size, x + size + 1, y + size + 1);
            /*
            if (ppenOld != NULL)
            {
                pDC->SelectObject(ppenOld);
                ppenOld = NULL;
            }
            if (pbrOld != NULL)
            {
                pDC->SelectObject(pbrOld);
                pbrOld = NULL;
            }
            if (hbrOld != NULL)
            {
                pDC->SelectObject(hbrOld);
                hbrOld = NULL;
            }
            */
        }

        public int SearchEditPoint(Point pt, int iDistanceLimit)
        {
            int[] aiPointID = new int[Constants.SIDEJOINTEDITWND_POINTID_MAX];
            Point[] aptDisplay = new Point[Constants.SIDEJOINTEDITWND_POINTID_MAX];
            int iPointCount = 0;

            aptDisplay[iPointCount].X = (m_ptEar.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptEar.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_EAR;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptShoulder.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptShoulder.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_SHOULDER;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptRightBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptRightBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_RIGHTBELT;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptLeftBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptLeftBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_LEFTBELT;
            iPointCount++;

            //aptDisplay[iPointCount].x	= ( m_ptKnee.x - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
            //aptDisplay[iPointCount].y	= ( m_ptKnee.y - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
            //aiPointID[iPointCount]		= SIDEJOINTEDITWND_POINTID_KNEE;
            //iPointCount++;

            //aptDisplay[iPointCount].x	= ( m_ptAnkle.x - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
            //aptDisplay[iPointCount].y	= ( m_ptAnkle.y - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
            //aiPointID[iPointCount]		= SIDEJOINTEDITWND_POINTID_ANKLE;
            //iPointCount++;

            aptDisplay[iPointCount].X = (m_ptBenchmark1.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptBenchmark1.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK1;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptBenchmark2.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptBenchmark2.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK2;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptKneeRightBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptKneeRightBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_KNEE_RIGHTBELT;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptKneeLeftBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptKneeLeftBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_KNEE_LEFTBELT;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptAnkleRightBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptAnkleRightBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_ANKLE_RIGHTBELT;
            iPointCount++;

            aptDisplay[iPointCount].X = (m_ptAnkleLeftBelt.X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            aptDisplay[iPointCount].Y = (m_ptAnkleLeftBelt.Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
            aiPointID[iPointCount] = Constants.SIDEJOINTEDITWND_POINTID_ANKLE_LEFTBELT;
            iPointCount++;

            int iShortestDistance = iDistanceLimit;
            int iRet = Constants.SIDEJOINTEDITWND_POINTID_NONE;
            int i = 0;
            for (i = 0; i < iPointCount; i++)
            {
                int iDiffX = aptDisplay[i].X - pt.X;
                int iDiffY = aptDisplay[i].Y - pt.Y;
                int iDistance = iDiffX * iDiffX + iDiffY * iDiffY;
                if (iDistance <= iShortestDistance)
                {
                    iRet = aiPointID[i];
                    iShortestDistance = iDistance;
                }
            }

            return iRet;
        }

        public int GetSrcX()
        {
            return m_iSrcX;
        }

        public int GetSrcY()
        {
            return m_iSrcY;
        }

        public int GetSrcWidth()
        {
            return m_iSrcWidth;
        }

        public int GetSrcHeight()
        {
            return m_iSrcHeight;
        }

        public int GetEditPtID()
        {
            return m_iEditPtID;
        }

        public int GetBackgroundWidth()
        {
            return m_iBackgroundWidth;
        }

        public int GetBackgroundHeight()
        {
            return m_iBackgroundHeight;
        }

        public void SetMarkerSizeBase(int iMarkerSize)
        {
            m_iMarkerSizeBase = iMarkerSize;
        }

        public void SetDataVersion(int iDataVersion)
        {
            m_iDataVersion = iDataVersion;
        }

        public void SetShoulderOffset0(int iShoulderOffset0)
        {
            m_iShoulderOffset0 = iShoulderOffset0;
        }

        public void SetShoulderOffset1(int iShoulderOffset1)
        {
            m_iShoulderOffset1 = iShoulderOffset1;
        }

        public void SetShoulderOffset2(int iShoulderOffset2)
        {
            m_iShoulderOffset2 = iShoulderOffset2;
        }

        public void SetStyleLine(bool bStyleLine)
        {
            m_bStyleLine = bStyleLine;
        }

        public void SetStyleLineStyle(uint uiStyleLineStyle)
        {
            m_uiStyleLineStyle = uiStyleLineStyle;
        }

        public void SetStyleLineColor(Color crStyleLineColor)
        {
            m_crStyleLineColor = crStyleLineColor;
        }

        public void SetStyleLineWidth(uint uiStyleLineWidth)
        {
            m_uiStyleLineWidth = uiStyleLineWidth;
        }

        public bool IsEditingBenchmarkBar()
        {
            return m_iEditingBenchmarkBar;
        }



    }
}
