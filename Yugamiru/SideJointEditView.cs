﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class SideJointEditView : Form
    {
        PictureBox m_MainPic = new PictureBox();
        SideJointEditWnd m_wndSidePosture = new SideJointEditWnd();
        SideBodyPosition m_SideBodyPosition = new SideBodyPosition();

        byte[] m_pbyteBits;
        JointEditDoc m_JointEditDoc;
        int m_ZoomFlag = 0;

        public void DisposeControls()
        {
            m_MainPic.Image.Dispose();

            IDC_Mag1Btn.Image.Dispose();
            IDC_Mag2Btn.Image.Dispose();
            IDC_ResetImgBtn.Image.Dispose();

            IDC_OkBtn.Image.Dispose();
            IDC_CancelBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic5.Dispose();

            Yugamiru.Properties.Resources.mag2.Dispose();
            Yugamiru.Properties.Resources.mag1.Dispose();
            Yugamiru.Properties.Resources.mag3_down.Dispose();

            Yugamiru.Properties.Resources.completegreen_on.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_on.Dispose();

            this.Dispose();
            this.Close();

        }

        public SideJointEditView(JointEditDoc GetDocument)
        {
            InitializeComponent();
            label1.Font = new Font("MS UI Gothic", 12, FontStyle.Regular);
            label2.Font = new Font("MS UI GOTHIC", 12, FontStyle.Regular);

            m_MainPic.Size = new Size(Yugamiru.Properties.Resources.Mainpic5.Size.Width,
                Yugamiru.Properties.Resources.Mainpic5.Size.Height);
            m_MainPic.BackColor = Color.Transparent;
            this.Controls.Add(m_MainPic);
            m_MainPic.Image = Yugamiru.Properties.Resources.Mainpic5;

            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;
            if (GetDocument.GetInputMode() == Constants.INPUTMODE_NEW)
            {
                IDC_OkBtn.Image = Yugamiru.Properties.Resources.completered_on;
                IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;

            }

            if (GetDocument.GetInputMode() == Constants.INPUTMODE_MODIFY)
            {

                IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                IDC_OkBtn.Image = Yugamiru.Properties.Resources.completered_on;

            }


            GetDocument.GetSideBodyPosition(ref m_SideBodyPosition);
            m_wndSidePosture.SetSideBodyPosition(m_SideBodyPosition);


            m_pbyteBits = new byte[1024 * 1280];
            m_wndSidePosture.SetBackgroundBitmap(1024, 1280, m_pbyteBits);
            SetFOV();



            m_wndSidePosture.SetDataVersion(GetDocument.GetDataVersion());
            m_wndSidePosture.SetMarkerSizeBase(GetDocument.GetMarkerSize());
            m_wndSidePosture.SetShoulderOffset0(GetDocument.GetShoulderOffset0());
            m_wndSidePosture.SetShoulderOffset1(GetDocument.GetShoulderOffset1());
            m_wndSidePosture.SetShoulderOffset2(GetDocument.GetShoulderOffset2());
            m_wndSidePosture.SetStyleLine(GetDocument.IsValidStyleLine());
            m_wndSidePosture.SetStyleLineStyle(GetDocument.GetStyleLineStyle());
            m_wndSidePosture.SetStyleLineColor(GetDocument.GetStyleLineColor());
            m_wndSidePosture.SetStyleLineWidth(GetDocument.GetStyleLineWidth());



            label1.Text = Yugamiru.Properties.Resources.SIDE_VIEW_MODE;//"Drag each icons in side view.";
            label2.Text = "";
            label3.Text = Yugamiru.Properties.Resources.DISTANCE_BETWEEN_REFERENCE;

            GetDocument.LoadSetup2File();

            string strBenchmarkDistance;
            strBenchmarkDistance = GetDocument.GetBenchmarkDistance().ToString();

            textBox1.Text = strBenchmarkDistance;
            m_JointEditDoc = GetDocument;
        }

        private void SideJointEditView_Load(object sender, EventArgs e)
        {


        }

        public void SideJointEditView_SizeChanged(object sender, EventArgs e)
        {
            m_MainPic.Left = (this.ClientSize.Width - m_MainPic.Width) / 2;
            //m_MainPic.Top = (this.ClientSize.Height - m_MainPic.Height) / 2;
            m_MainPic.Top = 0;
            pictureBox1.Size = new Size(423, 470);
            pictureBox1.Location = new Point(550 - 110, 200 - 48);
            pictureBox1.Left = (this.Width - pictureBox1.Width) / 2 - 34;

            label1.Location = new Point(250, 40);
            label1.Left = pictureBox1.Left - 220;
            label2.Location = new Point(250, 80);
            label2.Left = label1.Left;//(this.Width - label2.Width) / 2 - 330;

            IDC_Mag1Btn.Size = new Size(48, 48);
            IDC_Mag1Btn.Location = new Point(1110, 250);
            IDC_Mag1Btn.Left = pictureBox1.Left + pictureBox1.Width + 240;

            panel1.Size = new Size(42, 192);
            panel1.Location = new Point(1110, 300);
            panel1.Left = pictureBox1.Left + pictureBox1.Width + 240;

            IDC_Slider1.Size = new Size(36, 192);
            //IDC_Slider1.Location = new Point(1115, 300);

            IDC_Mag2Btn.Size = new Size(48, 48);
            IDC_Mag2Btn.Location = new Point(1110, 500);
            IDC_Mag2Btn.Left = pictureBox1.Left + pictureBox1.Width + 240;


            IDC_ResetImgBtn.Size = new Size(48, 48);
            IDC_ResetImgBtn.Location = new Point(1110, 550);
            IDC_ResetImgBtn.Left = pictureBox1.Left + pictureBox1.Width + 240;

            IDC_OkBtn.Size = new Size(112, 42);
            IDC_OkBtn.Location = new Point(880 + 100, 575 + 60);
            IDC_OkBtn.Left = pictureBox1.Left + pictureBox1.Width + 100;

            label3.Size = new Size(200, 26);
            label3.Location = new Point(410, 575 + 64);
            label3.Left = pictureBox1.Left;

            textBox1.Size = new Size(60, 26);
            textBox1.Location = new Point(620, 575 + 64);
            textBox1.Left = label3.Left + label3.Width + 10;

            label4.Size = new Size(100, 26);
            label4.Location = new Point(740, 575 + 64);
            label4.Left = textBox1.Left + textBox1.Width + 10;

            IDC_CancelBtn.Size = new Size(112, 42);
            IDC_CancelBtn.Location = new Point(350 - 100, 575 + 60);
            IDC_CancelBtn.Left = pictureBox1.Left - 240;

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            m_wndSidePosture.m_pbyteBits = m_JointEditDoc.m_SideImageBytes;
            m_wndSidePosture.UpdateOffscreen(e.Graphics);
            /*SetEditPointMessage(m_wndSidePosture.GetEditPtID(), m_wndSidePosture.IsEditingBenchmarkBar());
            m_JointEditDoc.SetSideMarkerTouchFlag(m_wndSidePosture.GetEditPtID());
            if (m_wndSidePosture.IsEditingBenchmarkBar())
            {
                m_JointEditDoc.SetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK1);
                m_JointEditDoc.SetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK2);
            } -- by Meena*/
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            m_wndSidePosture.OnMouseMove(e);
            pictureBox1.Invalidate();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                m_wndSidePosture.OnLButtonDown(e);
            else if (e.Button == MouseButtons.Right)
                m_wndSidePosture.OnRButtonDown(e);

            pictureBox1.Invalidate();
            SetEditPointMessage(m_wndSidePosture.GetEditPtID(), m_wndSidePosture.IsEditingBenchmarkBar());
            m_JointEditDoc.SetSideMarkerTouchFlag(m_wndSidePosture.GetEditPtID());
            if (m_wndSidePosture.IsEditingBenchmarkBar())
            {
                m_JointEditDoc.SetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK1);
                m_JointEditDoc.SetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK2);
            }

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                m_wndSidePosture.OnLButtonUp(e);
            else if (e.Button == MouseButtons.Right)
                m_wndSidePosture.OnRButtonUp(e);
            pictureBox1.Invalidate();
            SetEditPointMessage(m_wndSidePosture.GetEditPtID(), false);


        }
        public void SetFOV()
        {
            int iBackgroundWidth = m_wndSidePosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndSidePosture.GetBackgroundHeight();
            int iSrcWidth = iBackgroundWidth;
            int iSrcHeight = iBackgroundHeight;
            int iSrcX = 0;
            int iSrcY = 0;
            double dImgMag = Constants.IMG_SCALE_MIN * 100;


            dImgMag = 3.5;//1.5; Modified by Meena
            iSrcWidth = iBackgroundWidth * 2 / 3;
            iSrcHeight = iBackgroundHeight * 2 / 3;
            iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            iSrcY = 307;

            m_wndSidePosture.SetSrcPos(iSrcX, iSrcY);
            m_wndSidePosture.SetSrcSize(iSrcWidth, iSrcHeight);


            IDC_Slider1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
            IDC_Slider1.TickFrequency = 50;
            IDC_Slider1.Value = (int)(Constants.IMG_SCALE_MAX * 100 +
                Constants.IMG_SCALE_MIN * 100 - dImgMag * 100);
        }
        public void UpdatePictureScale(int iSliderPos)
        {
            int iSrcX = m_wndSidePosture.GetSrcX();
            int iSrcY = m_wndSidePosture.GetSrcY();
            int iSrcWidth = m_wndSidePosture.GetSrcWidth();
            int iSrcHeight = m_wndSidePosture.GetSrcHeight();

            int iSrcCenterX = iSrcX + iSrcWidth / 2;
            int iSrcCenterY = iSrcY + iSrcHeight / 2;


            int Val = 400 - (iSliderPos - 100);
            double dImgMag = (Constants.IMG_SCALE_MAX + Constants.IMG_SCALE_MIN) - Val / 100.0;
            if (dImgMag < Constants.IMG_SCALE_MIN)
            {
                dImgMag = Constants.IMG_SCALE_MIN;
            }
            else if (dImgMag > Constants.IMG_SCALE_MAX)
            {
                dImgMag = Constants.IMG_SCALE_MAX;
            }
            int iNewSrcWidth = (int)(m_wndSidePosture.GetBackgroundWidth() / dImgMag);
            int iNewSrcHeight = (int)(m_wndSidePosture.GetBackgroundHeight() / dImgMag);

            int iNewSrcX = iSrcCenterX - iNewSrcWidth / 2;
            int iNewSrcY = iSrcCenterY - iNewSrcHeight / 2;
            if (iNewSrcX < 0)
            {
                iNewSrcX = 0;
            }
            if (iNewSrcY < 0)
            {
                iNewSrcY = 0;
            }
            if (iNewSrcX + iNewSrcWidth >= m_wndSidePosture.GetBackgroundWidth())
            {
                iNewSrcX = m_wndSidePosture.GetBackgroundWidth() - 1 - iNewSrcWidth;
            }
            if (iNewSrcY + iNewSrcHeight >= m_wndSidePosture.GetBackgroundHeight())
            {
                iNewSrcY = m_wndSidePosture.GetBackgroundHeight() - 1 - iNewSrcHeight;
            }
            m_wndSidePosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndSidePosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);


            pictureBox1.Invalidate();
        }
        void ZoomOutbtn()
        {
            int scale = IDC_Slider1.Value;
            if (scale <= 100)
                return;
            if ((scale - 50) < 100)
                IDC_Slider1.Value = 100;
            else
                IDC_Slider1.Value = scale - 50;

            UpdatePictureScale(scale - 50);
        }

        //@Šg‘åƒ{ƒ^ƒ“
        void ZoomInbtn()
        {
            int scale = IDC_Slider1.Value;
            if (scale >= 400)
                return;
            if ((scale + 50) > 400)
                IDC_Slider1.Value = 400;
            else
                IDC_Slider1.Value = scale + 50;
            UpdatePictureScale(scale + 50);
        }

        //@Œ³‚É–ß‚·ƒ{ƒ^ƒ“
        void ZoomResetbtn()
        {
            SetFOV();
            int scale = IDC_Slider1.Value;
            UpdatePictureScale(scale);
        }

        private void IDC_Mag1Btn_Click(object sender, EventArgs e)
        {
            m_ZoomFlag = Constants.ZOOMOUT;
            timer1.Interval = 100;
            timer1.Start();
            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_down;

            ZoomInbtn();
        }

        private void IDC_Mag2Btn_Click(object sender, EventArgs e)
        {
            m_ZoomFlag = Constants.ZOOMIN;
            timer1.Interval = 100;
            timer1.Start();
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_down;

            ZoomOutbtn();
        }

        private void IDC_ResetImgBtn_Click(object sender, EventArgs e)
        {
            m_ZoomFlag = Constants.RESET;
            timer1.Interval = 100;
            timer1.Start();
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_down;

            ZoomResetbtn();
        }

        private void IDC_Slider1_Scroll(object sender, EventArgs e)
        {
            int scale = IDC_Slider1.Value;
            UpdatePictureScale(scale);
        }

        private void IDC_CancelBtn_Click(object sender, EventArgs e)
        {
            IDC_OkBtn.Image = Yugamiru.Properties.Resources.completegreen_up;
            IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackred_on;

            String strBenchmarkDistance;
            strBenchmarkDistance = textBox1.Text;
            int iBenchmarkDistance = Int32.Parse(strBenchmarkDistance);
            m_JointEditDoc.SetBenchmarkDistance(iBenchmarkDistance);
            //m_JointEditDoc.SaveSetup2File();

            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_UPPERBODY);
            //m_JointEditDoc.ChangeToJointEditView();
            /* Function_GoBackToJointEditView(EventArgs.Empty);
             this.Close();
             DisposeControls();*/
            this.Visible = false;
            m_JointEditDoc.GetJointEditView().Visible = true;

        }
        void SetEditPointMessage(int iEditPointID, bool bIsEditingBenchmarkBar)
        {
            switch (iEditPointID)
            {
                case Constants.SIDEJOINTEDITWND_POINTID_NONE:
                    if (bIsEditingBenchmarkBar)
                    {
                        //SetDlgItemText(IDC_MESSAGE2, "–îˆó‚ðˆÚ“®‚µ‚ÄA–îˆó—¼’[ŠÔ‚ÌŽÀÛ‹——£‚ð´ÃÞ¨¯ÄÎÞ¯¸½“à‚Ì”’l‚É‚ ‚í‚¹‚Ä‚­‚¾‚³‚¢B");
                        //"The distance between arrow edges should be equal to the value in edit box. "
                        label2.Text = Yugamiru.Properties.Resources.ARROW_POSITION;
                    }
                    else
                    {

                        label2.Text = "";
                    }
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_KNEE:     //@•G
                    //"This icon should be positioned on knee ( coneter of patellas ).
                    label2.Text = Yugamiru.Properties.Resources.SIDE_KNEE_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_ANKLE:        //@‘«Žñ
                    //"This icon should be positioned on ankle. "
                    label2.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_RIGHTBELT:        //@ƒxƒ‹ƒg

                    //"This icon should be positioned on the end of belt.
                    label2.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_LEFTBELT:
                    /*//@ƒxƒ‹ƒg

"This icon should be positioned on the end of belt.*/
                    label2.Text = Yugamiru.Properties.Resources.BELT_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_SHOULDER:     //@Œ¨

                    // This icon should be positioned on contour of shoulder.

                    label2.Text = Yugamiru.Properties.Resources.SIDE_SHOULDER_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_EAR:  //@Ž¨
                    //This icon should be positioned on contour of ear
                    label2.Text = Yugamiru.Properties.Resources.EAR_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK1:
                    //SetDlgItemText(IDC_MESSAGE2, "–îˆó‚ðˆÚ“®‚µ‚ÄA–îˆó—¼’[ŠÔ‚ÌŽÀÛ‹——£‚ð´ÃÞ¨¯ÄÎÞ¯¸½“à‚Ì”’l‚É‚ ‚í‚¹‚Ä‚­‚¾‚³‚¢B");
                    label2.Text = Yugamiru.Properties.Resources.ARROW_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK2:
                    //SetDlgItemText(IDC_MESSAGE2, "–îˆó‚ðˆÚ“®‚µ‚ÄA–îˆó—¼’[ŠÔ‚ÌŽÀÛ‹——£‚ð´ÃÞ¨¯ÄÎÞ¯¸½“à‚Ì”’l‚É‚ ‚í‚¹‚Ä‚­‚¾‚³‚¢B");
                    label2.Text = Yugamiru.Properties.Resources.ARROW_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_KNEE_RIGHTBELT:
                    //SetDlgItemText(IDC_MESSAGE2, "‚±‚ÌƒAƒCƒRƒ“‚Í•Gƒxƒ‹ƒg‚Ì’[‚É‡‚í‚¹‚Ä‰º‚³‚¢B");
                    label2.Text = Yugamiru.Properties.Resources.SIDE_KNEE_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_KNEE_LEFTBELT:
                    //SetDlgItemText(IDC_MESSAGE2, "‚±‚ÌƒAƒCƒRƒ“‚Í•Gƒxƒ‹ƒg‚Ì’[‚É‡‚í‚¹‚Ä‰º‚³‚¢B");
                    label2.Text = Yugamiru.Properties.Resources.SIDE_KNEE_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_ANKLE_RIGHTBELT:
                    //SetDlgItemText(IDC_MESSAGE2, "‚±‚ÌƒAƒCƒRƒ“‚Í‘«Žñƒxƒ‹ƒg‚Ì’[‚É‡‚í‚¹‚Ä‰º‚³‚¢B");
                    label2.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                    break;
                case Constants.SIDEJOINTEDITWND_POINTID_ANKLE_LEFTBELT:
                    //SetDlgItemText(IDC_MESSAGE2, "‚±‚ÌƒAƒCƒRƒ“‚Í‘«Žñƒxƒ‹ƒg‚Ì’[‚É‡‚í‚¹‚Ä‰º‚³‚¢B");
                    label2.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;
                    break;
                default:
                    //SetDlgItemText(IDC_MESSAGE2, "‰EƒNƒŠƒbƒN‚µ‚È‚ª‚çƒ}ƒEƒX‚ð“®‚©‚·‚ÆAŽÊ^‚ð“®‚©‚·‚±‚Æ‚ª‚Å‚«‚Ü‚·");
                    label2.Text = "";
                    break;
            }
        }
        public event EventHandler GoBackToJointEditView; // creating event handler - step1
        public void Function_GoBackToJointEditView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = GoBackToJointEditView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }


        private void IDC_OkBtn_Click(object sender, EventArgs e)
        {
            IDC_OkBtn.Image = Yugamiru.Properties.Resources.completered_on;
            IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            string strWarning = string.Empty;
            if (GetUntouchedMarkerWarning(ref strWarning))
            {
                DialogResult result = MessageBox.Show(strWarning, "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }

            }
            SideBodyPosition temp_SideBodyPosition = new SideBodyPosition(m_SideBodyPosition);
            //m_JointEditDoc.GetSideBodyPosition(ref temp_SideBodyPosition);
            m_wndSidePosture.GetSideBodyPosition(ref temp_SideBodyPosition);
            m_JointEditDoc.SetSideBodyPosition(temp_SideBodyPosition);

            //m_JointEditDoc.SetSideBodyPositionObj(temp_SideBodyPosition);

            string strBenchmarkDistance;
            if (textBox1.Text != "")
            {
                strBenchmarkDistance = textBox1.Text;

                int iBenchmarkDistance = Int32.Parse(strBenchmarkDistance);
                m_JointEditDoc.SetBenchmarkDistance(iBenchmarkDistance);
            }


            //m_JointEditDoc.SaveSetup2File();

            // GetDocument()->ChangeToResultView();
            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_NONE);
            m_JointEditDoc.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_TRUE);
            FunctionToChangeResultView(EventArgs.Empty);
            this.Visible = false;
            //this.Close();
            //DisposeControls();

        }
        public bool GetUntouchedMarkerWarning(ref string strWarning)
        {
            if (m_JointEditDoc.GetInputMode() != Constants.INPUTMODE_NEW)
            {
                return false;
            }
            string strPart = string.Empty;
            if (!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_EAR)))
            {
                strPart += Yugamiru.Properties.Resources.EAR + " ";//"ear ";
            }
            if (!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_SHOULDER)))
            {
                strPart += Yugamiru.Properties.Resources.SHOULDER + " ";//"shoulder ";

            }
            if ((!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_RIGHTBELT))) ||
                (!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_LEFTBELT))))
            {
                strPart += Yugamiru.Properties.Resources.BELT + " ";//"belt ";

            }
            //if ( !(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_KNEE )) ){
            if ((!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_KNEE_RIGHTBELT))) ||
                 (!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_KNEE_LEFTBELT))))
            {
                strPart += Yugamiru.Properties.Resources.KNEE + " ";//"knee ";

            }
            //if ( !(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_ANKLE )) ){
            if ((!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_ANKLE_RIGHTBELT))) ||
                 (!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_ANKLE_LEFTBELT))))
            {
                strPart += Yugamiru.Properties.Resources.ANKLE + " ";//"ankle ";

            }
            if ((!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK1))) ||
                (!(m_JointEditDoc.GetSideMarkerTouchFlag(Constants.SIDEJOINTEDITWND_POINTID_BENCHMARK2))))
            {
                strPart += Yugamiru.Properties.Resources.BASE_ARROW;//"base arrow ";

            }
            if (strPart == string.Empty)
            {
                return false;
            }
            strWarning = Yugamiru.Properties.Resources.WARNING4 +
                        /*"There are unmoved icons. proceed anyway?*/"\n" + strPart;
            return true;
        }

        public event EventHandler EventToChangeResultView; // creating event handler - step1
        public void FunctionToChangeResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToChangeResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void SideJointEditView_Paint(object sender, PaintEventArgs e)
        {


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (m_ZoomFlag)
            {
                case Constants.ZOOMOUT:
                    IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
                    break;
                case Constants.ZOOMIN:
                    IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
                    break;
                case Constants.RESET:
                    IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;
                    break;
            }
        }
        public void RefreshForms()
        {
            m_MainPic.Image = Yugamiru.Properties.Resources.Mainpic5;
            label1.Text = Yugamiru.Properties.Resources.SIDE_VIEW_MODE;//"Drag each icons in side view.";
            label3.Text = Yugamiru.Properties.Resources.DISTANCE_BETWEEN_REFERENCE;

            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;
            if (m_JointEditDoc.GetInputMode() == Constants.INPUTMODE_NEW)
            {
                m_JointEditDoc.GetSideBodyPosition(ref m_SideBodyPosition);
                m_wndSidePosture.SetSideBodyPosition(m_SideBodyPosition);


                m_pbyteBits = new byte[1024 * 1280];
                m_wndSidePosture.SetBackgroundBitmap(1024, 1280, m_pbyteBits);
                SetFOV();
                IDC_OkBtn.Image = Yugamiru.Properties.Resources.completered_on;
                IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;


            }
            if (m_JointEditDoc.GetInputMode() == Constants.INPUTMODE_MODIFY)
            {
                m_JointEditDoc.GetSideBodyPosition(ref m_SideBodyPosition);
                m_wndSidePosture.SetSideBodyPosition(m_SideBodyPosition);


                m_pbyteBits = new byte[1024 * 1280];
                m_wndSidePosture.SetBackgroundBitmap(1024, 1280, m_pbyteBits);
                SetFOV();

                IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                IDC_OkBtn.Image = Yugamiru.Properties.Resources.completered_on;

            }
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);
        }

    }
}
