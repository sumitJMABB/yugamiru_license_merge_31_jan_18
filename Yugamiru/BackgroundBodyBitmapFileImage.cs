﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public class BackgroundBodyBitmapFileImage
    {
        int m_iSize;
        byte[] m_pbyteFileImage;
        int m_Width;
        int m_Height;

        public BackgroundBodyBitmapFileImage()
        {

            m_iSize = 0;

            m_pbyteFileImage = new byte[m_iSize];
        
        }

        
public bool Load(Image pchFileName)
{
    if (m_pbyteFileImage != null)
    {       
        m_pbyteFileImage = null;
    }
    m_iSize = 0;
            m_Width = pchFileName.Width;
            m_Height = pchFileName.Height;

            ImageConverter _imageConverter = new ImageConverter();
            m_pbyteFileImage = (byte[])_imageConverter.ConvertTo(pchFileName, typeof(byte[]));
            
    
    //m_pbyteFileImage = new byte[iSize];
    
    m_iSize = m_pbyteFileImage.Length;

           
            return true;
}
/*
BOOL CBackgroundBodyBitmapFileImage::Save( const char* pchFileName) const
{
	FILE* fp = fopen(pchFileName, "wb");
	if ( fp == NULL ){
		return FALSE;
	}
	if ( fwrite( &(m_pbyteFileImage[0]), 1, m_iSize, fp ) < (size_t)m_iSize ){

        fclose(fp );
fp = NULL;
		return FALSE;
	}

    fclose(fp );
fp = NULL;
	return TRUE;
}
*/
public int GetWidth() 
{
	if ( m_pbyteFileImage == null ){
		return 0;
	}
/*	if ( m_iSize< sizeof( BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) ){
		return 0;
	}
	BITMAPINFOHEADER* pbih = (BITMAPINFOHEADER*)(m_pbyteFileImage + sizeof(BITMAPFILEHEADER));*/


	return (m_Width);
}

public int GetHeight() 
{
	if ( m_pbyteFileImage == null ){
		return 0;
	}
/*	if ( m_iSize< sizeof( BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) ){
		return 0;
	}
	BITMAPINFOHEADER* pbih = (BITMAPINFOHEADER*)(m_pbyteFileImage + sizeof(BITMAPFILEHEADER));*/
	return (m_Height );
}

public int GetSize() 
{
	return m_iSize;
}

public bool ApplyMuscleBitmapFileImage( MuscleBitmapFileImage MuscleBitmapFileImage, int iRed, int iGreen, int iBlue, int iRed2, int iGreen2, int iBlue2)
{
    int iWidth = GetWidth();
    int iHeight = GetHeight();
    if (GetWidth() != MuscleBitmapFileImage.GetWidth())
    {
        return false;
    }
    if (GetHeight() != MuscleBitmapFileImage.GetHeight())
    {
        return false;
    }
            int iHeaderSize = 54;/*sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)*/;
    int iDstLineStep = (GetWidth() * 3 + 3) / 4 * 4;
    int iSrcLineStep = (MuscleBitmapFileImage.GetWidth() + 3) / 4 * 4;
    int i = 0;
    int j = 0;
    int iDstColor = 0;
    int iSrcColor = 0;
    int iTmpColor = 0;
    for (i = 0; i < iHeight; i++)
    {
        for (j = 0; j < iWidth; j++)
        {
            iSrcColor = MuscleBitmapFileImage.GetImageData(i * iSrcLineStep + j);

            iDstColor = m_pbyteFileImage[iHeaderSize + i * iDstLineStep + j * 3 + 0];
            if (iSrcColor > 0)
            {
                if (iDstColor < 128)
                {
                    iTmpColor = iDstColor * iBlue * 2 / 255;
                }
                else
                {
                    iTmpColor = 2 * (iDstColor + iBlue - iDstColor * iBlue / 255) - 255;
                }
                iTmpColor = iTmpColor * iBlue2 / 255;
                iDstColor = iDstColor * (255 - iSrcColor) / 255 + iSrcColor * iTmpColor / 255;
            }
            m_pbyteFileImage[iHeaderSize + i * iDstLineStep + j * 3 + 0] = (byte)iDstColor;

            iDstColor = m_pbyteFileImage[iHeaderSize + i * iDstLineStep + j * 3 + 1];
            if (iSrcColor > 0)
            {
                if (iDstColor < 128)
                {
                    iTmpColor = iDstColor * iGreen * 2 / 255;
                }
                else
                {
                    iTmpColor = 2 * (iDstColor + iGreen - iDstColor * iGreen / 255) - 255;
                }
                iTmpColor = iTmpColor * iGreen2 / 255;
                iDstColor = iDstColor * (255 - iSrcColor) / 255 + iSrcColor * iTmpColor / 255;
            }
            m_pbyteFileImage[iHeaderSize + i * iDstLineStep + j * 3 + 1] = (byte)iDstColor;

            iDstColor = m_pbyteFileImage[iHeaderSize + i * iDstLineStep + j * 3 + 2];
            if (iSrcColor > 0)
            {
                if (iDstColor < 128)
                {
                    iTmpColor = iDstColor * iRed * 2 / 255;
                }
                else
                {
                    iTmpColor = 2 * (iDstColor + iRed - iDstColor * iRed / 255) - 255;
                }
                iTmpColor = iTmpColor * iRed2 / 255;
                iDstColor = iDstColor * (255 - iSrcColor) / 255 + iSrcColor * iTmpColor / 255;
            }
            m_pbyteFileImage[iHeaderSize + i * iDstLineStep + j * 3 + 2] = (byte)iDstColor;
        }
    }
    return true;
}

public Image Draw(Graphics pDC, int iDestX, int iDestY) 
{
	if ( m_pbyteFileImage == null ){
		return null;
	}
            MemoryStream ms = new MemoryStream(m_pbyteFileImage);
            Image returnImage = Image.FromStream(ms);
            Rectangle DestRect = new Rectangle(iDestX- 30, iDestY + 100, 400 - 10 , 300 + 50 +20);
            Rectangle SrcRect = new Rectangle(0, 0, returnImage.Width, returnImage.Height);
            //using (returnImage)
            {
                pDC.DrawImage(returnImage, DestRect, SrcRect, GraphicsUnit.Pixel);
            }
            return returnImage;
	/*BITMAPINFO bmi;
	::memcpy( &(bmi.bmiHeader), m_pbyteFileImage + sizeof(BITMAPFILEHEADER), sizeof(BITMAPINFOHEADER) );
	void* pvBits = m_pbyteFileImage + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	::SetDIBitsToDevice(pDC->GetSafeHdc(), iDestX, iDestY, 
		bmi.bmiHeader.biWidth, bmi.bmiHeader.biHeight, 0, 0, 0, bmi.bmiHeader.biHeight, pvBits, &bmi, DIB_RGB_COLORS );*/
}
/*
void CBackgroundBodyBitmapFileImage::Draw(CDIB &dibDst, int iDestX, int iDestY) const
{
	if ( m_pbyteFileImage == NULL ){
		return;
	}
	CDIB dibTmp;
dibTmp.LoadFromMemoryImage( m_pbyteFileImage, (DWORD)m_iSize );
	dibDst.Blt( iDestX, iDestY, dibTmp );
}
*/

    }
}
