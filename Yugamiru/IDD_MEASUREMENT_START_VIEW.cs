﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.IO;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public partial class IDD_MEASUREMENT_START_VIEW : Form
    {
        bool m_ShootBtnFlag = true;
        bool m_RotateFlag = false;
        bool m_ReturnFlag = false;
        bool m_SearchFlag = false;
        enum MY_STATE
        {
            MY_STATE_STANDING = 0,
            MY_STATE_KNEE_DOWN,
            MY_STATE_SIDE,
            MY_STATE_END
        };


        ImageClipWnd m_ImageClipWnd = new ImageClipWnd();
        ImagePreviewWnd m_ImagePreviewWnd = new ImagePreviewWnd();

        PictureBox picturebox1 = new PictureBox();

        Bitmap bmBack1;
        JointEditDoc m_JointEditDoc;
        Image<Bgr,byte> m_currentImage;

        public IDD_MEASUREMENT_START_VIEW(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;

            GetDocument.CreateBmpInfo(1280 * 1024 * 3, 1024, 1280);

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;


            IDC_ID.Text = GetDocument.GetDataID().ToString();
            IDC_Name.Text = GetDocument.GetDataName();

            string str = "-";
            if (GetDocument.GetDataGender() == 1) str = "M";
            if (GetDocument.GetDataGender() == 2) str = "F";

            IDC_Gender.Text = str;
            string year = string.Empty, month = string.Empty, day = string.Empty;
            GetDocument.GetDataDoB(ref year, ref month, ref day);
            str = month + "." + day + " " + year;

            IDC_DoB.Text = str;
            if (GetDocument.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = GetDocument.GetDataHeight().ToString(); // g’·

            IDC_Height.Text = str;
            string strInstitutionName = string.Empty;
            GetDocument.GetInstitutionName(strInstitutionName);



            bmBack1 = Yugamiru.Properties.Resources.Mainpic3;
            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
            picturebox1.BackColor = Color.Transparent;
            this.Controls.Add(picturebox1);
            picturebox1.Image = bmBack1;
            IDC_NextBtn.Visible = false;


            switch (GetDocument.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    SetSideStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    SetFrontStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    SetFrontKneedownMode();
                    break;
                default:
                    break;
            }

        }
        public void reload()
        {
            IDC_ID.Text = m_JointEditDoc.GetDataID().ToString();
            IDC_Name.Text = m_JointEditDoc.GetDataName();

            string str = "-";
            if (m_JointEditDoc.GetDataGender() == 1)
            {
                if (m_JointEditDoc.GetLanguage() == "English" || m_JointEditDoc.GetLanguage() == "Thai")
                    str = "M";
                if (m_JointEditDoc.GetLanguage() == "Chinese" || m_JointEditDoc.GetLanguage() == "Japanese")
                    str = "男";
                if (m_JointEditDoc.GetLanguage() == "Korean")
                    str = "남성";
            }

            if (m_JointEditDoc.GetDataGender() == 2)
            {
                if (m_JointEditDoc.GetLanguage() == "English" || m_JointEditDoc.GetLanguage() == "Thai")
                    str = "F";
                if (m_JointEditDoc.GetLanguage() == "Chinese" || m_JointEditDoc.GetLanguage() == "Japanese")
                    str = "女";
                if (m_JointEditDoc.GetLanguage() == "Korean")
                    str = "여성";
            }


            IDC_Gender.Text = str;
            string year = string.Empty, month = string.Empty, day = string.Empty;
            m_JointEditDoc.GetDataDoB(ref year, ref month, ref day);
            str = month + "." + day + " " + year;

            IDC_DoB.Text = str;
            if (m_JointEditDoc.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = m_JointEditDoc.GetDataHeight().ToString(); // g’·

            IDC_Height.Text = str;
        }
        public void DisposeControls()
        {
            this.picturebox1.Image.Dispose();

            this.IDC_BackBtn.Image.Dispose();
            this.IDC_NextBtn.Image.Dispose();
            this.IDC_RotateBtn.Image.Dispose();
            this.IDC_SearchBtn.Image.Dispose();
            this.IDC_ShootBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic3.Dispose();

            Yugamiru.Properties.Resources.imagecopy_on.Dispose();
            Yugamiru.Properties.Resources.imageload_on.Dispose();
            Yugamiru.Properties.Resources.imagerotation_on.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_on.Dispose();
            Yugamiru.Properties.Resources.gonextgreen_on.Dispose();

            Yugamiru.Properties.Resources.SpeechFrontStanding.Dispose();

            this.Dispose();
            this.Close();

        }

        public void IDD_MEASUREMENT_START_VIEW_SizeChanged(object sender, EventArgs e)
        {

            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            picturebox1.Top = 0;

            IDC_ID.Location = new Point(230 + 10, 50 - 10);
            IDC_ID.Size = new Size(90, 24);
            IDC_ID.Font = new Font("HP Simplified Light", 10);

            IDC_Name.Location = new Point(150 + 230 + 10, 50 - 10);
            IDC_Name.Size = new Size(167, 24);
            IDC_Name.Font = new Font("HP Simplified Light", 10);

            IDC_Gender.Location = new Point(300 + 230 + 20, 50 - 10);

            IDC_Gender.Size = new Size(34, 24);
            IDC_Gender.Font = new Font("HP Simplified Light", 10);

            IDC_DoB.Location = new Point(450 + 230 - 50, 50 - 10);
            IDC_DoB.Size = new Size(126, 24);
            IDC_DoB.Font = new Font("HP Simplified Light", 10);

            IDC_Height.Location = new Point(600 + 230 - 30, 50 - 10);
            IDC_Height.Size = new Size(50, 24);
            IDC_Height.Font = new Font("HP Simplified Light", 10);

            IDC_BackBtn.Location = new Point(220, 550 + 80);
            IDC_BackBtn.Size = new Size(112, 42);
            IDC_SearchBtn.Location = new Point(IDC_BackBtn.Left + IDC_BackBtn.Width + 20, 550 + 80);
            IDC_SearchBtn.Size = new Size(112, 42);
            IDC_RotateBtn.Location = new Point(IDC_SearchBtn.Left + IDC_SearchBtn.Width + 20, 550 + 80);
            IDC_RotateBtn.Size = new Size(112, 42);
            IDC_NextBtn.Location = new Point(900, 550 + 80);
            IDC_NextBtn.Size = new Size(112, 42);
            IDC_ShootBtn.Location = new Point(600 + 26, 250);
            IDC_ShootBtn.Size = new Size(112, 42);


            ImagePrevWnd.Top = 130;
            ImagePrevWnd.Size = new Size(384, 480);

            ImageClipWnd.Top = 130;
            ImageClipWnd.Size = new Size(384, 480);

            IDC_ID.Left = (this.Width - 74) / 2 - 400; // 74 --> default width of label for 10 characters
            IDC_Name.Left = IDC_ID.Left + 126;
            IDC_Gender.Left = IDC_Name.Left + 170 + 6;
            IDC_DoB.Left = IDC_Gender.Left + 56 + 8;
            IDC_Height.Left = IDC_DoB.Left + 180;

            IDC_ShootBtn.Left = (this.Width - IDC_ShootBtn.Width) / 2;
            IDC_ShootBtn.Top = (this.Height - IDC_ShootBtn.Height) / 2;
            ImageClipWnd.Left = (this.Width - ImageClipWnd.Width) / 2 - 264;
            ImagePrevWnd.Left = ImageClipWnd.Left + ImageClipWnd.Width + IDC_ShootBtn.Width + 30;

            IDC_BackBtn.Left = ImageClipWnd.Left - 10;
            IDC_SearchBtn.Left = IDC_BackBtn.Left + IDC_BackBtn.Width + 20;
            IDC_RotateBtn.Left = IDC_SearchBtn.Left + IDC_SearchBtn.Width + 20;
            IDC_NextBtn.Left = ImagePrevWnd.Left + ImagePrevWnd.Width - IDC_NextBtn.Width;

            pictureBox2.Left = IDC_Height.Left + 120 + 3;
            pictureBox2.Top = 30 + 5 - 2;

            m_ImagePreviewWnd.m_iOffscreenWidth = ImagePrevWnd.Width;
            m_ImagePreviewWnd.m_iOffscreenHeight = ImagePrevWnd.Height;

            m_ImageClipWnd.m_iOffscreenWidth = ImagePrevWnd.Width;
            m_ImageClipWnd.m_iOffscreenHeight = ImagePrevWnd.Height;

        }
       

        public void SetSideStandingMode()
        {
            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            pictureBox2.Visible = false;

            if (m_JointEditDoc.m_SideImageBytes == null)
            {
                //string strScoresheetFolderPath;
                //m_JointEditDoc.GetScoresheetFolderPath(strScoresheetFolderPath);
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.SetSideClipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui));
            }
            else
            {
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                Image<Bgr, byte> EmguCVSideImage = new Image<Bgr, byte>(1024,1280);
                EmguCVSideImage.Bytes = m_JointEditDoc.m_SideImageBytes;

                m_JointEditDoc.SetSideClipImage(EmguCVSideImage.ToBitmap());
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocSideImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);

            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        if(m_JointEditDoc.m_SideImageBytes == null)
                        IDC_NextBtn.Visible = false;
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        
                        if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                            IDC_BackBtn.Visible = true;
                        else
                            IDC_BackBtn.Visible = false;
                        IDC_NextBtn.Visible = true;
                        
                    }
                    break;
                default:
                    break;
            }
        }

        public void SetFrontStandingMode()
        {

            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            pictureBox2.Visible = true;
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;


            if (m_JointEditDoc.m_FrontStandingImageBytes == null)
            {
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.SetStandClipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui));
            }
            else
            {
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);

                Image<Bgr, byte> EmguCVStandImage = new Image<Bgr, byte>(1024, 1280);
                EmguCVStandImage.Bytes = m_JointEditDoc.m_FrontStandingImageBytes;

                m_JointEditDoc.SetStandClipImage(EmguCVStandImage.ToBitmap());
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocStandingImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING);
            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();
            
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                       // if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                        //    IDC_BackBtn.Visible = true;
                        //else
                            IDC_BackBtn.Visible = true;
                        IDC_BackBtn.Image = Properties.Resources.gobackgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                default:
                    break;
            }
        }


        public void SetFrontKneedownMode()
        {
            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            pictureBox2.Visible = true;
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;


            if (m_JointEditDoc.m_FrontKneedownImageBytes == null)
            {

                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.SetKneeClipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui));

            }
            else
            {
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);

                Image<Bgr, byte> EmguCVKneedownImage = new Image<Bgr, byte>(1024, 1280);
                EmguCVKneedownImage.Bytes = m_JointEditDoc.m_FrontKneedownImageBytes;

                m_JointEditDoc.SetKneeClipImage(EmguCVKneedownImage.ToBitmap());


                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocKneedownImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN);
            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        //if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                            //IDC_BackBtn.Visible = true;
                       // else
                            IDC_BackBtn.Visible = true;

                        IDC_BackBtn.Image = Properties.Resources.gobackgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                default:
                    break;
            }
        }


        private void ImageClipWnd_SizeChanged(object sender, EventArgs e)
        {
        }

        private void ImageClipWnd_Paint(object sender, PaintEventArgs e)
        {

            if (m_RotateFlag)
                m_ImageClipWnd.RotateClockwiseBackgroundBitmap(e.Graphics);

            m_ImageClipWnd.UpdateOffscreen(e.Graphics);
        }

        private void ImageClipWnd_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_ImageClipWnd.m_iMouseCaptureMode > 0)
            {
                int iSelectionFrameCenterX = (e.X - m_ImageClipWnd.m_iDestRectUpperLeftCornerX) *
                    m_ImageClipWnd.m_iBackgroundWidth / m_ImageClipWnd.m_iDestRectWidth;
                if (iSelectionFrameCenterX < 0)
                {
                    iSelectionFrameCenterX = 0;
                }
                if (iSelectionFrameCenterX >= m_ImageClipWnd.m_iBackgroundWidth)
                {
                    iSelectionFrameCenterX = m_ImageClipWnd.m_iBackgroundWidth;
                }
                m_ImageClipWnd.m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX -
                    m_ImageClipWnd.m_iSelectionFrameWidth / 2;
                ImageClipWnd.Invalidate();





            }

        }

        private void ImageClipWnd_MouseUp(object sender, MouseEventArgs e)
        {
            m_ImageClipWnd.m_iMouseCaptureMode = 0;
        }

        private void ImageClipWnd_MouseDown(object sender, MouseEventArgs e)
        {
            m_ImageClipWnd.m_iMouseCaptureMode = 1;
            int iSelectionFrameCenterX = (e.X - m_ImageClipWnd.m_iDestRectUpperLeftCornerX) *
                m_ImageClipWnd.m_iBackgroundWidth / m_ImageClipWnd.m_iDestRectWidth;
            if (iSelectionFrameCenterX < 0)
            {
                iSelectionFrameCenterX = 0;
            }
            if (iSelectionFrameCenterX >= m_ImageClipWnd.m_iBackgroundWidth)
            {
                iSelectionFrameCenterX = m_ImageClipWnd.m_iBackgroundWidth;
            }
            m_ImageClipWnd.m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX -
                m_ImageClipWnd.m_iSelectionFrameWidth / 2;

        }

        private void IDC_ShootBtn_Click(object sender, EventArgs e)
        {
            IDC_NextBtn.Visible = true;
            m_ShootBtnFlag = true;
            ImagePrevWnd.Invalidate();

            byte[] pbyteBits = new byte[1024 * 1280 * 3];
            Bitmap bmp = new Bitmap(ImagePrevWnd.ClientSize.Width, ImagePrevWnd.ClientSize.Height);
            ImagePrevWnd.DrawToBitmap(bmp, ImagePrevWnd.ClientRectangle);
            Image<Bgr, byte> EmguCVPreviewImage = new Image<Bgr, byte>(bmp);
            EmguCVPreviewImage = EmguCVPreviewImage.Resize(1024, 1280, Emgu.CV.CvEnum.Inter.Linear);
            pbyteBits = EmguCVPreviewImage.Bytes;

            switch (m_JointEditDoc.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    m_JointEditDoc.SetSideClipImage(EmguCVPreviewImage.ToBitmap());
                    m_JointEditDoc.AllocSideImage(pbyteBits);
                    
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    m_JointEditDoc.SetStandClipImage(EmguCVPreviewImage.ToBitmap());
                    m_JointEditDoc.AllocStandingImage(pbyteBits);                    
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    m_JointEditDoc.SetKneeClipImage(EmguCVPreviewImage.ToBitmap());
                    m_JointEditDoc.AllocKneedownImage(pbyteBits);
                    break;
                default:
                    break;

            }

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_on;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;



        }

        private void IDC_SearchBtn_Click(object sender, EventArgs e)
        {
            m_SearchFlag = true;
            m_ShootBtnFlag = false;
            IMAGE_FILE_SELECT m_OpenDialog = new IMAGE_FILE_SELECT(m_JointEditDoc);
            if (m_OpenDialog.ShowDialog() == DialogResult.OK)
            {
                //m_currentImage = new Bitmap(m_JointEditDoc.m_Image_Path);
                m_currentImage = new Image<Bgr, byte>(m_JointEditDoc.m_Image_Path);
                if (m_currentImage.Width % 2 != 0) // check for odd number
                    m_currentImage = m_currentImage.Resize(m_currentImage.Width - 1, m_currentImage.Height, Emgu.CV.CvEnum.Inter.Linear);
                if (m_currentImage.Width == 1366) //1366--> default laptop width, its creating issue since 1366/2 = 683 which is a odd number
                    m_currentImage = m_currentImage.Resize(m_currentImage.Width - 2, m_currentImage.Height, Emgu.CV.CvEnum.Inter.Linear);
                //m_JointEditDoc.SetFlipImage(m_currentImage.ToBitmap());                
                switch (m_JointEditDoc.GetMeasurementStartViewMode())
                {
                    case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                        m_JointEditDoc.SetSideClipImage(m_currentImage.ToBitmap());
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                        m_JointEditDoc.SetStandClipImage(m_currentImage.ToBitmap());
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                        m_JointEditDoc.SetKneeClipImage(m_currentImage.ToBitmap());
                        break;
                    default:
                        break;
                }

                m_ImageClipWnd.SetBackgroundBitmap(m_currentImage);//m_JointEditDoc.m_Image_Path));

                ImageClipWnd.Invalidate();
            }
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_on;
            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
        }



        private void IDC_BackBtn_Click(object sender, EventArgs e)
        {
            m_ReturnFlag = true;
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                // ŒÂlî•ñ‰æ–Ê‚É‘JˆÚ‚·‚é.
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                //m_JointEditDoc.ChangeToMeasurementView();
                                /*  CloseForm(EventArgs.Empty);
                                  this.Close();
                                  DisposeControls();*/
                                this.Visible = false;
                                m_JointEditDoc.GetMeasurementDlg().Visible = true;
                                m_JointEditDoc.GetMeasurementDlg().RefreshForm();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                IDC_NextBtn.Visible = true;
                                SetSideStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                IDC_NextBtn.Visible = true;
                                SetFrontStandingMode();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                // ‚±‚±‚É‚­‚é‚±‚Æ‚Í‚È‚¢.
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                //m_JointEditDoc.ChangeToMeasurementView();
                                /*  CloseForm(EventArgs.Empty);
                                  this.Close();
                                  DisposeControls();*/
                                this.Visible = false;
                                m_JointEditDoc.GetMeasurementDlg().Visible = true;
                                m_JointEditDoc.GetMeasurementDlg().RefreshForm();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                SetSideStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                SetFrontStandingMode();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_on;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);

        }


        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_NextBtn_Click(object sender, EventArgs e)
        {
            //m_ReturnFlag = false;
            
          /*  byte[] pbyteBits = new byte[1024 * 1280 * 3];
            Bitmap bmp = new Bitmap(ImagePrevWnd.ClientSize.Width, ImagePrevWnd.ClientSize.Height);
            ImagePrevWnd.DrawToBitmap(bmp, ImagePrevWnd.ClientRectangle);
            Image<Bgr, byte> EmguCVPreviewImage = new Image<Bgr, byte>(bmp);
            EmguCVPreviewImage = EmguCVPreviewImage.Resize(1024, 1280, Emgu.CV.CvEnum.Inter.Linear);
            pbyteBits = EmguCVPreviewImage.Bytes;
            */
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                               /* if (m_ShootBtnFlag)
                                {
                                    m_JointEditDoc.SetSideClipImage(EmguCVPreviewImage.ToBitmap());
                                    m_JointEditDoc.AllocSideImage(pbyteBits);
                                }*/
                                
                                SetFrontStandingMode();
                                IDC_NextBtn.Visible = false;
                                if (m_JointEditDoc.m_FrontStandingImageBytes != null)
                                    IDC_NextBtn.Visible = true;
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                              /*  if (m_ShootBtnFlag)
                                {
                                    m_JointEditDoc.SetStandClipImage(EmguCVPreviewImage.ToBitmap());
                                    m_JointEditDoc.AllocStandingImage(pbyteBits);
                                }*/
                                SetFrontKneedownMode();
                                IDC_NextBtn.Visible = false;
                                if (m_JointEditDoc.m_FrontKneedownImageBytes != null)
                                    IDC_NextBtn.Visible = true;
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                               /* if (m_ShootBtnFlag)
                                {
                                    m_JointEditDoc.SetKneeClipImage(EmguCVPreviewImage.ToBitmap());
                                    m_JointEditDoc.AllocKneedownImage(pbyteBits);
                                }*/
                                m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);
                                m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);                                
                                this.Visible = false;
                                m_JointEditDoc.GetJointEditView().Visible = true;
                                m_JointEditDoc.GetJointEditView().RefreshForm();
                                
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                //m_JointEditDoc.SetSideClipImage(EmguCVPreviewImage.ToBitmap());
                                //m_JointEditDoc.AllocSideImage(pbyteBits);
                                SetFrontStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                //m_JointEditDoc.SetStandClipImage(EmguCVPreviewImage.ToBitmap());
                                //m_JointEditDoc.AllocStandingImage(pbyteBits);
                                SetFrontKneedownMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                // Œ‹‰Ê‰æ–Ê‚É‘JˆÚ‚·‚é.
                                //GetDocument()->ChangeToResultView();
                                //m_JointEditDoc.SetKneeClipImage(EmguCVPreviewImage.ToBitmap());
                                //m_JointEditDoc.AllocKneedownImage(pbyteBits);
                                m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                FunctionToDisplayResultViewFromCrouchedView(EventArgs.Empty);
                                this.Visible = false;
                                //this.Close();
                                //DisposeControls();

                                break;
                            default:
                                break;

                        }
                    }
                    break;
                default:
                    break;

            }
            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_on;
            m_ShootBtnFlag = false;
        }


        public event EventHandler EventToStartNextScreen; // creating event handler - step1
        public void CloseFormToStartNextScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartNextScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventfromCrouchedViewtoResultView;
        public void FunctionToDisplayResultViewFromCrouchedView(EventArgs e)
        {
            EventHandler eventHandler = EventfromCrouchedViewtoResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }

        }

        private void ImagePrevWnd_Paint(object sender, PaintEventArgs e)
        {

            int iSelectedBitmapWidth = m_ImageClipWnd.GetSelectedBitmapWidth();
            int iSelectedBitmapHeight = m_ImageClipWnd.GetSelectedBitmapHeight();
            int iSelectedBitmapSize = m_ImageClipWnd.CalcSelectedBitmapSize();
            byte[] pbyteBits = new byte[1024 * 1280 * 3];


            if (m_ShootBtnFlag && m_SearchFlag)
                m_ImageClipWnd.GetSelectedBitmap(e.Graphics, ref pbyteBits);
            else
            {
                
                switch(m_JointEditDoc.GetMeasurementStartViewMode())
                {
                    case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                        if(m_JointEditDoc.m_SideImageBytes != null)
                            m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                        if(m_JointEditDoc.m_FrontStandingImageBytes != null)
                            m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                        if(m_JointEditDoc.m_FrontKneedownImageBytes != null)
                            m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);
                        break;
                    default:
                        break;
                }
                
                m_ImagePreviewWnd.UpdateOffscreen(e.Graphics);
            }
                  
            
        }

        private void IDC_RotateBtn_Click(object sender, EventArgs e)
        {
            Bitmap bmp = null;

            //if (m_ReturnFlag)
            {
                switch (m_JointEditDoc.GetMeasurementStartViewMode())
                {
                    case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                        bmp = m_JointEditDoc.GetSideClipImage();
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                        bmp = m_JointEditDoc.GetStandClipImage();
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                        bmp = m_JointEditDoc.GetKneeClipImage();
                        break;
                    default:
                        break;
                }

            }
            //else
            {
                // bmp = m_JointEditDoc.GetFlipImage();
            }



            bmp.RotateFlip(RotateFlipType.Rotate270FlipXY);

            Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(bmp);

            int iDestImageWidth = m_ImageClipWnd.m_iBackgroundHeight;
            int iDestImageHeight = m_ImageClipWnd.m_iBackgroundWidth;
            //if (m_JointEditDoc.m_SideImageBytes == null)
            m_ImageClipWnd.SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, EmguCVImage.Bytes);
            /*else
                m_ImageClipWnd.SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, m_JointEditDoc.m_SideImageBytes);
                */
            ImageClipWnd.Invalidate();

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
        }

        private void IDD_MEASUREMENT_START_VIEW_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(bmBack1, (this.Width - bmBack1.Width) / 2, 0, bmBack1.Width, bmBack1.Height);
        }

        private void IDD_MEASUREMENT_START_VIEW_Resize(object sender, EventArgs e)
        {
            //MessageBox.Show("hi");
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        public void RefreshForm()
        {
            m_ShootBtnFlag = true;
            m_SearchFlag = true;
            picturebox1.Image = Yugamiru.Properties.Resources.Mainpic3;
            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);
            switch (m_JointEditDoc.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    pictureBox2.Image = Yugamiru.Properties.Resources.SpeechSideStanding;
                    SetSideStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    SetFrontStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
                    SetFrontKneedownMode();
                    break;
                default:
                    break;


            }
        }
    }

}
