﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Yugamiru
{
    public partial class PortSetting : Form
    {
        public PortSetting()
        {
            InitializeComponent();
            this.Text = Yugamiru.Properties.Resources.KEYWORD_PORT;
            label1.Text = Yugamiru.Properties.Resources.PORT;
            int iPortNumber = Constants.DEFAULT_PORT_NUMBER;
            IDC_Cancel.Text = Yugamiru.Properties.Resources.CANCEL;
            
           
            SettingDataMgr SettingDataMgr = new SettingDataMgr();
            if (SettingDataMgr.ReadFromFile(@"Resources\", "port.txt"))
            {
                string strValue = string.Empty;
                if (SettingDataMgr.GetValriableValue("PORT", ref strValue))
                {
                    textBox1.Text = strValue;

                }
                else
                {
                    textBox1.Text = iPortNumber.ToString();
                }
                
            }
           

            
        }

        private void IDC_OK_Click(object sender, EventArgs e)
        {
            string strText = textBox1.Text;
            
            char[] pchText = strText.ToCharArray();
            if (pchText == null)
            {
                return;
            }
            int iPortNumber = 0;
            int i = 0;
            if (pchText[0] == '\0')
            {
                return;
            }
            while (i < pchText.Length)
            {
                if (pchText[i] < '0')
                {
                    return;
                }
                if (pchText[i] > '9')
                {
                    return;
                }
                iPortNumber = iPortNumber * 10 + (pchText[i] - '0');
                if (iPortNumber > 65535)
                {
                    return;
                }
                i++;
            }

           
            File.WriteAllText(@"Resources\port.txt", string.Empty);
            using (FileStream fs = File.Open(@"Resources\port.txt", FileMode.Open, FileAccess.Write))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes("PORT,"+"\"" + iPortNumber + "\""+ "\r\n");
                fs.Write(info, 0,info.Length);
            }
            MessageBox.Show(Yugamiru.Properties.Resources.PORT_WARNING);
            this.Close();
        }

        private void IDC_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PortSetting_Load(object sender, EventArgs e)
        {

        }
    }
}
