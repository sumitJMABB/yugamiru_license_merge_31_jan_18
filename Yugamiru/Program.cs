﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

namespace Yugamiru
{
    static class Program
    {

        //Added By Sumit GSP-365------------START
        static Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        /// Added by sumit GSP-365---------------END

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            #region Original By Meena Commented By Sumit for one instance GSP-365
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);

            //#region Commented
            ///*IDD_BALANCELABO_DIALOG StartingForm = new IDD_BALANCELABO_DIALOG();
            //StartingForm.Text = Yugamiru.Properties.Resources.YUGAMIRU_TITLE;
            //Application.Run(StartingForm); Commented by meena becoz of out of memory exception*/ // this is the main page
            //#endregion

            //SettingDataMgr SettingDataMgr = new SettingDataMgr();
            //if (SettingDataMgr.ReadFromFile(@"Resources\", "Languageconfig.txt"))
            //{
            //    string strValue = string.Empty;
            //    if (SettingDataMgr.GetValriableValue("LANGUAGE", ref strValue))
            //    {
            //        CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(strValue);
            //    }

            //}

            //Application.Run(new IDD_BALANCELABO_DIALOG());

            ////Application.Run(new JointEditView());
            ////Application.Run(new Form4());
            #endregion

            //Edited By Sumit GSP-365------------START



            /// 
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                SettingDataMgr SettingDataMgr = new SettingDataMgr();
                if (SettingDataMgr.ReadFromFile(@"Resources\", "Languageconfig.txt"))
                {
                    string strValue = string.Empty;
                    if (SettingDataMgr.GetValriableValue("LANGUAGE", ref strValue))
                    {
                        CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(strValue);
                    }
                    mutex.ReleaseMutex();
                }
                Application.Run(new IDD_BALANCELABO_DIALOG());
            }
            else
            {
                // send our Win32 message to make the currently running instance
                // jump on top of all the other windows
                NativeMethods.PostMessage(
                    (IntPtr)NativeMethods.HWND_BROADCAST,
                    NativeMethods.WM_SHOWME,
                    IntPtr.Zero,
                    IntPtr.Zero);
            }



            /// Edit by sumit GSP-365---------------END

        }
    }
    //
    // this class just wraps some Win32 stuff that we're going to use
    internal class NativeMethods
    {
        public const int HWND_BROADCAST = 0xffff;
        public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");
        [DllImport("user32")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
        [DllImport("user32")]
        public static extern int RegisterWindowMessage(string message);
    }
}
