﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MuscleStatePatternDefinitionElement
    {
        int m_iMuscleStatePatternID;        // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“‚h‚c.
        int m_iMuscleID;                    // ‹Ø“÷‚h‚c.
        int m_iMuscleStateTypeID;           // ‹Ø“÷ó‘Ôƒ^ƒCƒv‚h‚c.
        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“’è‹`‚Ìƒ}ƒXƒ^[ƒf[ƒ^‚ÌƒXƒL[ƒ}.
        public MuscleStatePatternDefinitionElement( )
        {
            m_iMuscleStatePatternID = Constants.MUSCLESTATEPATTERNID_NONE;  // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“‚h‚c.

            m_iMuscleID = Constants.MUSCLEID_NONE;                      // ‹Ø“÷‚h‚c.

            m_iMuscleStateTypeID = Constants.MUSCLESTATETYPEID_NONE;      // ‹Ø“÷ó‘Ôƒ^ƒCƒv‚h‚c.
        
        }

       

    public int GetMuscleStatePatternID() 
{
	return m_iMuscleStatePatternID;
}


        public int GetMuscleID() 
{
	return m_iMuscleID;
}

        public int GetMuscleStateTypeID() 
{
	return m_iMuscleStateTypeID;
}

        public int ReadFromString(ref char[] lpszText)
{
    string strMuscleStatePatternSymbol = string.Empty;
    string strMuscleSymbol = string.Empty;
            string strMuscleStateTypeSymbol = string.Empty;

            TextBuffer TextBuffer = new TextBuffer(lpszText);

    TextBuffer.SkipSpaceOrTab();
    if (TextBuffer.IsEOF())
    {
        return (Constants.READFROMSTRING_NULLLINE);
    }

    if (TextBuffer.ReadNewLine())
    {
        if (TextBuffer.IsEOF())
        {
            return (Constants.READFROMSTRING_NULLLINE);
        }
    }

    if (!TextBuffer.ReadSymbol(ref strMuscleStatePatternSymbol))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadComma())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadSymbol(ref strMuscleSymbol))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadComma())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    if (!TextBuffer.ReadSymbol(ref strMuscleStateTypeSymbol))
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    TextBuffer.SkipSpaceOrTab();
    TextBuffer.ReadNewLine();
    if (!TextBuffer.IsEOF())
    {
        return (Constants.READFROMSTRING_SYNTAX_ERROR);
    }

    m_iMuscleStatePatternID = m_SymbolFunc.GetMuscleStatePatternIDBySymbol(strMuscleStatePatternSymbol);
    if (m_iMuscleStatePatternID == Constants.MUSCLESTATEPATTERNID_NONE)
    {
        return Constants.READFROMSTRING_MUSCLESTATEPATTERN_INVALID;
    }
    m_iMuscleID = m_SymbolFunc.GetMuscleIDBySymbol(strMuscleSymbol);
    if (m_iMuscleID == Constants.MUSCLEID_NONE)
    {
        return Constants.READFROMSTRING_MUSCLE_INVALID;
    }
    m_iMuscleStateTypeID = m_SymbolFunc.GetMuscleStateTypeIDBySymbol(strMuscleStateTypeSymbol);
    if (m_iMuscleStateTypeID == Constants.MUSCLESTATETYPEID_NONE)
    {
        return Constants.READFROMSTRING_MUSCLESTATETYPE_INVALID;
    }

    return Constants.READFROMSTRING_SYNTAX_OK;
}

public bool GetMuscleStateInfo(ref MuscleStateInfo temp_MuscleStateInfo, int iMuscleStatePatternID, int iMuscleStateLevel) 
{
	if ( m_iMuscleStatePatternID != iMuscleStatePatternID ){
		return false;
	}
            temp_MuscleStateInfo.AddData( m_iMuscleID, m_iMuscleStatePatternID, m_iMuscleStateTypeID, iMuscleStateLevel );
	return true;
}


   
}
}
