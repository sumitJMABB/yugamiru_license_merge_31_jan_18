﻿namespace Yugamiru
{
    partial class IDD_BALANCELABO_DIALOG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sETTINGSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qRCODEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pORTSETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lANGUAGESETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bETATESTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iMPORTYGAFILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eXPORTYGAFILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hELPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStripMenuItem_License = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_View_License = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Release_License = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sETTINGSToolStripMenuItem,
            this.bETATESTINGToolStripMenuItem,
            this.hELPToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(913, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // sETTINGSToolStripMenuItem
            // 
            this.sETTINGSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.qRCODEToolStripMenuItem,
            this.pORTSETTINGToolStripMenuItem,
            this.lANGUAGESETTINGToolStripMenuItem});
            this.sETTINGSToolStripMenuItem.Name = "sETTINGSToolStripMenuItem";
            this.sETTINGSToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.sETTINGSToolStripMenuItem.Text = "SETTINGS";
            // 
            // qRCODEToolStripMenuItem
            // 
            this.qRCODEToolStripMenuItem.CheckOnClick = true;
            this.qRCODEToolStripMenuItem.Name = "qRCODEToolStripMenuItem";
            this.qRCODEToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.qRCODEToolStripMenuItem.Text = "QR CODE";
            this.qRCODEToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.qRCODEToolStripMenuItem_CheckStateChanged);
            this.qRCODEToolStripMenuItem.Click += new System.EventHandler(this.qRCODEToolStripMenuItem_Click);
            // 
            // pORTSETTINGToolStripMenuItem
            // 
            this.pORTSETTINGToolStripMenuItem.Name = "pORTSETTINGToolStripMenuItem";
            this.pORTSETTINGToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.pORTSETTINGToolStripMenuItem.Text = "PORT SETTING";
            this.pORTSETTINGToolStripMenuItem.Click += new System.EventHandler(this.pORTSETTINGToolStripMenuItem_Click);
            // 
            // lANGUAGESETTINGToolStripMenuItem
            // 
            this.lANGUAGESETTINGToolStripMenuItem.Name = "lANGUAGESETTINGToolStripMenuItem";
            this.lANGUAGESETTINGToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.lANGUAGESETTINGToolStripMenuItem.Text = "LANGUAGE SETTING";
            this.lANGUAGESETTINGToolStripMenuItem.Click += new System.EventHandler(this.lANGUAGESETTINGToolStripMenuItem_Click);
            // 
            // bETATESTINGToolStripMenuItem
            // 
            this.bETATESTINGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iMPORTYGAFILEToolStripMenuItem,
            this.eXPORTYGAFILEToolStripMenuItem,
            this.toolStripMenuItem_License});
            this.bETATESTINGToolStripMenuItem.Name = "bETATESTINGToolStripMenuItem";
            this.bETATESTINGToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.bETATESTINGToolStripMenuItem.Text = "TOOLS";
            // 
            // iMPORTYGAFILEToolStripMenuItem
            // 
            this.iMPORTYGAFILEToolStripMenuItem.Name = "iMPORTYGAFILEToolStripMenuItem";
            this.iMPORTYGAFILEToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.iMPORTYGAFILEToolStripMenuItem.Text = "IMPORT YGA FILE";
            this.iMPORTYGAFILEToolStripMenuItem.Click += new System.EventHandler(this.iMPORTYGAFILEToolStripMenuItem_Click);
            // 
            // eXPORTYGAFILEToolStripMenuItem
            // 
            this.eXPORTYGAFILEToolStripMenuItem.Name = "eXPORTYGAFILEToolStripMenuItem";
            this.eXPORTYGAFILEToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.eXPORTYGAFILEToolStripMenuItem.Text = "EXPORT YGA FILE";
            this.eXPORTYGAFILEToolStripMenuItem.Click += new System.EventHandler(this.eXPORTYGAFILEToolStripMenuItem_Click);
            // 
            // hELPToolStripMenuItem
            // 
            this.hELPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionInfoToolStripMenuItem});
            this.hELPToolStripMenuItem.Name = "hELPToolStripMenuItem";
            this.hELPToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.hELPToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.hELPToolStripMenuItem.Text = "HELP(Alt+H)";
            // 
            // versionInfoToolStripMenuItem
            // 
            this.versionInfoToolStripMenuItem.Name = "versionInfoToolStripMenuItem";
            this.versionInfoToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.versionInfoToolStripMenuItem.Text = "Version Info Yugamiru cloud(A)...";
            this.versionInfoToolStripMenuItem.Click += new System.EventHandler(this.versionInfoToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.Location = new System.Drawing.Point(0, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(75, 52);
            this.panel2.TabIndex = 5;
            this.panel2.SizeChanged += new System.EventHandler(this.panel2_SizeChanged);
            // 
            // toolStripMenuItem_License
            // 
            this.toolStripMenuItem_License.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_View_License,
            this.toolStripMenuItem_Release_License});
            this.toolStripMenuItem_License.Name = "toolStripMenuItem_License";
            this.toolStripMenuItem_License.Size = new System.Drawing.Size(167, 22);
            this.toolStripMenuItem_License.Text = "LICENSE";
            // 
            // toolStripMenuItem_View_License
            // 
            this.toolStripMenuItem_View_License.Name = "toolStripMenuItem_View_License";
            this.toolStripMenuItem_View_License.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem_View_License.Text = "VIEW LICENSE";
            this.toolStripMenuItem_View_License.Click += new System.EventHandler(this.toolStripMenuItem_View_License_Click);
            // 
            // toolStripMenuItem_Release_License
            // 
            this.toolStripMenuItem_Release_License.Name = "toolStripMenuItem_Release_License";
            this.toolStripMenuItem_Release_License.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem_Release_License.Text = "RELEASE LICENSE";
            this.toolStripMenuItem_Release_License.Click += new System.EventHandler(this.toolStripMenuItem_Release_License_Click);
            // 
            // IDD_BALANCELABO_DIALOG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(913, 487);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(915, 486);
            this.Name = "IDD_BALANCELABO_DIALOG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "YUGAMIRU";
            this.MaximumSizeChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_MaximumSizeChanged);
            this.MinimumSizeChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_MinimumSizeChanged);
            this.Load += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_Load);
            this.Shown += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_Shown);
            this.SizeChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IDD_BALANCELABO_DIALOG_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDD_BALANCELABO_DIALOG_KeyDown);
            this.Resize += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sETTINGSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qRCODEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pORTSETTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hELPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionInfoToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem lANGUAGESETTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bETATESTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iMPORTYGAFILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eXPORTYGAFILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_License;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_View_License;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Release_License;
    }
}

