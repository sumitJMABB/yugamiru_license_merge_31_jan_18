﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Windows;
using System.Windows.Input;
using static Yugamiru.stretchDIBbits;
using System.IO;
using System.Windows.Forms;

namespace Yugamiru
{
    public class FrontJointEditWnd
    {
        public byte[] m_pbyteBits;
        stretchDIBbits.BITMAPINFO m_bmi;
        public Graphics m_pDCOffscreen;
        public Bitmap m_pbmOffscreen;
        public Bitmap m_pbmOffscreenOld;
        public int m_iOffscreenWidth;
        public int m_iOffscreenHeight;
        public int m_iBackgroundWidth;
        public int m_iBackgroundHeight;
        public int m_iSrcX;
        public int m_iSrcY;
        public int m_iSrcWidth;
        public int m_iSrcHeight;

        public int m_iMouseCaptureMode;
        public int m_iMousePointXOnDragStart;
        public int m_iMousePointYOnDragStart;
        public int m_iScreenWidthOnDragStart;
        public int m_iScreenHeightOnDragStart;
        public int m_iSrcXOnDragStart;
        public int m_iSrcYOnDragStart;
        public int m_iSrcWidthOnDragStart;
        public int m_iSrcHeightOnDragStart;

        public System.Drawing.Point[] m_aptEdit = new System.Drawing.Point[16] ;
        public bool[] m_isValidPoint = new bool[16];
        public bool[] m_isValidEditPoint = new bool[16];

        public  int m_iMode;
        public int m_Marker;       // ƒ}[ƒJ[ƒTƒCƒY
        public int m_iMarkerSizeBase;
        public int m_iEditPtID;

        public int m_iDataVersion;
        public int m_iShoulderOffset0;
        public int m_iShoulderOffset1;
        public int m_iShoulderOffset2;

        public bool m_bStyleLine;
        public uint m_uiStyleLineStyle;
        public Color m_crStyleLineColor;
        public uint m_uiStyleLineWidth;



        public FrontJointEditWnd()
        {

            m_pbyteBits = null;

            m_pDCOffscreen = null;

    m_pbmOffscreen = null;

            m_pbmOffscreenOld= null;

            m_iOffscreenWidth = 0;

    m_iOffscreenHeight = 0;

            m_iBackgroundWidth = 0;

            m_iBackgroundHeight = 0;

            m_iSrcX = 0;

            m_iSrcY = 0;

            m_iSrcWidth = 0;

            m_iSrcHeight = 0;

            m_iMouseCaptureMode = 0;

            m_iMousePointXOnDragStart = 0;

            m_iMousePointYOnDragStart = 0;

            m_iScreenWidthOnDragStart = 0;

            m_iScreenHeightOnDragStart = 0;

            m_iSrcXOnDragStart = 0;

            m_iSrcYOnDragStart = 0;

            m_iSrcWidthOnDragStart = 0;

            m_iSrcHeightOnDragStart = 0;

            m_iMode = 0;

            m_Marker = 0;

            m_iMarkerSizeBase = 0;

            m_iDataVersion = 0;

            m_iShoulderOffset0 = 0;

            m_iShoulderOffset1 = 0;

            m_iShoulderOffset2 = 0;

            m_bStyleLine = false;

    m_uiStyleLineStyle = 0;

            // m_crStyleLineColor.ToArgb(RGB(0,0,0) ),

            m_uiStyleLineWidth = 0;

            m_iEditPtID = -1;
        
            int i = 0;
            for (i = 0; i < 16; i++)
            {
                m_aptEdit[i].X = 0;
                m_aptEdit[i].Y = 0;
                m_isValidPoint[i] = false;
                m_isValidEditPoint[i] = false;
            }
            //memset(&m_bmi, 0, sizeof(m_bmi));
        }

        ~FrontJointEditWnd()
        {
            Cleanup();
        }

        public void Cleanup()
        {
            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            if (m_pDCOffscreen != null)
            {
                if (m_pbmOffscreenOld != null)
                {
                    //m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
                //delete m_pDCOffscreen;
                m_pDCOffscreen = null;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
        }


        public void OnPaint()
        {
            //CPaintDC dc(this); // device context for painting
        /*    Graphics dc = this.CreateGraphics();
            CRect rcClient;
            GetClientRect(rcClient);
            if ((m_pDCOffscreen != null) &&
                (rcClient.Width() == m_iOffscreenWidth) &&
                (rcClient.Height() == m_iOffscreenHeight))
            {
                dc.BitBlt(0, 0, rcClient.Width(), rcClient.Height(), m_pDCOffscreen, 0, 0, SRCCOPY);
            }
            else
            {
                dc.FillSolidRect(&rcClient, RGB(255, 0, 0));
            }*/
        }

        public void OnDestroy()
        {
            Cleanup();
            //CWnd::OnDestroy();
        }

        public void OnSize(uint nType, int cx, int cy)
        {
            //CWnd::OnSize(nType, cx, cy);
            if (m_pDCOffscreen != null)
            {
                if ((cx == m_iOffscreenWidth) && (cy == m_iOffscreenHeight))
                {
                    // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ÌÄ¶¬•s—v.
                    return;
                }
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðÄ¶¬.
                if (m_pbmOffscreenOld != null)
                {
                    //m_pDCOffscreen->SelectObject(m_pbmOffscreenOld);
                    m_pbmOffscreenOld = null;
                }
            }
            else
            {
                // ƒIƒtƒXƒNƒŠ[ƒ“ƒoƒbƒtƒ@‚ðV‹K¶¬.
           /*     m_pDCOffscreen = new CDC;
                CDC* pDC = GetDC();
                m_pDCOffscreen->CreateCompatibleDC(pDC);
                ReleaseDC(pDC);
                pDC = NULL;
                */
            }

            if (m_pbmOffscreen == null)
            {
                //m_pbmOffscreen = new CBitmap;
            }
            if (m_pbmOffscreen != null)
            {
                //delete m_pbmOffscreen;
                m_pbmOffscreen = null;
            }
    /*        
            m_pbmOffscreen = new CBitmap;
            CDC* pDC = GetDC();
            m_pbmOffscreen->CreateCompatibleBitmap(pDC, cx, cy);
            ReleaseDC(pDC);
            pDC = NULL;
            m_pbmOffscreenOld = m_pDCOffscreen->SelectObject(m_pbmOffscreen);
            if (m_pbyteBits != null)
            {
                m_pDCOffscreen->SetStretchBltMode(HALFTONE);

        ::StretchDIBits(m_pDCOffscreen->GetSafeHdc(),
            0, 0, cx, cy,
            m_iSrcX, (m_iBackgroundHeight - 1 - (m_iSrcY + m_iSrcHeight)),
            m_iSrcWidth, m_iSrcHeight,
            m_pbyteBits, &m_bmi, DIB_RGB_COLORS, SRCCOPY);
            }
            else
            {
                m_pDCOffscreen->FillSolidRect(0, 0, cx, cy, RGB(0, 255, 0));
            }
            m_iOffscreenWidth = cx;
            m_iOffscreenHeight = cy;
            */
        }

        bool OnEraseBkgnd(Graphics pDC)
        {
            return true;
        }

  public bool SetBackgroundBitmap( string pchFileName )
{
            FileStream fp = new FileStream(pchFileName, FileMode.Open, FileAccess.ReadWrite);
            //FILE * fp = fopen(pchFileName, "rb");
	if ( fp == null ){
		return false;
	}
    if(fp.Seek(0, SeekOrigin.End) != 0) {
                //if ( fseek(fp, 0, SEEK_END ) != 0 )

                fp.Close();
    fp = null;
		return false;
	}
            long lSize = fp.Length;//ftell(fp);
	if (fp.Seek(0,SeekOrigin.Current) != 0)
            { 
                //fseek(fp, 0, SEEK_SET ) != 0 ){

        fp.Close();
fp = null;
		return false;
	}
            //unsigned char* pbyteData = new unsigned char[lSize];
            byte[] pbyteData = new byte[lSize];

    if ( pbyteData == null ){

        fp.Close();
fp = null;
		return false;		
	}
	if ( fp.Read(pbyteData, 1, (int)lSize) != lSize ){

        fp.Close();
fp = null;
		if ( pbyteData != null ){
			//delete[] pbyteData;
pbyteData = null;
		}
		return false;		
	}

    fp.Close();
fp = null;
            /*

            stretchDIBbits.BITMAPINFOHEADER pbih = (stretchDIBbits.BITMAPINFOHEADER)(pbyteData)+ stretchDIBbits.BITMAPFILEHEADER.);
            stretchDIBbits.BITMAPINFO pbi;//= (BITMAPINFO*)(pbyteData + sizeof(BITMAPFILEHEADER));
            //unsigned char* pbyteBits = (unsigned char*)( pbyteData + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) );
            byte[] pbyteBits = pbyteData;


    int iBmpWidthStep = (pbih.biWidth * 3 + 3) / 4 * 4;
int iBmpBitsSize = iBmpWidthStep * pbih.biHeight;
	if ( iBmpBitsSize <= 0 ){
		if ( pbyteData != null ){
			//delete[] pbyteData;
pbyteData = null;
		}
		return false;
	}
	if ( m_pbyteBits != null ){
		//delete[] m_pbyteBits;
m_pbyteBits = null;
	}
	m_pbyteBits = new byte[iBmpBitsSize];
	if ( m_pbyteBits == null ){
		if ( pbyteData != null ){
			//delete[] pbyteData;
pbyteData = null;
		}
		return false;
	}
	int i = 0;
	for( i = 0; i<iBmpBitsSize; i++ ){
		m_pbyteBits[i] = pbyteBits[i];
	}

	m_bmi.bmiHeader.biSize			= //40sizeof(BITMAPINFOHEADER);
	m_bmi.bmiHeader.biWidth			= pbih->biWidth;
	m_bmi.bmiHeader.biHeight		= pbih->biHeight;
	m_bmi.bmiHeader.biPlanes		= 1;
	m_bmi.bmiHeader.biBitCount		= 24;
	m_bmi.bmiHeader.biCompression	= 0;
	m_bmi.bmiHeader.biSizeImage		= 0;
	m_bmi.bmiHeader.biXPelsPerMeter	= 0;
	m_bmi.bmiHeader.biYPelsPerMeter	= 0;
	m_bmi.bmiHeader.biClrUsed		= 0;
	m_bmi.bmiHeader.biClrImportant	= 0;

	m_bmi.bmiColors[0].rgbBlue		= 255;
	m_bmi.bmiColors[0].rgbGreen		= 255;
	m_bmi.bmiColors[0].rgbRed		= 255;
	m_bmi.bmiColors[0].rgbReserved	= 255;

	m_iBackgroundWidth	= pbih->biWidth;
	m_iBackgroundHeight	= pbih->biHeight;
	m_iSrcX = 0;
	m_iSrcY = 0;
	m_iSrcWidth = pbih->biWidth;
	m_iSrcHeight = pbih->biHeight;

	if ( pbyteData != NULL ){
		delete[] pbyteData;
pbyteData = NULL;
	}
    */
	return true;
}

public bool SetBackgroundBitmap(int iWidth, int iHeight, byte[] pbyteBits)
{
    m_bmi.bmiHeader.biSize = 40;//sizeof(BITMAPINFOHEADER);
    m_bmi.bmiHeader.biWidth = iWidth;
    m_bmi.bmiHeader.biHeight = -iHeight;
    m_bmi.bmiHeader.biPlanes = 1;
    m_bmi.bmiHeader.biBitCount = 24;
    m_bmi.bmiHeader.biCompression = 0;
    m_bmi.bmiHeader.biSizeImage = 0;
    m_bmi.bmiHeader.biXPelsPerMeter = 0;
    m_bmi.bmiHeader.biYPelsPerMeter = 0;
    m_bmi.bmiHeader.biClrUsed = 0;
    m_bmi.bmiHeader.biClrImportant = 0;

    m_bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };

    m_bmi.bmiColors[0].rgbBlue = 255;
    m_bmi.bmiColors[0].rgbGreen = 255;
    m_bmi.bmiColors[0].rgbRed = 255;
    m_bmi.bmiColors[0].rgbReserved = 255;

    m_iBackgroundWidth = iWidth;
    m_iBackgroundHeight = iHeight;

    int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
    int iBmpBitsSize = iBmpWidthStep * iHeight;
    if (iBmpBitsSize <= 0)
    {
        return false;
    }
    if (m_pbyteBits != null)
    {
        //delete[] m_pbyteBits;
        m_pbyteBits = null;
    }
    //m_pbyteBits = new byte[iBmpBitsSize];
    if (m_pbyteBits == null)
    {
        return false;
    }
    int i = 0;
        /*     for (i = 0; i < iBmpBitsSize; i++)
              {
                          
                  m_pbyteBits[i] = pbyteBits[i];
              }*/
            //m_pbyteBits = converterDemo(Yugamiru.Properties.Resources.sokui);
            //m_pbyteBits = pbyteBits;
            m_iSrcX = 0;
    m_iSrcY = 0;
    m_iSrcWidth = m_iBackgroundWidth;
    m_iSrcHeight = m_iBackgroundHeight;


    return true;
}

        public static byte[] converterDemo(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }




        public void SetSrcPos(int iXPos, int iYPos)
{
    m_iSrcX = iXPos;
    m_iSrcY = iYPos;
}

        public void SetSrcSize(int iWidth, int iHeight)
{
    m_iSrcWidth = iWidth;
    m_iSrcHeight = iHeight;
}

        public void OnRButtonDown(uint nFlags, Point point)
{
    //CWnd::OnRButtonDown(nFlags, point);

    if (m_iMouseCaptureMode != 0)
    {
        return;
    }

    //SetCapture();
    m_iMouseCaptureMode = 2;

    Rectangle rcClient = new Rectangle();
    //GetClientRect(&rcClient);

    m_iMousePointXOnDragStart = point.X;
    m_iMousePointYOnDragStart = point.Y;
            m_iScreenWidthOnDragStart = 423;//rcClient.Width;
    m_iScreenHeightOnDragStart = 470;//rcClient.Height;
    m_iSrcXOnDragStart = m_iSrcX;
    m_iSrcYOnDragStart = m_iSrcY;
    m_iSrcWidthOnDragStart = m_iSrcWidth;
    m_iSrcHeightOnDragStart = m_iSrcHeight;
    m_iEditPtID = -1;
            /*
    if (GetParent() != NULL)
    {
        GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
    }
    */
}

public void OnRButtonUp(uint nFlags, Point point)
{
    //CWnd::OnRButtonUp(nFlags, point);
    if (m_iMouseCaptureMode != 2)
    {
        return;
    }
    //ReleaseCapture();
    m_iMouseCaptureMode = 0;
    m_iEditPtID = -1;
  /*  if (GetParent() != NULL)
    {
        GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
    }*/
}

public void OnLButtonDown(uint nFlags, Point point)
{
   // CWnd::OnLButtonDown(nFlags, point);

    if (m_iMouseCaptureMode != 0)
    {
        return;
    }

    m_iEditPtID = SearchEditPoint(point, 15 * 15);
    if (m_iEditPtID == -1)
    {
       // OutputDebugString("EditPoint = -1 \n");
        return;
    }
    else
    {
        char[] szMessage = new char[100];
     //   wsprintf(&(szMessage[0]), "EditPoint = %d\n", m_iEditPtID);
     //   OutputDebugString(&(szMessage[0]));
    }

  //  SetCapture();
    m_iMouseCaptureMode = 1;

    Rectangle rcClient = new Rectangle();
   // GetClientRect(&rcClient);

    m_iMousePointXOnDragStart = point.X;
    m_iMousePointYOnDragStart = point.Y;
    m_iScreenWidthOnDragStart = rcClient.Width;
    m_iScreenHeightOnDragStart = rcClient.Height;
    m_iSrcXOnDragStart = m_iSrcX;
    m_iSrcYOnDragStart = m_iSrcY;
    m_iSrcWidthOnDragStart = m_iSrcWidth;
    m_iSrcHeightOnDragStart = m_iSrcHeight;
/*
    if (GetParent() != NULL)
    {
        GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
    }
    */
}

void OnLButtonUp(uint nFlags, Point point)
{
    //CWnd::OnLButtonUp(nFlags, point);
    if (m_iMouseCaptureMode != 1)
    {
        return;
    }
    //ReleaseCapture();
    ExchangeLRData();
    m_iMouseCaptureMode = 0;
    m_iEditPtID = -1;
/*    if (GetParent() != NULL)
    {
        GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
    }
    */
}

public void OnMouseMove(uint nFlags, Point point)
{
   // CWnd::OnMouseMove(nFlags, point);

    if ((m_iMouseCaptureMode == 1) && (m_iEditPtID != -1) && (m_isValidEditPoint[m_iEditPtID]))
    {
        // ŠÖßˆÊ’uC³
        m_aptEdit[m_iEditPtID].X = point.X * m_iSrcWidth / m_iOffscreenWidth + m_iSrcX;
        m_aptEdit[m_iEditPtID].Y = point.Y * m_iSrcHeight / m_iOffscreenHeight + m_iSrcY;

        if (m_aptEdit[m_iEditPtID].X < 0)
        {
            m_aptEdit[m_iEditPtID].X = 0;
        }
        if (m_aptEdit[m_iEditPtID].Y < 0)
        {
            m_aptEdit[m_iEditPtID].Y = 0;
        }
        if (m_aptEdit[m_iEditPtID].X >= m_iBackgroundWidth)
        {
            m_aptEdit[m_iEditPtID].X = m_iBackgroundWidth - 1;
        }
        if (m_aptEdit[m_iEditPtID].Y >= m_iBackgroundHeight)
        {
            m_aptEdit[m_iEditPtID].Y = m_iBackgroundHeight - 1;
        }
 /*       if (GetParent() != NULL)
        {
            GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
        }
        UpdateOffscreen();
        CRect rcClient;
        GetClientRect(&rcClient);
        InvalidateRect(&rcClient);
        UpdateWindow();
        */
    }
    else if (m_iMouseCaptureMode == 2)
    {
        int iDiffX = point.X - m_iMousePointXOnDragStart;
        int iDiffY = point.Y - m_iMousePointYOnDragStart;
        m_iSrcX = m_iSrcXOnDragStart - iDiffX * m_iSrcWidthOnDragStart / m_iScreenWidthOnDragStart;
        m_iSrcY = m_iSrcYOnDragStart - iDiffY * m_iSrcHeightOnDragStart / m_iScreenHeightOnDragStart;
        if (m_iSrcX < 0)
        {
            m_iSrcX = 0;
        }
        if (m_iSrcY < 0)
        {
            m_iSrcY = 0;
        }
        if (m_iSrcX + m_iSrcWidthOnDragStart >= m_iBackgroundWidth)
        {
            m_iSrcX = m_iBackgroundWidth - m_iSrcWidthOnDragStart;
        }
        if (m_iSrcY + m_iSrcHeightOnDragStart >= m_iBackgroundHeight)
        {
            m_iSrcY = m_iBackgroundHeight - m_iSrcHeightOnDragStart;
        }
/*        if (GetParent() != NULL)
        {

            GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
        }
        UpdateOffscreen();
        CRect rcClient;
        GetClientRect(&rcClient);
        InvalidateRect(&rcClient);
        UpdateWindow();*/
    }
}

// st‚©‚çend‚Ü‚ÅaplliŽw’è‚Ì’¼ü‚ð•`‰æ
public void MyDrawLineStyl(Graphics pDC, Point st, Point end)
{
    MyDrawLine(pDC, st, end, (int)m_uiStyleLineStyle, m_crStyleLineColor, (int)m_uiStyleLineWidth);
}

// st‚©‚çend‚Ü‚ÅAüŽístyleAFrgbA‘¾‚³width‚Ì’¼ü‚ð•`‰æ
public void MyDrawLine(Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
{
    //	MoveToEx(hDC, st.x, st.y, NULL);
    //	LineTo(hDC, end.x, end.y);

    byte[] type = new byte[8];
            
    //int c = CDashLine::GetPattern(type, false, width, style);
    //CDashLine line(*pDC, type, c);
    Pen penNew = new Pen(rgb,width);

            //CPen* ppenOld = pDC->SelectObject(&penNew);
            pDC.DrawLine(penNew, st, end);
    //pDC->SelectObject(ppenOld);
}

// ’†Sx,yA”¼ŒasizeAFrgbA‚Ì“_‚ð•`‰æ
// fill‚ªtrue‚Ì‚Æ‚«‚Í“h‚è‚Â‚Ô‚µ
public void MyDrawPoint(Graphics pDC, int x, int y, int size, Color rgb /* =RGB(0,0,0) */, bool fill /*=false*/)
{
            //CPen penNew(PS_SOLID, 0, rgb );
            Pen penNew = new Pen(rgb);
            SolidBrush brNew = new SolidBrush(rgb);
            
    //CPen* ppenOld = pDC->SelectObject(&penNew);
    //CBrush* pbrOld = NULL;
    //HBRUSH hbrOld = NULL;
    if (fill)
    {
                //pDC.FillEllipse(brNew, x - size/2  , y - size/2 , size+4, size+4); by meena
                pDC.FillEllipse(brNew, x-size/2, y-size/2 , size, size);
                //pbrOld = pDC->SelectObject(&brNew);
            }
    else
    {
                // pDC.DrawEllipse(penNew, x - size/2 , y - size/2, size+4, size+4); by meena
                pDC.DrawEllipse(penNew, x-size/2 , y-size/2, size, size);
                //hbrOld = (HBRUSH)(pDC->SelectObject((HBRUSH)GetStockObject(NULL_BRUSH)));
            }
    //pDC.FillEllipse(brNew, x - size, y - size, x + size + 1, y + size + 1);
            //pDC.FillEllipse(brNew, x - size, y - size, size, size);
            
            //MessageBox.Show((x - size).ToString() + "," + (y - size).ToString());
            /*
    if (ppenOld != NULL)
    {
        pDC->SelectObject(ppenOld);
        ppenOld = NULL;
    }
    if (pbrOld != NULL)
    {
        pDC->SelectObject(pbrOld);
        pbrOld = NULL;
    }
    if (hbrOld != NULL)
    {
        pDC->SelectObject(hbrOld);
        hbrOld = NULL;
    }
    */
}

public void SetAnkleAndHipMode()
{
    m_iMode = Constants.FRONTJOINTEDITWNDMODE_ANKLE_AND_HIP;

    m_isValidPoint[Constants.RIGHT_HIP] = false;      // rhip
    m_isValidPoint[Constants.LEFT_HIP] = false;       // lhip
    m_isValidPoint[Constants.RIGHT_KNEE] = false;     // rknee
    m_isValidPoint[Constants.LEFT_KNEE] = false;      // lknee
    m_isValidPoint[Constants.RIGHT_ANKLE] = true;     // rankle
    m_isValidPoint[Constants.LEFT_ANKLE] = true;      // lankle
    m_isValidPoint[Constants.BELT1] = true;       // rbelt
    m_isValidPoint[Constants.BELT2] = true;       // lbelt
    m_isValidPoint[Constants.RIGHT_SHOULDER] = false;     // rshoulder
    m_isValidPoint[Constants.LEFT_SHOULDER] = false;      // lshoulder
    m_isValidPoint[Constants.RIGHT_EAR] = false;  // rear
    m_isValidPoint[Constants.LEFT_EAR] = false;   // lear
    m_isValidPoint[Constants.CHIN] = false;   // head
    m_isValidPoint[Constants.MIDDLE_EYEBROWS] = false;    // tin

    m_isValidEditPoint[Constants.RIGHT_HIP] = false;      // rhip
    m_isValidEditPoint[Constants.LEFT_HIP] = false;       // lhip
    m_isValidEditPoint[Constants.RIGHT_KNEE] = false;     // rknee
    m_isValidEditPoint[Constants.LEFT_KNEE] = false;      // lknee
    m_isValidEditPoint[Constants.RIGHT_ANKLE] = true;     // rankle
    m_isValidEditPoint[Constants.LEFT_ANKLE] = true;      // lankle
    m_isValidEditPoint[Constants.BELT1] = true;       // rbelt
    m_isValidEditPoint[Constants.BELT2] = true;       // lbelt
    m_isValidEditPoint[Constants.RIGHT_SHOULDER] = false;     // rshoulder
    m_isValidEditPoint[Constants.LEFT_SHOULDER] = false;      // lshoulder
    m_isValidEditPoint[Constants.RIGHT_EAR] = false;  // rear
    m_isValidEditPoint[Constants.LEFT_EAR] = false;   // lear
    m_isValidEditPoint[Constants.CHIN] = false;   // head
    m_isValidEditPoint[Constants.MIDDLE_EYEBROWS] = false;    // tin

    //UpdateOffscreen(graphics);
}

public void SetKneeMode()
{
    m_iMode = Constants.FRONTJOINTEDITWNDMODE_KNEE;

    m_isValidPoint[Constants.RIGHT_HIP] = true;       // rhip
    m_isValidPoint[Constants.LEFT_HIP] = true;        // lhip
    m_isValidPoint[Constants.RIGHT_KNEE] = true;      // rknee
    m_isValidPoint[Constants.LEFT_KNEE] = true;       // lknee
    m_isValidPoint[Constants.RIGHT_ANKLE] = true;     // rankle
    m_isValidPoint[Constants.LEFT_ANKLE] = true;      // lankle
    m_isValidPoint[Constants.BELT1] = false;      // rbelt
    m_isValidPoint[Constants.BELT2] = false;      // lbelt
    m_isValidPoint[Constants.RIGHT_SHOULDER] = false;     // rshoulder
    m_isValidPoint[Constants.LEFT_SHOULDER] = false;      // lshoulder
    m_isValidPoint[Constants.RIGHT_EAR] = false;  // rear
    m_isValidPoint[Constants.LEFT_EAR] = false;   // lear
    m_isValidPoint[Constants.CHIN] = false;   // head
    m_isValidPoint[Constants.MIDDLE_EYEBROWS] = false;    // tin

    m_isValidEditPoint[Constants.RIGHT_HIP] = false;      // rhip
    m_isValidEditPoint[Constants.LEFT_HIP] = false;       // lhip
    m_isValidEditPoint[Constants.RIGHT_KNEE] = true;      // rknee
    m_isValidEditPoint[Constants.LEFT_KNEE] = true;       // lknee
    m_isValidEditPoint[Constants.RIGHT_ANKLE] = false;    // rankle
    m_isValidEditPoint[Constants.LEFT_ANKLE] = false; // lankle
    m_isValidEditPoint[Constants.BELT1] = false;      // rbelt
    m_isValidEditPoint[Constants.BELT2] = false;      // lbelt
    m_isValidEditPoint[Constants.RIGHT_SHOULDER] = false;     // rshoulder
    m_isValidEditPoint[Constants.LEFT_SHOULDER] = false;      // lshoulder
    m_isValidEditPoint[Constants.RIGHT_EAR] = false;  // rear
    m_isValidEditPoint[Constants.LEFT_EAR] = false;   // lear
    m_isValidEditPoint[Constants.CHIN] = false;   // head
    m_isValidEditPoint[Constants.MIDDLE_EYEBROWS] = false;    // tin

    //UpdateOffscreen();
}

public void SetUpperBodyMode()
{
    m_iMode = Constants.FRONTJOINTEDITWNDMODE_UPPERBODY;

    m_isValidPoint[Constants.RIGHT_HIP] = true;   // rhip
    m_isValidPoint[Constants.LEFT_HIP] = true;    // lhip
    m_isValidPoint[Constants.RIGHT_KNEE] = true;  // rknee
    m_isValidPoint[Constants.LEFT_KNEE] = true;   // lknee
    m_isValidPoint[Constants.RIGHT_ANKLE] = true; // rankle
    m_isValidPoint[Constants.LEFT_ANKLE] = true;  // lankle
    m_isValidPoint[Constants.BELT1] = false;      // rbelt
    m_isValidPoint[Constants.BELT2] = false;      // lbelt
    m_isValidPoint[Constants.RIGHT_SHOULDER] = true;      // rshoulder
    m_isValidPoint[Constants.LEFT_SHOULDER] = true;       // lshoulder
    m_isValidPoint[Constants.RIGHT_EAR] = true;   // rear
    m_isValidPoint[Constants.LEFT_EAR] = true;    // lear
    m_isValidPoint[Constants.CHIN] = true;    // head
    m_isValidPoint[Constants.MIDDLE_EYEBROWS] = true; // tin

    m_isValidEditPoint[Constants.RIGHT_HIP] = false;      // rhip
    m_isValidEditPoint[Constants.LEFT_HIP] = false;       // lhip
    m_isValidEditPoint[Constants.RIGHT_KNEE] = false;     // rknee
    m_isValidEditPoint[Constants.LEFT_KNEE] = false;      // lknee
    m_isValidEditPoint[Constants.RIGHT_ANKLE] = false;        // rankle
    m_isValidEditPoint[Constants.LEFT_KNEE] = false;      // lankle
    m_isValidEditPoint[Constants.BELT1] = false;      // rbelt
    m_isValidEditPoint[Constants.BELT2] = false;      // lbelt
    m_isValidEditPoint[Constants.RIGHT_SHOULDER] = true;      // rshoulder
    m_isValidEditPoint[Constants.LEFT_SHOULDER] = true;       // lshoulder
    m_isValidEditPoint[Constants.RIGHT_EAR] = true;   // rear
    m_isValidEditPoint[Constants.LEFT_EAR] = true;    // lear
    m_isValidEditPoint[Constants.CHIN] = true;    // head
    m_isValidEditPoint[Constants.MIDDLE_EYEBROWS] = true; // tin

    //UpdateOffscreen();
}

public int SearchEditPoint(Point pt, int iDistanceLimit)
{

    int no = -1;
    double distance;
    double shortest = iDistanceLimit;

    Point[] pos = new Point[16];
    int i = 0;
    for (i = 0; i < 8; i++)
    {
        pos[i].X = (m_aptEdit[i].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
        pos[i].Y = (m_aptEdit[i].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
    }

    if (m_iDataVersion >= Constants.VER_2005_RELEASE)
    {
        for (i = 8; i < 16; i++)
        {
            pos[i].X= (m_aptEdit[i].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
            pos[i].Y = (m_aptEdit[i].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
        }
    }

    for (i = 0; i < 8; i++)
    {
        distance = (pos[i].X - pt.X) * (pos[i].X - pt.X) + (pos[i].Y - pt.Y) * (pos[i].Y - pt.Y);

        if (m_isValidEditPoint[i] && distance <= shortest)
        {
            no = i;
            shortest = distance;
        }
    }

    if (m_iDataVersion >= Constants.VER_2005_RELEASE)
    {
        for (i = 8; i < 16; i++)
        {
            distance = (pos[i].X - pt.X) * (pos[i].X - pt.X) + (pos[i].Y - pt.Y) * (pos[i].Y - pt.Y);

            if (m_isValidEditPoint[i] && distance <= shortest)
            {
                no = i;
                shortest = distance;
            }
        }
    }

    return no;
}

public int GetSrcX() 
{
	return m_iSrcX;
}

public int GetSrcY() 
{
	return m_iSrcY;
}

        public int GetSrcWidth() 
{
	return m_iSrcWidth;
}

        public int GetSrcHeight() 
{
	return m_iSrcHeight;
}

        public int GetEditPtID() 
{
	return m_iEditPtID;
}

// ¶‰E‚ÌƒAƒCƒRƒ“‚ðØ‚è‘Ö‚¦‚é
public void ExchangeLRData()
{
    // ¶‰E‚Ìƒf[ƒ^“ü‚ê‘Ö‚¦
    Point[] pos = new Point[16];
    Point tmp;
    int i = 0;
    for (i = 0; i < 16; i++)
    {
        pos[i].X = m_aptEdit[i].X;
        pos[i].Y = m_aptEdit[i].Y;
    }

    // ˜
    if (pos[Constants.RIGHT_HIP].X > pos[Constants.LEFT_HIP].X)
    {
        tmp = pos[Constants.RIGHT_HIP];
        pos[Constants.RIGHT_HIP] = pos[Constants.LEFT_HIP];
        pos[Constants.LEFT_HIP] = tmp;
        if (m_iEditPtID == Constants.RIGHT_HIP)
        {
            m_iEditPtID = Constants.LEFT_HIP;
        }
        else
        {
            m_iEditPtID = Constants.RIGHT_HIP;
        }
    }
    // ‚Ð‚´
    if (pos[Constants.RIGHT_KNEE].X > pos[Constants.LEFT_KNEE].X)
    {
        tmp = pos[Constants.RIGHT_KNEE];
        pos[Constants.RIGHT_KNEE] = pos[Constants.LEFT_KNEE];
        pos[Constants.LEFT_KNEE] = tmp;
        if (m_iEditPtID == Constants.RIGHT_KNEE)
        {
            m_iEditPtID = Constants.LEFT_KNEE;
        }
        else
        {
            m_iEditPtID = Constants.RIGHT_KNEE;
        }
    }

    // æù
    if (pos[Constants.RIGHT_ANKLE].X > pos[Constants.LEFT_ANKLE].X)
    {
        tmp = pos[Constants.RIGHT_ANKLE];
        pos[Constants.RIGHT_ANKLE] = pos[Constants.LEFT_ANKLE];
        pos[Constants.LEFT_ANKLE] = tmp;
        if (m_iEditPtID == Constants.RIGHT_ANKLE)
        {
            m_iEditPtID = Constants.LEFT_ANKLE;
        }
        else
        {
            m_iEditPtID = Constants.RIGHT_ANKLE;
        }
    }

    // ƒxƒ‹ƒg
    if (pos[Constants.BELT1].X > pos[Constants.BELT2].X)
    {
        tmp = pos[Constants.BELT1];
        pos[Constants.BELT1] = pos[Constants.BELT2];
        pos[Constants.BELT2] = tmp;
        if (m_iEditPtID == Constants.BELT1)
        {
            m_iEditPtID = Constants.BELT2;
        }
        else
        {
            m_iEditPtID = Constants.BELT1;
        }
    }

    if (m_iDataVersion >= Constants.VER_2005_RELEASE)
    {
        //Ž¨
        if (pos[Constants.RIGHT_EAR].X > pos[Constants.LEFT_EAR].X)
        {
            tmp = pos[Constants.RIGHT_EAR];
            pos[Constants.RIGHT_EAR] = pos[Constants.LEFT_EAR];
            pos[Constants.LEFT_EAR] = tmp;
            if (m_iEditPtID == Constants.RIGHT_EAR)
            {
                m_iEditPtID = Constants.LEFT_EAR;
            }
            else
            {
                m_iEditPtID = Constants.RIGHT_EAR;
            }
        }

        //Œ¨
        if (pos[Constants.RIGHT_SHOULDER].X > pos[Constants.LEFT_SHOULDER].X)
        {
            tmp = pos[Constants.RIGHT_SHOULDER];
            pos[Constants.RIGHT_SHOULDER] = pos[Constants.LEFT_SHOULDER];
            pos[Constants.LEFT_SHOULDER] = tmp;
            if (m_iEditPtID == Constants.RIGHT_SHOULDER)
            {
                m_iEditPtID = Constants.LEFT_SHOULDER;
            }
            else
            {
                m_iEditPtID = Constants.RIGHT_SHOULDER;
            }
        }

        //“ª(ã‰º“ü‚ê‘Ö‚¦)
        if (pos[Constants.CHIN].Y < pos[Constants.MIDDLE_EYEBROWS].Y)
        {
            tmp = pos[Constants.CHIN];
            pos[Constants.CHIN] = pos[Constants.MIDDLE_EYEBROWS];
            pos[Constants.MIDDLE_EYEBROWS] = tmp;
            if (m_iEditPtID == Constants.CHIN)
            {
                m_iEditPtID = Constants.MIDDLE_EYEBROWS;
            }
            else
            {
                m_iEditPtID = Constants.CHIN;
            }
        }
    }

    for (i = 0; i < 16; i++)
    {
        m_aptEdit[i] = pos[i];
    }
}

public int GetBackgroundWidth() 
{
	return m_iBackgroundWidth;
}

public int GetBackgroundHeight() 
{
	return m_iBackgroundHeight;
}
/*
public int OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CWnd::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }

    return 0;
}
*/
public void GetFrontBodyPosition(ref FrontBodyPosition FrontBodyPosition ) 
{
	FrontBodyPosition.SetRightHipPosition( m_aptEdit[Constants.RIGHT_HIP] );
	FrontBodyPosition.SetLeftHipPosition( m_aptEdit[Constants.LEFT_HIP] );
	FrontBodyPosition.SetRightKneePosition( m_aptEdit[Constants.RIGHT_KNEE] );
	FrontBodyPosition.SetLeftKneePosition( m_aptEdit[Constants.LEFT_KNEE] );
	FrontBodyPosition.SetRightAnklePosition( m_aptEdit[Constants.RIGHT_ANKLE] );
	FrontBodyPosition.SetLeftAnklePosition( m_aptEdit[Constants.LEFT_ANKLE] );
	FrontBodyPosition.SetRightBeltPosition( m_aptEdit[Constants.BELT1] );
	FrontBodyPosition.SetLeftBeltPosition( m_aptEdit[Constants.BELT2] );
	FrontBodyPosition.SetRightShoulderPosition( m_aptEdit[Constants.RIGHT_SHOULDER] );
	FrontBodyPosition.SetLeftShoulderPosition( m_aptEdit[Constants.LEFT_SHOULDER] );
	FrontBodyPosition.SetRightEarPosition( m_aptEdit[Constants.RIGHT_EAR] );
	FrontBodyPosition.SetLeftEarPosition( m_aptEdit[Constants.LEFT_EAR] );
	FrontBodyPosition.SetChinPosition( m_aptEdit[Constants.CHIN] );
	FrontBodyPosition.SetGlabellaPosition( m_aptEdit[Constants.MIDDLE_EYEBROWS] );
}

public void SetFrontBodyPosition( FrontBodyPosition FrontBodyPosition )
{
    //FrontBodyPosition.GetRightHipPosition(m_aptEdit[Constants.RIGHT_HIP]);
  m_aptEdit[Constants.RIGHT_HIP] = new Point(FrontBodyPosition.m_ptRightHip.X, FrontBodyPosition.m_ptRightHip.Y);
            //MessageBox.Show(m_aptEdit[Constants.RIGHT_HIP].X.ToString());
            //FrontBodyPosition.GetLeftHipPosition(m_aptEdit[Constants.LEFT_HIP]);
  m_aptEdit[Constants.LEFT_HIP] = new Point(FrontBodyPosition.m_ptLeftHip.X, FrontBodyPosition.m_ptLeftHip.Y);
            //FrontBodyPosition.GetRightKneePosition(m_aptEdit[Constants.RIGHT_KNEE]);
  m_aptEdit[Constants.RIGHT_KNEE] = new Point(FrontBodyPosition.m_ptRightKnee.X, FrontBodyPosition.m_ptRightKnee.Y);
            //FrontBodyPosition.GetLeftKneePosition(m_aptEdit[Constants.LEFT_KNEE]);
  m_aptEdit[Constants.LEFT_KNEE] = new Point(FrontBodyPosition.m_ptLeftKnee.X, FrontBodyPosition.m_ptLeftKnee.Y);
            //FrontBodyPosition.GetRightAnklePosition(m_aptEdit[Constants.RIGHT_ANKLE]);
  m_aptEdit[Constants.RIGHT_ANKLE] = new Point(FrontBodyPosition.m_ptRightAnkle.X, FrontBodyPosition.m_ptRightAnkle.Y);
            //FrontBodyPosition.GetLeftAnklePosition(m_aptEdit[Constants.LEFT_ANKLE]);
  m_aptEdit[Constants.LEFT_ANKLE] = new Point(FrontBodyPosition.m_ptLeftAnkle.X, FrontBodyPosition.m_ptLeftAnkle.Y);
            //FrontBodyPosition.GetRightBeltPosition(m_aptEdit[Constants.BELT1]);
  m_aptEdit[Constants.BELT1] = new Point(FrontBodyPosition.m_ptRightBelt.X, FrontBodyPosition.m_ptRightBelt.Y);
            //FrontBodyPosition.GetLeftBeltPosition(m_aptEdit[Constants.BELT2]);
            m_aptEdit[Constants.BELT2] = new Point(FrontBodyPosition.m_ptLeftBelt.X, FrontBodyPosition.m_ptLeftBelt.Y);

            //FrontBodyPosition.GetRightShoulderPosition(m_aptEdit[Constants.RIGHT_SHOULDER]);
            m_aptEdit[Constants.RIGHT_SHOULDER] = new Point(FrontBodyPosition.m_ptRightShoulder.X, FrontBodyPosition.m_ptRightShoulder.Y);
            //FrontBodyPosition.GetLeftShoulderPosition(m_aptEdit[Constants.LEFT_SHOULDER]);
            m_aptEdit[Constants.LEFT_SHOULDER] = new Point(FrontBodyPosition.m_ptLeftShoulder.X, FrontBodyPosition.m_ptLeftShoulder.Y);
            //FrontBodyPosition.GetRightEarPosition(m_aptEdit[Constants.RIGHT_EAR]);
            m_aptEdit[Constants.RIGHT_EAR] = new Point(FrontBodyPosition.m_ptRightEar.X, FrontBodyPosition.m_ptRightEar.Y);
            //FrontBodyPosition.GetLeftEarPosition(m_aptEdit[Constants.LEFT_EAR]);
            m_aptEdit[Constants.LEFT_EAR] = new Point(FrontBodyPosition.m_ptLeftEar.X, FrontBodyPosition.m_ptLeftEar.Y);
            //FrontBodyPosition.GetChinPosition(m_aptEdit[Constants.CHIN]);
            m_aptEdit[Constants.CHIN] = new Point(FrontBodyPosition.m_ptChin.X, FrontBodyPosition.m_ptChin.Y);
            //FrontBodyPosition.GetGlabellaPosition(m_aptEdit[Constants.MIDDLE_EYEBROWS]);
            m_aptEdit[Constants.MIDDLE_EYEBROWS] = new Point(FrontBodyPosition.m_ptGlabella.X, FrontBodyPosition.m_ptGlabella.Y);
        }

public void SetMarkerSizeBase(int iMarkerSize)
{
    m_iMarkerSizeBase = iMarkerSize;
}

public void SetDataVersion(int iDataVersion)
{
    m_iDataVersion = iDataVersion;
}

public void SetShoulderOffset0(int iShoulderOffset0)
{
    m_iShoulderOffset0 = iShoulderOffset0;
}

public void SetShoulderOffset1(int iShoulderOffset1)
{
    m_iShoulderOffset1 = iShoulderOffset1;
}

public void SetShoulderOffset2(int iShoulderOffset2)
{
    m_iShoulderOffset2 = iShoulderOffset2;
}

public void SetStyleLine(bool bStyleLine)
{
    m_bStyleLine = bStyleLine;
}

public void SetStyleLineStyle(uint uiStyleLineStyle)
{
    m_uiStyleLineStyle = uiStyleLineStyle;
}

public  void SetStyleLineColor(Color crStyleLineColor)
{
    m_crStyleLineColor = crStyleLineColor;
}

public void SetStyleLineWidth(uint uiStyleLineWidth)
{
    m_uiStyleLineWidth = uiStyleLineWidth;
}


        public void UpdateOffscreen(Graphics m_pDCOffscreen)
        {
            if (m_pDCOffscreen == null)
            {
                //return;
            }
         /*   m_iBackgroundHeight = 1024;
            m_iBackgroundWidth = 1280;
            m_iOffscreenWidth = 423;
            m_iOffscreenHeight = 470;
            m_iSrcWidth = 512;
            m_iSrcHeight = 640;*/

            //m_pbyteBits = converterDemo(Yugamiru.Properties.Resources.sokui);
            if (m_pbyteBits != null)
            {
                stretchDIBbits.SetStretchBltMode(m_pDCOffscreen.GetHdc(),StretchBltMode.STRETCH_HALFTONE);
                m_pDCOffscreen.ReleaseHdc();
                stretchDIBbits.StretchDIBits(
                            m_pDCOffscreen.GetHdc(),
                            0, 0, m_iOffscreenWidth, m_iOffscreenHeight,
                            m_iSrcX, (m_iBackgroundHeight - 1 - (m_iSrcY + m_iSrcHeight - 1)),
                            m_iSrcWidth, m_iSrcHeight,
                            m_pbyteBits,
                            ref m_bmi,
                            Constants.DIB_RGB_COLORS,
                            Constants.SRCCOPY);
                            
       /*         stretchDIBbits.StretchDIBits(
              m_pDCOffscreen.GetHdc(),
              0, 0, m_iOffscreenWidth, m_iOffscreenHeight,
              m_iSrcX,//0
              (m_iBackgroundHeight - 1 - (m_iSrcY + m_iSrcHeight - 1)),//0
              m_iSrcWidth,
              m_iSrcHeight,
              m_pbyteBits,
              ref m_bmi,
              Constants.DIB_RGB_COLORS,
              Constants.SRCCOPY);
              */
                m_pDCOffscreen.ReleaseHdc();
            }

            

            /*
        #if 0
            // ƒtƒbƒgƒvƒŒ[ƒg
            if ( theApp.m_CalibInf.isCaliblated ){
                CPoint fpl[4];
                fpl[0].x = ( theApp.m_CalibInf.FPLpos[0][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                fpl[0].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[0][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
                fpl[1].x = ( theApp.m_CalibInf.FPLpos[1][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                fpl[1].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[1][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
                fpl[2].x = ( theApp.m_CalibInf.FPLpos[2][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                fpl[2].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[2][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;
                fpl[3].x = ( theApp.m_CalibInf.FPLpos[3][X] - m_iSrcX ) * m_iOffscreenWidth / m_iSrcWidth;
                fpl[3].y = (( theApp.GetImageHeight() - 1 - theApp.m_CalibInf.FPLpos[3][Y] ) - m_iSrcY ) * m_iOffscreenHeight / m_iSrcHeight;

                MyDrawLine(m_pDCOffscreen, fpl[0], fpl[1], PS_SOLID, RGB(255,0,0));
                MyDrawLine(m_pDCOffscreen, fpl[0], fpl[2], PS_SOLID, RGB(255,0,0));
                MyDrawLine(m_pDCOffscreen, fpl[2], fpl[3], PS_SOLID, RGB(255,0,0));
                MyDrawLine(m_pDCOffscreen, fpl[1], fpl[3], PS_SOLID, RGB(255,0,0));
            }
        #endif
        */
            m_Marker = (int)((double)m_iMarkerSizeBase * m_iBackgroundWidth / (m_iSrcWidth * Constants.IMG_SCALE_MAX));

            Point ptLeftshoulder = new Point(0, 0);
            Point ptRightshoulder = new Point(0, 0);
            Point ptLeftknee = new Point(0, 0);
            Point ptRightknee = new Point(0, 0);
            Point ptLeftear = new Point(0, 0);
            Point ptRightear = new Point(0, 0);
            Point ptLeftbelt = new Point(0, 0);
            Point ptRightbelt = new Point(0, 0);
            Point ptmidface = new Point(0, 0);
            Point pttin = new Point(0, 0);
            Point pt1 = new Point(0, 0);
            Point pt2 = new Point(0, 0);
            Point pt3 = new Point(0, 0);
            int style = Constants.PS_SOLID;
            int width = 3;
            // color = RGB(255, 0, 0);
            Point[] pt = new Point[3];
            Point[] pts = new Point[3];

            Pen penRed = new Pen(Color.Red, 3);
            //CPen* ppenOld = m_pDCOffscreen->SelectObject(&penRed);

            // ŠÖßA’†Sü...

            Point[] dispPos = new Point[14];
            {
                int X;
                int Y;
                int i = 0;
                for (i = 0; i < 8; i++)
                {
                    /* dispPos[i].SetPoint(
                         (m_aptEdit[i].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth,
                         (m_aptEdit[i].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight);*/
                    X = (m_aptEdit[i].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                    Y = (m_aptEdit[i].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
                    dispPos[i] = new Point(X, Y);



                }
            }

            if ((m_iMode == Constants.FRONTJOINTEDITWNDMODE_KNEE) || 
                (m_iMode == Constants.FRONTJOINTEDITWNDMODE_UPPERBODY))
            {
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.RIGHT_HIP], dispPos[Constants.LEFT_HIP]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.RIGHT_HIP], dispPos[Constants.RIGHT_KNEE]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.RIGHT_KNEE], dispPos[Constants.RIGHT_ANKLE]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.LEFT_HIP], dispPos[Constants.LEFT_KNEE]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.LEFT_KNEE], dispPos[Constants.LEFT_ANKLE]);
            }

            {
                int i = 8;
                for (i = 8; i < 14; i++)
                {
                    dispPos[i] = new Point(
                        (m_aptEdit[i].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth,
                        (m_aptEdit[i].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight);
                }
            }

            if (m_iMode == Constants.FRONTJOINTEDITWNDMODE_UPPERBODY)
            {
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.RIGHT_SHOULDER], dispPos[Constants.LEFT_SHOULDER]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.RIGHT_EAR], dispPos[Constants.LEFT_EAR]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.CHIN], dispPos[Constants.MIDDLE_EYEBROWS]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.RIGHT_SHOULDER], dispPos[Constants.RIGHT_HIP]);
                MyDrawLineStyl(m_pDCOffscreen, dispPos[Constants.LEFT_SHOULDER], dispPos[Constants.LEFT_HIP]);
            }

            int offset_sx1 = m_iShoulderOffset0 * m_iOffscreenWidth / m_iSrcWidth;
            int offset_sx2 = m_iShoulderOffset1 * m_iOffscreenWidth / m_iSrcWidth;
            int offset_sy = m_iShoulderOffset2 * m_iOffscreenHeight / m_iSrcHeight;

            if (m_iMode == Constants.FRONTJOINTEDITWNDMODE_UPPERBODY)
            {
                //¶Œ¨ shoulder

                //#if !defined(TOYOTA)

                ptLeftshoulder.X = (m_aptEdit[Constants.LEFT_SHOULDER].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptLeftshoulder.Y = (m_aptEdit[Constants.LEFT_SHOULDER].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt1.X = ptLeftshoulder.X - offset_sx1;
                pt1.Y = ptLeftshoulder.Y - offset_sy;
                pt2.X = ptLeftshoulder.X;
                pt2.Y = ptLeftshoulder.Y - offset_sy;
                pt3.X = ptLeftshoulder.X + offset_sx2;
                pt3.Y = ptLeftshoulder.Y;

                Color m_color = Color.FromArgb(255,0,0);
                MyDrawLine(m_pDCOffscreen, pt1, pt2, style, m_color, width);

                pt[0].X = pt2.X;
                pt[0].Y = pt2.Y;
                pt[1].X = (pt2.X + pt3.X) / 2;
                pt[1].Y = pt2.Y;
                pt[2].X = pt3.X;
                pt[2].Y = pt3.Y;

                //m_pDCOffscreen.DrawLine(penRed, pt[0],pt[1]); 
                //m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                //m_pDCOffscreen->PolyBezierTo(pt, 3);
                m_pDCOffscreen.DrawBezier(penRed, pt[0],pt[1],pt[2],pt[2]);
                //‰EŒ¨ shoulder
                offset_sx1 *= -1;
                offset_sx2 *= -1;
                ptRightshoulder.X = (m_aptEdit[Constants.RIGHT_SHOULDER].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptRightshoulder.Y = (m_aptEdit[Constants.RIGHT_SHOULDER].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt1.X = ptRightshoulder.X - offset_sx1;
                pt1.Y = ptRightshoulder.Y - offset_sy;
                pt2.X = ptRightshoulder.X;
                pt2.Y = ptRightshoulder.Y - offset_sy;
                pt3.X = ptRightshoulder.X + offset_sx2;
                pt3.Y = ptRightshoulder.Y;


                MyDrawLine(m_pDCOffscreen, pt1, pt2, style, m_color, width);

                pt[0].X = pt2.X;
                pt[0].Y = pt2.Y;
                pt[1].X = (pt2.X + pt3.X) / 2;
                pt[1].Y = pt2.Y;
                pt[2].X = pt3.X;
                pt[2].Y = pt3.Y;

                //m_pDCOffscreen.DrawLine(penRed, 0, 0, pt[0].X, pt[0].Y);
                // m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                //    m_pDCOffscreen->PolyBezierTo(pt, 3);
                m_pDCOffscreen.DrawBezier(penRed, pt[0],pt[1],pt[2],pt[2]);

                //#endif

                //‰EŽ¨
                ptRightear.X = (m_aptEdit[Constants.RIGHT_EAR].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptRightear.Y = (m_aptEdit[Constants.RIGHT_EAR].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt[0].X = ptRightear.X + 10 * m_iOffscreenWidth / m_iSrcWidth;
                pt[0].Y = ptRightear.Y + 10 * m_iOffscreenHeight / m_iSrcHeight;
                pt[1].X = ptRightear.X + 5 * m_iOffscreenWidth / m_iSrcWidth;
                pt[1].Y = ptRightear.Y + 15 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = ptRightear.X - 2 * m_iOffscreenWidth / m_iSrcWidth;
                pt[2].Y = ptRightear.Y + 10 * m_iOffscreenHeight / m_iSrcHeight;
                pt1.X = pt[2].X;
                pt1.Y = pt[2].Y;

                //m_pDCOffscreen.DrawLine(penRed, 0, 0, pt[0].X, pt[0].Y);
                // m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                // m_pDCOffscreen->PolyBezierTo(pt, 3);
                Point startline = new Point(pt[2].X,pt[2].Y);
                m_pDCOffscreen.DrawBezier(penRed, pt[0],pt[1],pt[2],pt[2]);

                pt[0].X = ptRightear.X - 7 * m_iOffscreenWidth / m_iSrcWidth;
                pt[0].Y = ptRightear.Y - 5 * m_iOffscreenHeight / m_iSrcHeight;
                pt[1].X = ptRightear.X - 2 * m_iOffscreenWidth / m_iSrcWidth;
                pt[1].Y = ptRightear.Y - 18 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = ptRightear.X + 5 * m_iOffscreenWidth / m_iSrcWidth;
                pt[2].Y = ptRightear.Y - 10 * m_iOffscreenHeight / m_iSrcHeight;
                pt2.X = pt[0].X;
                pt2.Y = pt[0].Y;

                //m_pDCOffscreen.DrawLine(penRed, 0, 0, pt2.X, pt2.Y);
                //   m_pDCOffscreen->LineTo(pt2.x, pt2.y);
                //  m_pDCOffscreen->PolyBezierTo(pt, 3);
                m_pDCOffscreen.DrawLine(penRed,startline.X,startline.Y,pt[0].X,pt[0].Y);
                m_pDCOffscreen.DrawBezier(penRed, pt[0],pt[1],pt[2],pt[2]);

                //¶Ž¨
                ptLeftear.X = (m_aptEdit[Constants.LEFT_EAR].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptLeftear.Y = (m_aptEdit[Constants.LEFT_EAR].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt[0].X = ptLeftear.X - 10 * m_iOffscreenWidth / m_iSrcWidth;
                pt[0].Y = ptLeftear.Y + 10 * m_iOffscreenHeight / m_iSrcHeight;
                pt[1].X = ptLeftear.X - 5 * m_iOffscreenWidth / m_iSrcWidth;
                pt[1].Y = ptLeftear.Y + 15 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = ptLeftear.X + 2 * m_iOffscreenWidth / m_iSrcWidth;
                pt[2].Y = ptLeftear.Y + 10 * m_iOffscreenHeight / m_iSrcHeight;
                pt1.X = pt[2].X;
                pt1.Y = pt[2].Y;


                //m_pDCOffscreen.DrawLine(penRed, 0, 0, pt[0].X, pt[0].Y);

                /*   m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                   m_pDCOffscreen->PolyBezierTo(pt, 3);
                   */
                 startline = new Point(pt[2].X, pt[2].Y);
        Point[] bezierPoints =
          {
                 pt[0], pt[1],pt[2], pt[2]
             };
                //m_pDCOffscreen.DrawCurve(penRed, bezierPoints);
                m_pDCOffscreen.DrawBezier(penRed, pt[0], pt[1], pt[2], pt[2]);// meena

                pt[0].X = ptLeftear.X + 7 * m_iOffscreenWidth / m_iSrcWidth;
                pt[0].Y = ptLeftear.Y - 5 * m_iOffscreenHeight / m_iSrcHeight;
                pt[1].X = ptLeftear.X + 2 * m_iOffscreenWidth / m_iSrcWidth;
                pt[1].Y = ptLeftear.Y - 18 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = ptLeftear.X - 5 * m_iOffscreenWidth / m_iSrcWidth;
                pt[2].Y = ptLeftear.Y - 10 * m_iOffscreenHeight / m_iSrcHeight;
                pt2.X = pt[0].X;
                pt2.Y = pt[0].Y;

                //m_pDCOffscreen.DrawLine(penRed, 0, 0, pt2.X, pt2.Y);
                //   m_pDCOffscreen->LineTo(pt2.x, pt2.y);
                //  m_pDCOffscreen->PolyBezierTo(pt, 3);
                m_pDCOffscreen.DrawLine(penRed,startline.X, startline.Y, pt[0].X, pt[0].Y);
                m_pDCOffscreen.DrawBezier(penRed, pt[0],pt[1],pt[2],pt[2]);

                //‚ ‚² menton
                pttin.X = (m_aptEdit[Constants.CHIN].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                pttin.Y = (m_aptEdit[Constants.CHIN].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt[0].X = pttin.X - 20 * m_iOffscreenWidth / m_iSrcWidth;
                pt[0].Y = pttin.Y;
                pt[1].X = pttin.X;
                pt[1].Y = pttin.Y + 30 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = pttin.X + 20 * m_iOffscreenWidth / m_iSrcWidth;
                pt[2].Y = pttin.Y;

                //m_pDCOffscreen.DrawLine(penRed, 0, 0, pt[0].X, pt[0].Y);
                //  m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                //  m_pDCOffscreen->PolyBezierTo(pt, 3);
                m_pDCOffscreen.DrawBezier(penRed, pt[0],pt[1],pt[2],pt[2]);
            }

            if (m_iMode == Constants.FRONTJOINTEDITWNDMODE_KNEE)
            {

                //‰E•G
                ptRightknee.X = (m_aptEdit[Constants.RIGHT_KNEE].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptRightknee.Y = (m_aptEdit[Constants.RIGHT_KNEE].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
                Color m_color = Color.FromArgb(255,0,0);
                
                MyDrawPoint(m_pDCOffscreen, ptRightknee.X, ptRightknee.Y, (15 * m_iOffscreenWidth / m_iSrcWidth), /*RGB(255, 0, 0)*/m_color, false);

                //¶•G
                ptLeftknee.X = (m_aptEdit[Constants.LEFT_KNEE].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptLeftknee.Y = (m_aptEdit[Constants.LEFT_KNEE].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;
                MyDrawPoint(m_pDCOffscreen, ptLeftknee.X, ptLeftknee.Y, (15 * m_iOffscreenWidth / m_iSrcWidth), /*RGB(255, 0, 0)*/m_color, false);
            }

            if (m_iMode == Constants.FRONTJOINTEDITWNDMODE_ANKLE_AND_HIP)
            {

                //‰EBelt
                ptRightbelt.X = (m_aptEdit[Constants.BELT1].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptRightbelt.Y = (m_aptEdit[Constants.BELT1].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt[0].X = ptRightbelt.X;
                pt[0].Y = ptRightbelt.Y + 12 * m_iOffscreenHeight / m_iSrcHeight;
                pt[1].X = ptRightbelt.X;
                pt[1].Y = ptRightbelt.Y - 12 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = ptRightbelt.X;
                pt[2].Y = ptRightbelt.Y + 6 * m_iOffscreenHeight / m_iSrcHeight;
                pts[0].X = ptRightbelt.X;
                pts[0].Y = ptRightbelt.Y - 6 * m_iOffscreenHeight / m_iSrcHeight;
                pts[1].X = ptRightbelt.X + 12 * m_iOffscreenWidth / m_iSrcWidth;
                pts[1].Y = ptRightbelt.Y + 6 * m_iOffscreenHeight / m_iSrcHeight;
                pts[2].X = ptRightbelt.X + 12 * m_iOffscreenWidth / m_iSrcWidth;
                pts[2].Y = ptRightbelt.Y - 6 * m_iOffscreenHeight / m_iSrcHeight;

                //   m_pDCOffscreen->SelectObject(ppenOld);
                //  CPen penRed2(PS_SOLID, 3, RGB(255, 0, 0));
                Pen penRed2 = new Pen(Color.Red, 3);
                penRed2.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                //ppenOld = m_pDCOffscreen->SelectObject(&penRed2);
                //ppenOld = penRed2;

                //#if !defined(TOYOTA)
                m_pDCOffscreen.DrawLine(penRed2, pt[0].X, pt[0].Y, pt[1].X, pt[1].Y);
                // m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                // m_pDCOffscreen->LineTo(pt[1].x, pt[1].y);
                //#endif

                /* m_pDCOffscreen->SelectObject(ppenOld);
                 CPen penRed3(PS_INSIDEFRAME, 10, RGB(255, 0, 0));
                 ppenOld = m_pDCOffscreen->SelectObject(&penRed3);*/
                Pen penRed3 = new Pen(Color.Red, 1);
                SolidBrush brush_Red = new SolidBrush(Color.Red);

                //#if defined(TOYOTA)
                Color m_color = Color.FromArgb(255,0,0);
                //MyDrawPoint(m_pDCOffscreen, ptRightbelt.X, ptRightbelt.Y, m_Marker, /*RGB(255,0,0)*/ m_color, true);
                //#else
                //m_pDCOffscreen.DrawRectangle(penRed3, pt[2].X, pt[2].Y, pts[2].X, pts[2].Y);
                //DrawRectangle(penRed3, pt[2].X - (pts[2].X - pt[2].X), pts[2].Y, pts[2].X - pt[2].X, pt[2].Y - pts[2].Y);
                //m_pDCOffscreen.FillRectangle(brush_Red, pt[2].X - (pts[2].X - pt[2].X), pts[2].Y, pts[2].X - pt[2].X, pt[2].Y - pts[2].Y);
                m_pDCOffscreen.FillRectangle(brush_Red, pt[2].X,pts[2].Y,pts[2].X -pt[2].X,pt[2].Y - pts[2].Y) ;
                //MessageBox.Show(pt[2].X.ToString()+","+ pt[2].Y.ToString()+","+ (pts[2].X - pt[2].X).ToString()+"," +(pt[2].Y - pts[2].Y).ToString());
                
                //    m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
                // m_pDCOffscreen->Rectangle(pt[2].x, pt[2].y, pts[2].x, pts[2].y);
                //#endif
                //¶Belt
                ptLeftbelt.X = (m_aptEdit[Constants.BELT2].X - m_iSrcX) * m_iOffscreenWidth / m_iSrcWidth;
                ptLeftbelt.Y = (m_aptEdit[Constants.BELT2].Y - m_iSrcY) * m_iOffscreenHeight / m_iSrcHeight;

                pt[0].X = ptLeftbelt.X;
                pt[0].Y = ptLeftbelt.Y + 12 * m_iOffscreenHeight / m_iSrcHeight;
                pt[1].X = ptLeftbelt.X;
                pt[1].Y = ptLeftbelt.Y - 12 * m_iOffscreenHeight / m_iSrcHeight;
                pt[2].X = ptLeftbelt.X;
                pt[2].Y = ptLeftbelt.Y + 6 * m_iOffscreenHeight / m_iSrcHeight;
                pts[0].X = ptLeftbelt.X;
                pts[0].Y = ptLeftbelt.Y - 6 * m_iOffscreenHeight / m_iSrcHeight;
                pts[1].X = ptLeftbelt.X - 12 * m_iOffscreenWidth / m_iSrcWidth;
                pts[1].Y = ptLeftbelt.Y + 6 * m_iOffscreenHeight / m_iSrcHeight;
                pts[2].X = ptLeftbelt.X - 12 * m_iOffscreenWidth / m_iSrcWidth;
                pts[2].Y = ptLeftbelt.Y - 6 * m_iOffscreenHeight / m_iSrcHeight;

                /*    m_pDCOffscreen->SelectObject(ppenOld);
                    CPen penRed4(PS_SOLID, 3, RGB(255, 0, 0));
                    ppenOld = m_pDCOffscreen->SelectObject(&penRed4);*/
                Pen penRed4 = new Pen(Color.Red, 3);
                penRed4.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                //#if !defined(TOYOTA)
                /*     m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
             m_pDCOffscreen->LineTo(pt[1].x, pt[1].y);*/
                m_pDCOffscreen.DrawLine(penRed4, pt[0], pt[1]);
                //#endif
                /*
                        m_pDCOffscreen->SelectObject(ppenOld);
                        CPen penRed5(PS_INSIDEFRAME, 10, RGB(255, 0, 0));
                        ppenOld = m_pDCOffscreen->SelectObject(&penRed5);
                        */
                Pen penRed5 = new Pen(Color.Red, 1);
                penRed5.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                        
                /*
                #if defined(TOYOTA)*/
                        //MyDrawPoint(m_pDCOffscreen, ptLeftbelt.X, ptLeftbelt.Y, m_Marker, m_color, true);
                /*#else
                /*      m_pDCOffscreen->MoveTo(pt[0].x, pt[0].y);
              m_pDCOffscreen->Rectangle(pt[2].x, pt[2].y, pts[2].x, pts[2].y);*/
                //m_pDCOffscreen.DrawRectangle(penRed5, pt[2].X, pt[2].Y, pts[2].X, pts[2].Y);
                
                //m_pDCOffscreen.DrawRectangle(penRed5, pt[2].X, pts[2].Y, pt[2].X - pts[2].X, pt[2].Y - pts[2].Y);
                //m_pDCOffscreen.FillRectangle(brush_Red, pt[2].X, pts[2].Y, pt[2].X - pts[2].X, pt[2].Y - pts[2].Y);
                m_pDCOffscreen.FillRectangle(brush_Red, pt[2].X - (pt[2].X - pts[2].X), pts[2].Y, pt[2].X - pts[2].X, pt[2].Y - pts[2].Y);
                //#endif

            }
            /*
                // Draw Points
                if (ppenOld != NULL)
                {
                    m_pDCOffscreen->SelectObject(ppenOld);
                    ppenOld = NULL;
                }
                ppenOld = m_pDCOffscreen->SelectObject(&penRed);
                */
            // RIGHT_EAR = 10, LEFT_EAR = 11, CHIN = 12, MIDDLE_EYEBROWS = 13, TALON = 14, FOOT_CURVE = 15 };
            if (m_isValidPoint[Constants.RIGHT_HIP])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.RIGHT_HIP].X), (int)(dispPos[Constants.RIGHT_HIP].Y), m_Marker, (m_isValidEditPoint[Constants.RIGHT_HIP]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            if (m_isValidPoint[Constants.LEFT_HIP])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.LEFT_HIP].X), (int)(dispPos[Constants.LEFT_HIP].Y), m_Marker, (m_isValidEditPoint[Constants.LEFT_HIP]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            if (m_isValidPoint[Constants.RIGHT_KNEE])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.RIGHT_KNEE].X), (int)(dispPos[Constants.RIGHT_KNEE].Y), m_Marker, (m_isValidEditPoint[Constants.RIGHT_KNEE]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            if (m_isValidPoint[Constants.LEFT_KNEE])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.LEFT_KNEE].X), (int)(dispPos[Constants.LEFT_KNEE].Y), m_Marker, (m_isValidEditPoint[Constants.LEFT_KNEE]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            if (m_isValidPoint[Constants.RIGHT_ANKLE])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.RIGHT_ANKLE].X), (int)(dispPos[Constants.RIGHT_ANKLE].Y), m_Marker, (m_isValidEditPoint[Constants.RIGHT_ANKLE]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            if (m_isValidPoint[Constants.LEFT_ANKLE])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.LEFT_ANKLE].X), (int)(dispPos[Constants.LEFT_ANKLE].Y), m_Marker, (m_isValidEditPoint[Constants.LEFT_ANKLE]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            //	if(m_isValidPoint[BELT1]){
            //		MyDrawPoint(m_pDCOffscreen, int(dispPos[BELT1].x), int(dispPos[BELT1].y), m_Marker, (m_isValidEditPoint[BELT1]) ? JOINTS_COLOR : INVALID_JOINTS_COLOR, true);
            //	}
            //	if(m_isValidPoint[BELT2]){
            //		MyDrawPoint(m_pDCOffscreen, int(dispPos[BELT2].x), int(dispPos[BELT2].y), m_Marker, (m_isValidEditPoint[BELT2]) ? JOINTS_COLOR : INVALID_JOINTS_COLOR, true);
            //	}
            if (m_isValidPoint[Constants.RIGHT_SHOULDER])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.RIGHT_SHOULDER].X), (int)(dispPos[Constants.RIGHT_SHOULDER].Y), (int)(m_Marker / 1.3), (m_isValidEditPoint[Constants.RIGHT_SHOULDER]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            if (m_isValidPoint[Constants.LEFT_SHOULDER])
            {
                MyDrawPoint(m_pDCOffscreen, (int)(dispPos[Constants.LEFT_SHOULDER].X), (int)(dispPos[Constants.LEFT_SHOULDER].Y), (int)(m_Marker / 1.3), (m_isValidEditPoint[Constants.LEFT_SHOULDER]) ? Color.FromArgb(255, 0, 0) : Color.FromArgb(108, 108, 30), true);
            }
            Color color = new Color();
            for (int i = 10; i < 14; i++)
            {
                if (m_isValidPoint[i])
                {
                    if (m_isValidEditPoint[i])
                    {
                        color = Color.FromArgb(255, 0, 0);
                    }
                    else
                    {
                        color = Color.FromArgb(108, 108, 30);
                    }
                    MyDrawPoint(m_pDCOffscreen, (int)(dispPos[i].X), (int)(dispPos[i].Y), (int)(m_Marker / 1.8), color, true);
                }
            }
        }


    }
}
