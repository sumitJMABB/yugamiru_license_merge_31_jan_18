﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Yugamiru
{
    public partial class IDD_BALANCELABO : Form
    {

        JointEditDoc m_JointEditDoc;
        public int screen_width = 0;


        public IDD_BALANCELABO(JointEditDoc GetDocument)
        {
            InitializeComponent();
            GetDocument.m_FrontBodyPositionKneedown = null;
            GetDocument.m_FrontBodyPositionStanding = null;
            GetDocument.m_SideImageBytes = null;

            GetDocument.m_FrontKneedownImageBytes = null;
            GetDocument.m_FrontStandingImageBytes = null;

           
            

            if (IDC_MeasurementBtn.Image != null)
                IDC_MeasurementBtn.Image.Dispose();
            else
            {
                if (GetDocument.GetInputMode() == Constants.INPUTMODE_NONE)
                    IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_up;
                else
                    IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_on;
            }
            IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_up;
            IDC_CloseBtn.Image = Yugamiru.Properties.Resources.end_up;
            IDC_SETTING_BTN.Image = Yugamiru.Properties.Resources.setting_up;
            m_JointEditDoc = GetDocument;

        }


        private void IDD_BALANCELABO_Load(object sender, EventArgs e)
        {

        }

        private void IDC_MeasurementBtn_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_NEW);
            m_JointEditDoc.ClearMakerTouchFlag();
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            m_JointEditDoc.InitBodyBalance();
            m_JointEditDoc.ClearStandingImage();
            m_JointEditDoc.ClearKneedownImage();
            m_JointEditDoc.ClearSideImage();
            m_JointEditDoc.SetDataMeasurementTime("");
            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.ChangeToMeasurementView();

            //CloseForm(EventArgs.Empty); // triggerring the event, to send it to form1 which is base form - step3
            this.Visible = false;
            m_JointEditDoc.GetMeasurementDlg().Visible = true;
            m_JointEditDoc.GetMeasurementDlg().RefreshForm();
            //this.Close();
            //DisposeControls();

        }

        public void IDD_BALANCELABO_SizeChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("I am child form");
            if (screen_width > 0)
            {
                //MessageBox.Show(screen_width.ToString());
                this.Width = screen_width;
            }

            this.IDC_MeasurementBtn.Size = new Size(112, 42);
            this.IDC_MeasurementBtn.Location = new Point(350, 600);

            this.IDC_AnalysisBtn.Size = new Size(112, 42);
            this.IDC_AnalysisBtn.Location = new Point(580 + 50, 600);

            this.IDC_CloseBtn.Size = new Size(112, 42);
            this.IDC_CloseBtn.Location = new Point(730 + 50, 600);

            this.IDC_SETTING_BTN.Size = new Size(112, 42);
            this.IDC_SETTING_BTN.Location = new Point(880 + 50, 600);


            IDC_MeasurementBtn.Left = this.Width / 2 - IDC_MeasurementBtn.Left;
            IDC_AnalysisBtn.Left = IDC_MeasurementBtn.Left + IDC_AnalysisBtn.Width + 90;
            IDC_CloseBtn.Left = IDC_AnalysisBtn.Left + IDC_CloseBtn.Width + 90;
            IDC_SETTING_BTN.Left = IDC_CloseBtn.Left + IDC_SETTING_BTN.Width + 90;
        }
        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_CloseBtn_Click(object sender, EventArgs e)
        {
            // DialogResult dialogResult = MessageBox.Show(
            //     Yugamiru.Properties.Resources.WARNING2/*"if application is closed, current data will be lost. close this application OK?"*/,
            //     "Yugamiru", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            // if (dialogResult == DialogResult.Yes)
            {
                this.Close();
                DisposeControls();
                Application.Exit();
            }
            //  else if (dialogResult == DialogResult.No)
            {
                //do something else
            }



        }

        private void IDC_SETTING_BTN_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetSettingMode(Constants.SETTING_SCREEN_MODE_TRUE);
            /*this.Close();
            DisposeControls();
            FunctionToOpenSettingScreen(EventArgs.Empty);*/
            this.Visible = false;
            m_JointEditDoc.GetSettingView().RefreshForms();
            m_JointEditDoc.GetSettingView().Visible = true;
        }
        public event EventHandler OpenSettingScreen; // creating event handler - step1
        public void FunctionToOpenSettingScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = OpenSettingScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public void DisposeControls()
        {
            IDC_MeasurementBtn.Image.Dispose();
            IDC_AnalysisBtn.Image.Dispose();
            IDC_CloseBtn.Image.Dispose();
            IDC_SETTING_BTN.Image.Dispose();

            Yugamiru.Properties.Resources.startgreen_on.Dispose();
            Yugamiru.Properties.Resources.dataload_up.Dispose();
            Yugamiru.Properties.Resources.end_up.Dispose();
            Yugamiru.Properties.Resources.setting_up.Dispose();
            Yugamiru.Properties.Resources.Mainpic.Dispose();


            this.Dispose();
            this.Close();
        }

        private void IDD_BALANCELABO_Paint(object sender, PaintEventArgs e)
        {
            if (m_JointEditDoc.GetQRCodeFlag())
            {
                // using (m_JointEditDoc.GetQRCodeImage())
                {
                   // e.Graphics.DrawImage(m_JointEditDoc.GetQRCodeImage(), 0, 0);
                    //if(m_JointEditDoc.GetQRCodeImage() != null)
                    e.Graphics.DrawImage(m_JointEditDoc.GetQRCodeImage(), (this.Width - 600) / 2, /*(this.Height - 750)/2,*/0, 600, 600);
                }
            }
            else
            {
                using (Yugamiru.Properties.Resources.Mainpic)
                {
                    e.Graphics.DrawImage(Yugamiru.Properties.Resources.Mainpic, (this.Width - 900) / 2, /*(this.Height - 750) / 2,*/0, 900, 700);
                }
            }
            //e.Graphics.DrawImage(bmBack1, 150 + 70, 10, 900, 700);

        }
        public void RefreshForms()
        {
          
            this.Invalidate();

            if (m_JointEditDoc.GetInputMode() == Constants.INPUTMODE_NONE)
                IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_up;
            else
                IDC_MeasurementBtn.Image = Yugamiru.Properties.Resources.startgreen_on;

            IDC_AnalysisBtn.Image = Yugamiru.Properties.Resources.dataload_up;
            IDC_CloseBtn.Image = Yugamiru.Properties.Resources.end_up;
            IDC_SETTING_BTN.Image = Yugamiru.Properties.Resources.setting_up;
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(true);
        }

        private void IDC_AnalysisBtn_Click(object sender, EventArgs e)
        {
            /*
            OpenResult obj = new OpenResult(m_JointEditDoc);
            obj.EventToChangeResultView += Obj_EventToChangeResultView; ;
            obj.Show();*/
            this.Visible = false;
            m_JointEditDoc.GetOpenResult().Visible = true;
            m_JointEditDoc.GetOpenResult().RefreshForms();
            
        }

        private void Obj_EventToChangeResultView(object sender, EventArgs e)
        {
            FunctionToOpenSettingScreen(EventArgs.Empty);
        }

        public void IDD_BALANCELABO_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(this.Visible.ToString());
     /*       if (e.KeyCode == Keys.A && this.Visible == true)
            {
                //IDD_ABOUTBOX version = new IDD_ABOUTBOX();
                //Form fc = Application.OpenForms["IDD_ABOUTBOX"]; // to find form is already open

                if (Application.OpenForms.OfType<IDD_ABOUTBOX>().Count() == 1)
                    Application.OpenForms.OfType<IDD_ABOUTBOX>().First().Close();

               
                    IDD_ABOUTBOX version = new IDD_ABOUTBOX();
                    version.Show();
                
                    
                //write code for your shortcut action
            }*/
        }
    }
}
