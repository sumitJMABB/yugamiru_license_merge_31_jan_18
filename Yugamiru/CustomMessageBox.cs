﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class CustomMessageBox : Form
    {
        JointEditDoc m_JointEditDoc;

        public CustomMessageBox(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;
            radioButton2.Checked = true;
        }

        private void CustomMessageBox_Load(object sender, EventArgs e)
        {

        }

        private void button_Ok_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true) // Add as new record
            {
                m_JointEditDoc.SetUpdateFlag(false);
            }
            if(radioButton2.Checked == true) // Update existing record
            {
                m_JointEditDoc.SetUpdateFlag(true);
            }
            this.Close();           
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
