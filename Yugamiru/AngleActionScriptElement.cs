﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class AngleActionScriptElement
    {
        public int m_iScriptOpecodeID;         // ƒIƒyƒR[ƒh‚h‚c.
        public int m_iBodyPositionTypeID;      // ‘ÌˆÊƒ^ƒCƒv‚h‚c.
        public int m_iBodyAngleID;             // Šp“x‚h‚c.
        public int m_iMinValueOperatorID;      // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        public int m_iMaxValueOperatorID;      // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        public double m_dMinValue;             // Å¬’l.
        public double m_dMaxValue;             // Å‘å’l.
        public int m_iMuscleStatePattenID;     // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“‚h‚c.
        public int m_iMuscleStateLevel;        // ‹Ø“÷ó‘ÔƒŒƒxƒ‹.
        public string m_strRangeSymbol;

        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // Šp“xƒAƒNƒVƒ‡ƒ“ƒXƒNƒŠƒvƒg‚Ìƒ}ƒXƒ^[ƒf[ƒ^‚ÌƒXƒL[ƒ}.
        public AngleActionScriptElement()
        {

            m_iScriptOpecodeID = Constants.ANGLESCRIPTOPECODEID_NOP;    // ƒIƒyƒR[ƒh‚h‚c.

            m_iBodyPositionTypeID = Constants.BODYPOSITIONTYPEID_NONE;  // ‘ÌˆÊƒ^ƒCƒv‚h‚c.

            m_iBodyAngleID = Constants.BODYANGLEID_NONE;                    // Šp“x‚h‚c.

            m_iMinValueOperatorID = Constants.COMPAREOPERATORID_NONE;       // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMaxValueOperatorID = Constants.COMPAREOPERATORID_NONE;     // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

            m_dMinValue = 0.0;                                  // Å¬’l.

            m_dMaxValue = 0.0;                              // Å‘å’l.

            m_iMuscleStatePattenID = Constants.MUSCLESTATEPATTERNID_NONE; // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“‚h‚c.

            m_iMuscleStateLevel = 0;                                // ‹Ø“÷ó‘ÔƒŒƒxƒ‹.

            m_strRangeSymbol = string.Empty;                                // ”ÍˆÍƒVƒ“ƒ{ƒ‹.

        }


        public void ResolveRange( int iMinValueOperatorID,  int iMaxValueOperatorID,  double dMinValue,  double dMaxValue)
        {
            m_iMinValueOperatorID = iMinValueOperatorID;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
            m_iMaxValueOperatorID = iMaxValueOperatorID;    // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
            m_dMinValue = dMinValue;            // Å¬’l.
            m_dMaxValue = dMaxValue;            // Å‘å’l.
        }

        public bool IsSatisfied(double dValue)
        {
            if (m_iMinValueOperatorID == Constants.COMPAREOPERATORID_GREATER)
            {
                if (!(dValue > m_dMinValue))
                {
                    return false;
                }
            }

            if (m_iMinValueOperatorID == Constants.COMPAREOPERATORID_GREATEROREQUAL)
            {
                if (!(dValue >= m_dMinValue))
                {
                    return false;
                }
            }

            if (m_iMaxValueOperatorID == Constants.COMPAREOPERATORID_LESS)
            {
                if (!(dValue < m_dMaxValue))
                {
                    return false;
                }
            }

            if (m_iMaxValueOperatorID == Constants.COMPAREOPERATORID_LESSOREQUAL)
            {
                if (!(dValue <= m_dMaxValue))
                {
                    return false;
                }
            }

            return true;
        }

        public int ReadFromString(ref char[] lpszText)
        {
            string strFunctionNameSymbol = string.Empty;

            TextBuffer TextBuffer = new TextBuffer(lpszText);

            TextBuffer.SkipSpaceOrTab();
            if (TextBuffer.IsEOF())
            {
                return (Constants.READFROMSTRING_NULLLINE);
            }

            if (TextBuffer.ReadNewLine())
            {
                if (TextBuffer.IsEOF())
                {
                    return (Constants.READFROMSTRING_NULLLINE);
                }
            }

            if (!TextBuffer.ReadSymbol(ref strFunctionNameSymbol))
            {
                return (Constants.READFROMSTRING_SYNTAX_ERROR);
            }

            TextBuffer.SkipSpaceOrTab();
            if (!TextBuffer.ReadLeftParenthesis())
            {
                return (Constants.READFROMSTRING_SYNTAX_ERROR);
            }
            if (strFunctionNameSymbol == "StartAngleCondition")
            {
                string strBodyPositionTypeSymbol = string.Empty;

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSymbol(ref strBodyPositionTypeSymbol))
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadRightParenthesis())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSemicolon())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                TextBuffer.ReadNewLine();
                if (!TextBuffer.IsEOF())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                m_iScriptOpecodeID = Constants.ANGLESCRIPTOPECODEID_START;
                m_iBodyPositionTypeID = m_SymbolFunc.GetBodyPositionTypeIDBySymbol(strBodyPositionTypeSymbol);
                if (m_iBodyPositionTypeID == Constants.BODYPOSITIONTYPEID_NONE)
                {
                    return (Constants.READFROMSTRING_BODYPOSITIONTYPE_INVALID);
                }
                return (Constants.READFROMSTRING_SYNTAX_OK);
            }
            else if (strFunctionNameSymbol == "CheckAngleCondition")
            {
                string strBodyAngleSymbol = string.Empty;
                string strRangeSymbol = string.Empty;

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSymbol(ref strBodyAngleSymbol))
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadComma())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSymbol(ref strRangeSymbol))
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadRightParenthesis())
                {
                    return 0;
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSemicolon())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                TextBuffer.ReadNewLine();
                if (!TextBuffer.IsEOF())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                m_iScriptOpecodeID = Constants.ANGLESCRIPTOPECODEID_CHECK;
                m_iBodyAngleID = m_SymbolFunc.GetBodyAngleIDBySymbol(strBodyAngleSymbol);
                if (m_iBodyAngleID == Constants.BODYANGLEID_NONE)
                {
                    return (Constants.READFROMSTRING_BODYANGLE_INVALID);
                }
                m_strRangeSymbol = strRangeSymbol;
                return (Constants.READFROMSTRING_SYNTAX_OK);
            }
            else if (strFunctionNameSymbol == "EndAngleCondition")
            {
                string strMuscleStatePatternSymbol = string.Empty;
                string strValue = string.Empty;

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSymbol(ref strMuscleStatePatternSymbol))
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadComma())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadValue(ref strValue))
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadRightParenthesis())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                if (!TextBuffer.ReadSemicolon())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                TextBuffer.SkipSpaceOrTab();
                TextBuffer.ReadNewLine();
                if (!TextBuffer.IsEOF())
                {
                    return (Constants.READFROMSTRING_SYNTAX_ERROR);
                }

                m_iScriptOpecodeID = Constants.ANGLESCRIPTOPECODEID_END;
                m_iMuscleStatePattenID = m_SymbolFunc.GetMuscleStatePatternIDBySymbol(strMuscleStatePatternSymbol);
                if (m_iMuscleStatePattenID == Constants.MUSCLESTATEPATTERNID_NONE)
                {
                    return (Constants.READFROMSTRING_MUSCLESTATEPATTERN_INVALID);
                }
                if (strValue == "")
                    m_iMuscleStateLevel = 0;
                else
                    m_iMuscleStateLevel = Convert.ToInt32(strValue);
                return (Constants.READFROMSTRING_SYNTAX_OK);
            }

            return (Constants.READFROMSTRING_OPECODE_INVALID);
        }

    }
}
