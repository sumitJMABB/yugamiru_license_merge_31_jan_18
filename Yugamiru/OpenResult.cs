﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class OpenResult : Form
    {
        JointEditDoc m_JointEditDoc;
        bool SearchById_Flag = false;
        bool SearchByIdAndName_Flag = false;
        bool SearchByName_Flag = false;

        private int PgSize = 20;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;

        public OpenResult(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;

            /*dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yy-MM-dd";*/
            dateTimePicker1.Enabled = false;

            /* dateTimePicker2.Format = DateTimePickerFormat.Custom;
             dateTimePicker2.CustomFormat = "yy-MM-dd";*/
            dateTimePicker2.Enabled = false;
            
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            LoadDataGridWithDefaultValues();

            textbox_PageNo.TextAlign = HorizontalAlignment.Center;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var selectedRow = dataGridView1.SelectedRows[0];

            var primaryKey = int.Parse(selectedRow.Cells[3].Value.ToString());
            m_JointEditDoc.SetUniqueId(primaryKey);

            LoadPatientDetails(primaryKey);
            LoadFrontBodyPositionStandingValues(primaryKey);
            LoadFrontBodyPositionKneedownValues(primaryKey);
            LoadSideBodyPositionValues(primaryKey);
            this.Visible = false;
            FunctionToChangeResultView(EventArgs.Empty);


            /* 
             *Code to write image in a jpeg file on desktop
             *
             *
             * byte[] bytes = null;
                Image temp;
             * bytes = Convert.FromBase64String(row["imagebytes"].ToString());
             * MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
               ms.Write(bytes, 0, bytes.Length);
               temp = Image.FromStream(ms, true);

               Image<Bgr, byte> Emgu_stand_image =
                   new Image<Bgr, byte>(new Bitmap(temp));
               Emgu_stand_image = Emgu_stand_image.Resize(1024, 1280,
                   Emgu.CV.CvEnum.Inter.Linear);

               using (Bitmap bmp = new Bitmap(Emgu_stand_image.ToBitmap()))
               {
                   bmp.Save(@"C:\Users\Meena\Desktop\newimage.jpg");
               }
   */
        }
        public event EventHandler EventToChangeResultView; // creating event handler - step1
        public void FunctionToChangeResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToChangeResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        private void LoadPatientDetails(int uniqueKey)
        {
            string query = "select patientid,name,gender,year,month,date,height,comment,BenchmarkDistance," +
                "MeasurementTime from patientdetails where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                m_JointEditDoc.SetDataID(row[0].ToString());
                m_JointEditDoc.SetDataName(row[1].ToString());
                m_JointEditDoc.SetDataGender(int.Parse(row[2].ToString()));
                m_JointEditDoc.SetDataDoB(row[3].ToString(), row[4].ToString(), row[5].ToString());
                m_JointEditDoc.SetDataHeight(float.Parse(row[6].ToString()));
                m_JointEditDoc.SetDataMeasurementTime(row[9].ToString());
                m_JointEditDoc.SetDataComment(row[7].ToString());
                m_JointEditDoc.SetBenchmarkDistance(int.Parse(row[8].ToString()));

            }
        }
        private void LoadFrontBodyPositionStandingValues(int uniqueKey)
        {
            string query = "select * from FrontBodyPositionStanding where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                FrontBodyPosition Standing_Position = new FrontBodyPosition();
                if (bool.Parse(row[1].ToString()))
                    Standing_Position.SetKneePositionDetected();
                if (bool.Parse(row[2].ToString()))
                    Standing_Position.SetUnderBodYPositionDetected();
                if (bool.Parse(row[3].ToString()))
                    Standing_Position.SetUpperBodyPositionDetected();

                Standing_Position.m_ptChin = new Point(int.Parse(row[4].ToString()),
                                                       int.Parse(row[5].ToString()));
                Standing_Position.m_ptGlabella = new Point(int.Parse(row[6].ToString()),
                                                       int.Parse(row[7].ToString()));
                Standing_Position.m_ptLeftAnkle = new Point(int.Parse(row[8].ToString()),
                                                       int.Parse(row[9].ToString()));
                Standing_Position.m_ptLeftBelt = new Point(int.Parse(row[10].ToString()),
                                                       int.Parse(row[11].ToString()));
                Standing_Position.m_ptLeftEar = new Point(int.Parse(row[12].ToString()),
                                                       int.Parse(row[13].ToString()));
                Standing_Position.m_ptLeftHip = new Point(int.Parse(row[14].ToString()),
                                                       int.Parse(row[15].ToString()));
                Standing_Position.m_ptLeftKnee = new Point(int.Parse(row[16].ToString()),
                                                       int.Parse(row[17].ToString()));
                Standing_Position.m_ptLeftShoulder = new Point(int.Parse(row[18].ToString()),
                                                       int.Parse(row[19].ToString()));
                Standing_Position.m_ptRightAnkle = new Point(int.Parse(row[20].ToString()),
                                                       int.Parse(row[21].ToString()));
                Standing_Position.m_ptRightBelt = new Point(int.Parse(row[22].ToString()),
                                                       int.Parse(row[23].ToString()));
                Standing_Position.m_ptRightEar = new Point(int.Parse(row[24].ToString()),
                                                       int.Parse(row[25].ToString()));
                Standing_Position.m_ptRightHip = new Point(int.Parse(row[26].ToString()),
                                                       int.Parse(row[27].ToString()));
                Standing_Position.m_ptRightKnee = new Point(int.Parse(row[28].ToString()),
                                                       int.Parse(row[29].ToString()));
                Standing_Position.m_ptRightShoulder = new Point(int.Parse(row[30].ToString()),
                                                       int.Parse(row[31].ToString()));

                byte[] bytes = null;
                Image temp;
                bytes = Convert.FromBase64String(row["imagebytes"].ToString());
                MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_image =
                    new Image<Bgr, byte>(new Bitmap(temp));
                Emgu_image = Emgu_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.AllocStandingImage(Emgu_image.Bytes);
                m_JointEditDoc.SetStandingFrontBodyPosition(Standing_Position);

            }

        }
        private void LoadFrontBodyPositionKneedownValues(int uniqueKey)
        {
            string query = "select * from FrontBodyPositionKneedown where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                FrontBodyPosition Kneedown_Position = new FrontBodyPosition();
                if (bool.Parse(row[1].ToString()))
                    Kneedown_Position.SetKneePositionDetected();
                if (bool.Parse(row[2].ToString()))
                    Kneedown_Position.SetUnderBodYPositionDetected();
                if (bool.Parse(row[3].ToString()))
                    Kneedown_Position.SetUpperBodyPositionDetected();

                Kneedown_Position.m_ptChin = new Point(int.Parse(row[4].ToString()),
                                                       int.Parse(row[5].ToString()));
                Kneedown_Position.m_ptGlabella = new Point(int.Parse(row[6].ToString()),
                                                       int.Parse(row[7].ToString()));
                Kneedown_Position.m_ptLeftAnkle = new Point(int.Parse(row[8].ToString()),
                                                       int.Parse(row[9].ToString()));
                Kneedown_Position.m_ptLeftBelt = new Point(int.Parse(row[10].ToString()),
                                                       int.Parse(row[11].ToString()));
                Kneedown_Position.m_ptLeftEar = new Point(int.Parse(row[12].ToString()),
                                                       int.Parse(row[13].ToString()));
                Kneedown_Position.m_ptLeftHip = new Point(int.Parse(row[14].ToString()),
                                                       int.Parse(row[15].ToString()));
                Kneedown_Position.m_ptLeftKnee = new Point(int.Parse(row[16].ToString()),
                                                       int.Parse(row[17].ToString()));
                Kneedown_Position.m_ptLeftShoulder = new Point(int.Parse(row[18].ToString()),
                                                       int.Parse(row[19].ToString()));
                Kneedown_Position.m_ptRightAnkle = new Point(int.Parse(row[20].ToString()),
                                                       int.Parse(row[21].ToString()));
                Kneedown_Position.m_ptRightBelt = new Point(int.Parse(row[22].ToString()),
                                                       int.Parse(row[23].ToString()));
                Kneedown_Position.m_ptRightEar = new Point(int.Parse(row[24].ToString()),
                                                       int.Parse(row[25].ToString()));
                Kneedown_Position.m_ptRightHip = new Point(int.Parse(row[26].ToString()),
                                                       int.Parse(row[27].ToString()));
                Kneedown_Position.m_ptRightKnee = new Point(int.Parse(row[28].ToString()),
                                                       int.Parse(row[29].ToString()));
                Kneedown_Position.m_ptRightShoulder = new Point(int.Parse(row[30].ToString()),
                                                       int.Parse(row[31].ToString()));
                m_JointEditDoc.SetStandingFrontBodyPosition(Kneedown_Position);

                byte[] bytes = null;
                Image temp;
                bytes = Convert.FromBase64String(row["imagebytes"].ToString());
                MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_image =
                    new Image<Bgr, byte>(new Bitmap(temp));
                Emgu_image = Emgu_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.AllocKneedownImage(Emgu_image.Bytes);

            }

        }
        private void LoadSideBodyPositionValues(int uniqueKey)
        {
            string query = "select * from SideBodyPosition where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                SideBodyPosition SideBody_Position = new SideBodyPosition();
                SideBody_Position.SetAnkleLeftBeltPosition(new Point(int.Parse(row[3].ToString()),
                                                       int.Parse(row[4].ToString())));
                SideBody_Position.SetAnklePosition(new Point(int.Parse(row[1].ToString()),
                                                       int.Parse(row[2].ToString())));
                SideBody_Position.SetAnkleRightBeltPosition(new Point(int.Parse(row[5].ToString()),
                                                       int.Parse(row[6].ToString())));
                SideBody_Position.SetBenchmark1Position(new Point(int.Parse(row[7].ToString()),
                                                       int.Parse(row[8].ToString())));
                SideBody_Position.SetBenchmark2Position(new Point(int.Parse(row[9].ToString()),
                                                       int.Parse(row[10].ToString())));
                SideBody_Position.SetChinPosition(new Point(int.Parse(row[11].ToString()),
                                                       int.Parse(row[12].ToString())));
                SideBody_Position.SetEarPosition(new Point(int.Parse(row[13].ToString()),
                                                       int.Parse(row[14].ToString())));
                SideBody_Position.SetGlabellaPosition(new Point(int.Parse(row[15].ToString()),
                                                       int.Parse(row[16].ToString())));
                SideBody_Position.SetHipPosition(new Point(int.Parse(row[17].ToString()),
                                                       int.Parse(row[18].ToString())));
                SideBody_Position.SetKneeLeftBeltPosition(new Point(int.Parse(row[21].ToString()),
                                                       int.Parse(row[22].ToString())));
                SideBody_Position.SetKneePosition(new Point(int.Parse(row[19].ToString()),
                                                       int.Parse(row[20].ToString())));
                SideBody_Position.SetKneeRightBeltPosition(new Point(int.Parse(row[23].ToString()),
                                                       int.Parse(row[24].ToString())));
                SideBody_Position.SetLeftBeltPosition(new Point(int.Parse(row[25].ToString()),
                                                       int.Parse(row[26].ToString())));
                SideBody_Position.SetRightBeltPosition(new Point(int.Parse(row[27].ToString()),
                                                       int.Parse(row[28].ToString())));
                SideBody_Position.SetShoulderPosition(new Point(int.Parse(row[29].ToString()),
                                                       int.Parse(row[30].ToString())));
                m_JointEditDoc.SetSideBodyPosition(SideBody_Position);

                byte[] bytes = null;
                Image temp;
                bytes = Convert.FromBase64String(row["imagebytes"].ToString());
                MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_image =
                    new Image<Bgr, byte>(new Bitmap(temp));
                Emgu_image = Emgu_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.AllocSideImage(Emgu_image.Bytes);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            m_JointEditDoc.GetInitialScreen().Visible = true;
            m_JointEditDoc.GetInitialScreen().RefreshForms();
        }
        public void RefreshForms()
        {
            SearchById_Flag = false;
            SearchByIdAndName_Flag = false;
            SearchByName_Flag = false;

            checkBox1.Checked = false;
            dateTimePicker1.Enabled = false;
            dateTimePicker2.Enabled = false;

            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);
        }

        private void IDC_Search_Click(object sender, EventArgs e)
        {

            string query = "select uniqueid from patientdetails ";

            if (textBox1.Text != "") // search by ID
            {
                query = query + "where patientid like '" + textBox1.Text + "%'";
                SearchById_Flag = true;
            }
            if (textBox1.Text != "" && textBox2.Text != "") // search by ID and name
            {
                query = query + " and name like '" + textBox2.Text + "%'";
                SearchByIdAndName_Flag = true;
            }
            if (textBox1.Text == "" && textBox2.Text != "") // search by name
            {
                query = query + " where name like '" + textBox2.Text + "%'";
                SearchByName_Flag = true;
            }

            if (checkBox1.Checked == true) // search by date of creation
            {
                if (!SearchById_Flag && !SearchByIdAndName_Flag && !SearchByName_Flag)
                {
                    query = query + " where MeasurementTime between '" +
                        dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                        dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";

                }
                else
                {
                    query = query + " and MeasurementTime between '" +
                        dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                        dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                }

            }
            CalculateTotalPages(query);
            textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            SearchById_Flag = false; SearchByIdAndName_Flag = false; SearchByName_Flag = false;
            this.CurrentPageIndex = 1;
            query = string.Empty;
            query = GetCurrentQuery(this.CurrentPageIndex);

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;
            
        }
        public void LoadDataGridWithDefaultValues()
        {
            CalculateTotalPages("select uniqueid from PatientDetails");
            textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            string query = GetCurrentQuery(this.CurrentPageIndex);//"select patientid,name,MeasurementTime, uniqueid from patientdetails order by Measurementtime desc";

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;
            

            dataGridView1.EnableHeadersVisualStyles = false;

            dataGridView1.Columns[0].HeaderText = "Id";
            dataGridView1.Columns[0].Width = 261;

            dataGridView1.Columns[1].HeaderText = "Name";
            dataGridView1.Columns[1].Width = 261;

            dataGridView1.Columns[2].HeaderText = "Date Of Creation";
            dataGridView1.Columns[2].Width = 270;

            dataGridView1.Columns[3].HeaderText = "uniqueid";
            dataGridView1.Columns[3].Width = 1;
            //dataGridView1.Columns[3].Visible = true;

            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
                col.HeaderCell.Style.BackColor = Color.LightSteelBlue;

                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            }

            // dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.LightBlue;
            dataGridView1.RowsDefaultCellStyle.BackColor = Color.LightYellow;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
            dataGridView1.DefaultCellStyle.Font = new Font("Arial", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.None;

            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.PaleTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;

            dataGridView1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToResizeColumns = false;



        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

            // dateTimePicker1.Format = DateTimePickerFormat.Custom;
            // dateTimePicker1.CustomFormat = "yy-MM-dd";
            //     FromDateFlag = true;

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            // dateTimePicker2.Format = DateTimePickerFormat.Custom;
            // dateTimePicker2.CustomFormat = "yy-MM-dd";
            // ToDateFlag = true;

        }

        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {

            DateTime fromdate = DateTime.Parse(dateTimePicker1.Value.ToString());
            DateTime todate1 = DateTime.Parse(dateTimePicker2.Value.ToString());
            if (fromdate <= todate1)
            {
                TimeSpan daycount = todate1.Subtract(fromdate);
                int dacount1 = Convert.ToInt32(daycount.Days) + 1;
                //MessageBox.Show(Convert.ToString(dacount1));
            }
            else
            {
                MessageBox.Show("From Date Must be Less Than To Date");
            }
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker2.Enabled = false;

            }

        }

        private void dateTimePicker2_FormatChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        public void OpenResult_SizeChanged(object sender, EventArgs e)
        {
            dataGridView1.Width = 800;
            dataGridView1.Height = 486;
            dataGridView1.Left = (this.ClientSize.Width - dataGridView1.Width) / 2;        
                

            label1.Location = new Point(dataGridView1.Location.X, label1.Location.Y);
            textBox1.Location = new Point(dataGridView1.Location.X + 120, textBox1.Location.Y);
            checkBox1.Location = new Point(dataGridView1.Location.X , checkBox1.Location.Y);
            label4.Location = new Point(dataGridView1.Location.X , label4.Location.Y);
            dateTimePicker1.Location = new Point(dataGridView1.Location.X + 120, dateTimePicker1.Location.Y);

            
            //dataGridView1.Location = new Point(dataGridView1.Location.X + 120, dataGridView1.Location.Y - 4);
            button2.Location = new Point(dataGridView1.Location.X, dataGridView1.Location.Y + dataGridView1.Height + 10);
            button1.Location = new Point(dataGridView1.Location.X + dataGridView1.Width - button1.Width, dataGridView1.Location.Y + dataGridView1.Height + 10);

            textBox2.Location = new Point((dataGridView1.Location.X + dataGridView1.Width - textBox2.Width),
                textBox2.Location.Y);
            label2.Location = new Point(textBox2.Location.X - 100, label2.Location.Y);
            dateTimePicker2.Location = new Point((dataGridView1.Location.X + dataGridView1.Width - dateTimePicker2.Width),
               dateTimePicker2.Location.Y);
            label5.Location = new Point(dateTimePicker2.Location.X - 100, label5.Location.Y);
            IDC_Search.Location = new Point(dataGridView1.Location.X +dataGridView1.Width/2 - 30, IDC_Search.Location.Y);

            FirstPage.Location = new Point(button2.Location.X + 136, button2.Location.Y);
            NextPage.Location = new Point(FirstPage.Location.X + 100, FirstPage.Location.Y);
            textbox_PageNo.Location = new Point(NextPage.Location.X + 100, NextPage.Location.Y);
            PreviousPage.Location = new Point(textbox_PageNo.Location.X + 140, textbox_PageNo.Location.Y);
            LastPage.Location = new Point(PreviousPage.Location.X + 100, PreviousPage.Location.Y);

        }

        private void CalculateTotalPages(string query)
        {
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            
            int rowCount = 0;
            rowCount = dt.Rows.Count;
           
            TotalPage = rowCount / PgSize;
            // if any row left after calculated pages, add one more page 
            if (rowCount % PgSize > 0)
                TotalPage += 1;
        }
        private string GetCurrentQuery(int page)
        {
            string query = string.Empty;

            if (page == 1)
            {
                query = "select patientid,name,MeasurementTime,uniqueid from patientdetails ";

                if (textBox1.Text != "") // search by ID
                {
                    query = query + "where patientid like '" + textBox1.Text + "%'";
                    SearchById_Flag = true;
                }
                if (textBox1.Text != "" && textBox2.Text != "") // search by ID and name
                {
                    query = query + " and name like '" + textBox2.Text + "%'";
                    SearchByIdAndName_Flag = true;
                }
                if (textBox1.Text == "" && textBox2.Text != "") // search by name
                {
                    query = query + " where name like '" + textBox2.Text + "%'";
                    SearchByName_Flag = true;
                }

                if (checkBox1.Checked == true) // search by date of creation
                {
                    if (!SearchById_Flag && !SearchByIdAndName_Flag && !SearchByName_Flag)
                    {
                        query = query + " where MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";

                    }
                    else
                    {
                        query = query + " and MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                    }

                }
                query = query + " order by Measurementtime desc LIMIT " + PgSize;
                SearchById_Flag = false; SearchByIdAndName_Flag = false; SearchByName_Flag = false;
            }
            else
            {
                int PreviousPageOffSet = (page - 1) * PgSize;

                query = "Select" +
                    " patientid,name,MeasurementTime, uniqueid from" +
                    " patientdetails WHERE uniqueid NOT IN" +
                    " (Select uniqueid from patientdetails ";
                if (textBox1.Text != "") // search by ID
                {
                    query = query + "where patientid like '" + textBox1.Text + "%'";
                    SearchById_Flag = true;
                }
                if (textBox1.Text != "" && textBox2.Text != "") // search by ID and name
                {
                    query = query + " and name like '" + textBox2.Text + "%'";
                    SearchByIdAndName_Flag = true;
                }
                if (textBox1.Text == "" && textBox2.Text != "") // search by name
                {
                    query = query + " where name like '" + textBox2.Text + "%'";
                    SearchByName_Flag = true;
                }

                if (checkBox1.Checked == true) // search by date of creation
                {
                    if (!SearchById_Flag && !SearchByIdAndName_Flag && !SearchByName_Flag)
                    {
                        query = query + " where MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                    }
                    else
                    {
                        query = query + " and MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                    }

                }
                query = query + " order by Measurementtime desc LIMIT " + PreviousPageOffSet + ")";
                SearchById_Flag = false; SearchByIdAndName_Flag = false; SearchByName_Flag = false;

                if (textBox1.Text != "") // search by ID
                {
                    query = query + " and patientid like '" + textBox1.Text + "%'";
                }
                if (textBox2.Text != "") // search by name
                {
                    query = query + " and name like '" + textBox2.Text + "%'";
                }
                if (checkBox1.Checked == true) // search by date of creation
                {
                    query = query + " and MeasurementTime between '" +
                    dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                    dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                }

                query = query + " order by Measurementtime desc LIMIT " + PgSize;
            }

            return query;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void FirstPage_Click(object sender, EventArgs e)
        {
            this.CurrentPageIndex = 1;
            string query = GetCurrentQuery(this.CurrentPageIndex);

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;

            textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
        }

        private void NextPage_Click(object sender, EventArgs e)
        {
            if (this.CurrentPageIndex < this.TotalPage)
            {
                this.CurrentPageIndex++;
                string query = GetCurrentQuery(this.CurrentPageIndex);

                CDatabase database = new CDatabase();
                DataTable dt = new DataTable();
                dt = database.selectQuery(query);
                dataGridView1.DataSource = dt;
            }

            textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
        }

        private void PreviousPage_Click(object sender, EventArgs e)
        {
            if (this.CurrentPageIndex > 1)
            {
                this.CurrentPageIndex--;
                string query = GetCurrentQuery(this.CurrentPageIndex);

                CDatabase database = new CDatabase();
                DataTable dt = new DataTable();
                dt = database.selectQuery(query);
                dataGridView1.DataSource = dt;
            }
            textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
        }

        private void LastPage_Click(object sender, EventArgs e)
        {
            this.CurrentPageIndex = TotalPage;
            string query = GetCurrentQuery(this.CurrentPageIndex);

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;

            textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
        }

        private void OpenResult_Load(object sender, EventArgs e)
        {

        }
    }
}
