﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public class FrontBodyResultData
    {
        public bool m_bStanding;
        public int m_iRightKnee;
        public int m_iLeftKnee;
        public int m_iHip;
        public int m_iCenterBalance;
        public int m_iShoulderBalance;
        public int m_iEarBalance;
        public int m_iBodyCenter;
        public int m_iHeadCenter;
        public const int RESULT_ID_RIGHTKNEE = 1;
        public const int RESULTID_LEFTKNEE = 2;
        public const int RESULTID_HIP = 3;
        public const int RESULTID_CENTERBALANCE = 4;
        public const int RESULTID_SHOULDERBALANCE = 5;

        public const int RESULTID_EARBALANCE = 6;
        public const int RESULTID_BODYCENTER = 7;

        public const int RESULTID_HEADCENTER = 8;
        public const int RESULTID_SIDEBODYBALANCE = 9;
        public const int RESULTID_NECKFORWARDLEANING = 10;
        public const int RESULTID_POSTUREPATTERN_NORMAL = 11;
        public const int RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD = 12;
        public const int RESULTID_POSTUREPATTERN_STOOP = 13;
        public const int RESULTID_POSTUREPATTERN_BEND_BACKWARD = 14;
        public const int RESULTID_POSTUREPATTERN_FLATBACK = 15;

        public FrontBodyResultData()
        {
            m_bStanding = false;

            m_iRightKnee = 0;

            m_iLeftKnee = 0;

            m_iHip = 0;

            m_iCenterBalance = 0;

            m_iShoulderBalance = 0;

            m_iEarBalance = 0;

            m_iBodyCenter = 0;

            m_iHeadCenter = 0;

        }

        FrontBodyResultData(FrontBodyResultData rSrc)
        {

            m_bStanding = rSrc.m_bStanding;

            m_iRightKnee = rSrc.m_iRightKnee;

            m_iLeftKnee = rSrc.m_iLeftKnee;

            m_iHip = rSrc.m_iHip;

            m_iCenterBalance = rSrc.m_iCenterBalance;

            m_iShoulderBalance = rSrc.m_iShoulderBalance;

            m_iEarBalance = rSrc.m_iEarBalance;

            m_iBodyCenter = rSrc.m_iBodyCenter;

            m_iHeadCenter = rSrc.m_iHeadCenter;

        }

        ~FrontBodyResultData()
        {
        }



        public void Clear()
        {
            m_iRightKnee = -999;
            m_iLeftKnee = -999;
            m_iHip = -999;
            m_iCenterBalance = -999;
            m_iShoulderBalance = -999;
            m_iEarBalance = -999;
            m_iBodyCenter = -999;
            m_iHeadCenter = -999;
        }

        public void Reset()
        {
            m_iRightKnee = 0;
            m_iLeftKnee = 0;
            m_iHip = 0;
            m_iCenterBalance = 0;
            m_iShoulderBalance = 0;
            m_iEarBalance = 0;
            m_iBodyCenter = 0;
            m_iHeadCenter = 0;
        }

        public void Calc(int sex, int gene, FrontBodyAngle FrontBodyAngle, ResultActionScriptMgr ResultActionScriptMgr /*CResultPropArray &ResultPropArray*/ )
       {
            //FrontBodyResultData m_FrontBodyResultData = new FrontBodyResultData();
            m_bStanding = FrontBodyAngle.IsStanding();
            ResultActionScriptMgr.Execute(this, FrontBodyAngle);
        }

        public bool IsStanding()
        {
            return m_bStanding;
        }

        public int GetRightKnee()
        {
            return m_iRightKnee;
            
            //return 3;
        }

        public int GetLeftKnee()
        {
            return m_iLeftKnee;
            //return 3;
        }

        public int GetHip()
        {
            return m_iHip;
            //return 3;
        }

        public int GetCenterBalance()
        {
            return m_iCenterBalance;
            //return 3;
        }

        public int GetShoulderBal()
        {
             return m_iShoulderBalance;
            //return 3;

        }

        public int GetEarBal()
        {
             return m_iEarBalance;
            //return 3;
        }

        public int GetBodyCenter()
        {
            return m_iBodyCenter;
           // return 3;
        }

        public int GetHeadCenter()
        {
             return m_iHeadCenter;            
            //return 3;
        }

      public  void SetDataByResultID(int iResultID, int iValue)
        {
            switch (iResultID)
            {
                case RESULT_ID_RIGHTKNEE:
                    m_iRightKnee = iValue;
                    //MessageBox.Show("RESULT_ID_RIGHTKNEE ="+m_iRightKnee.ToString());
                    
                    break;
                case RESULTID_LEFTKNEE:
                    m_iLeftKnee = iValue;
                    break;
                case RESULTID_HIP:
                    m_iHip = iValue;
                    break;
                case RESULTID_CENTERBALANCE:
                    m_iCenterBalance = iValue;
                    break;
                case RESULTID_SHOULDERBALANCE:
                    m_iShoulderBalance = iValue;
                    break;
                case RESULTID_EARBALANCE:
                    m_iEarBalance = iValue;
                    break;
                case RESULTID_BODYCENTER:
                    m_iBodyCenter = iValue;
                    break;
                case RESULTID_HEADCENTER:
                    m_iHeadCenter = iValue;
                    break;
                case RESULTID_SIDEBODYBALANCE:
                case RESULTID_NECKFORWARDLEANING:
                case RESULTID_POSTUREPATTERN_NORMAL:
                case RESULTID_POSTUREPATTERN_STOOP_AND_BEND_BACKWARD:
                case RESULTID_POSTUREPATTERN_STOOP:
                case RESULTID_POSTUREPATTERN_BEND_BACKWARD:
                case RESULTID_POSTUREPATTERN_FLATBACK:
                default:
                    break;
            }
        }

    }
}
