﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class CTrainingSelectConditionArray
    {
        public CTrainingSelectCondition[] m_pTrainingSelectCondition;// = new CTrainingSelectCondition[23] ;

        public int Check(ref int iPriority, ResultData result)
        {
            int iMaxPriority = -1;
            int i = 0;
            while (m_pTrainingSelectCondition.Length > i)
            {
                
                if (m_pTrainingSelectCondition[i].m_iConditionID == Constants.TRAINING_SELECTCONDITIONID_NONE)
                {
                    break;
                }
                //int iPriority = 0;
                iPriority = 0;
                if (m_pTrainingSelectCondition[i].Check(ref iPriority, result) > 0)
                { // check - Meena
                    if (iPriority > iMaxPriority)
                    {
                        iMaxPriority = iPriority;
                    }
                }
                i++;
            }
            if (iMaxPriority >= 0)
                return 1;
            else
                return 0;

        }
    }
}
