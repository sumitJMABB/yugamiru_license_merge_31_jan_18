﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class IDD_MeasurementDlg : Form
    {
        PictureBox picturebox1 = new PictureBox();
        Bitmap bmBack1;
        //public Panel Panel_reference;
        JointEditDoc m_GetDocument;
        public IDD_MeasurementDlg(JointEditDoc GetDocument)
        {
            InitializeComponent();
            bmBack1 = Yugamiru.Properties.Resources.Mainpic2;

            picturebox1.Image = bmBack1;

            this.Controls.Add(picturebox1);

            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);


            switch (GetDocument.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                        IDC_BackBtn.Visible = true;
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        //IDC_BackBtn.Image.Dispose();
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                        IDC_BackBtn.Visible = false;

                        //IDC_NextBtn.Image.Dispose();
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_down;
                        IDC_NextBtn.Visible = true;
                    }
                    break;
                default:
                    break;
            }

            m_GetDocument = GetDocument;

            IDC_ID.MaxLength = 10;
            IDC_ID.Focus();
            IDC_Name.MaxLength = 20;

            DateTime tm = new DateTime();
            DateTime.Now.ToString();

            int y = tm.Year;
            IDC_COMBO_GENDER.Items.Add("-");
            if (GetDocument.GetLanguage() == "Japanese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if(GetDocument.GetLanguage() == "Chinese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if(GetDocument.GetLanguage() == "Korean")
            {
                IDC_COMBO_GENDER.Items.Add("남성");
                IDC_COMBO_GENDER.Items.Add("여성");
            }
            if(GetDocument.GetLanguage() == "English" ||GetDocument.GetLanguage() == "Thai")
            {
                IDC_COMBO_GENDER.Items.Add("Male");
                IDC_COMBO_GENDER.Items.Add("Female");
            }
           

            // •\Ž¦
            switch (GetDocument.GetMeasurementViewMode())
            {
                case Constants.MEASUREMENTVIEWMODE_INITIALIZE:
                    {
                        IDC_COMBO_DAY.SelectedIndex = 0;
                        IDC_COMBO_GENDER.SelectedIndex = 0;
                        IDC_COMBO_MONTH.SelectedIndex = 0;

                    }
                    break;
                case Constants.MEASUREMENTVIEWMODE_RETAIN:
                    {
                        IDC_ID.Text = GetDocument.GetDataID();
                        IDC_Name.Text = GetDocument.GetDataName();
                        string str = "-";
                        if (GetDocument.GetDataGender() == 1) str = "Male";
                        if (GetDocument.GetDataGender() == 2) str = "Female";

                        IDC_COMBO_GENDER.SelectedIndex = IDC_COMBO_GENDER.Items.IndexOf(str);

                        string year = string.Empty, month = string.Empty, day = string.Empty;
                        GetDocument.GetDataDoB(ref year, ref month, ref day);
                        IDC_BirthYear.Value = Convert.ToInt32(year);

                        IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(month);
                        IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_DAY.Items.IndexOf(day);

                        if (GetDocument.GetDataHeight() == 0)
                        {
                            str = "-";  // g’·
                        }
                        else
                        {
                            str = GetDocument.GetDataHeight().ToString();
                        }
                        IDC_Height.Value = Convert.ToInt32(str);
                    }
                    break;
                case Constants.MEASUREMENTVIEWMODE_NONE:
                default:
                    break;
            }

            CheckInputData();

        }
        public void Reload()
        {
            IDC_ID.Focus();
            IDC_ID.Text = m_GetDocument.GetDataID();
            IDC_Name.Text = m_GetDocument.GetDataName();
            string str = "-";
           // if (m_GetDocument.GetDataGender() == 1) str = "Male";
           // if (m_GetDocument.GetDataGender() == 2) str = "Female";

           // IDC_COMBO_GENDER.SelectedIndex = IDC_COMBO_GENDER.Items.IndexOf(str);

            string year = string.Empty, month = string.Empty, day = string.Empty;
            m_GetDocument.GetDataDoB(ref year, ref month, ref day);
            IDC_BirthYear.Value = Convert.ToInt32(year);

            IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(month);
            IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_DAY.Items.IndexOf(day);

            if (m_GetDocument.GetDataHeight() == 0)
            {
                str = "-";  // g’·
            }
            else
            {
                str = m_GetDocument.GetDataHeight().ToString();
            }
            IDC_Height.Value = Convert.ToInt32(str);
        }
        public void DisposeControls()
        {
            this.picturebox1.Image.Dispose();

            this.IDC_BackBtn.Image.Dispose();
            this.IDC_NextBtn.Image.Dispose();

            Yugamiru.Properties.Resources.gobackgreen_up.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_down.Dispose();
            Yugamiru.Properties.Resources.completegreen_down.Dispose();
            Yugamiru.Properties.Resources.Mainpic2.Dispose();


            this.Dispose();
            this.Close();
        }
        public bool CheckInputData()
        {
            if (IDC_BirthYear.Value.ToString() != null)
            {
                IDC_NextBtn.Visible = true;
            }
            else
            {
                IDC_NextBtn.Visible = false;

            }
            //Invalidate();

            return true;
        }


        private void IDD_MeasurementDlg_Load(object sender, EventArgs e)
        {
            // Added by sumit GSP-357, 346: --------START

            ReCheck:
            WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            if (s != WebComCation.LicenseStatus.Valid)
            {
                Application.Exit();
                ////DialogResult deci= MessageBox.Show("Do you want to provide a valid license?", "gsport", MessageBoxButtons.YesNo);

                ////if (deci == DialogResult.Yes)
                //{
                //    try
                //    {

                //        WebComCation.KeyValidator kv = new WebComCation.KeyValidator();
                //        kv.ShowDialog();
                //        goto ReCheck;
                //    }
                //    finally
                //    {

                //    }
                //}
                ////else
                ////  Environment.Exit(0);
            }


            //End  GSP-357, 346 Added By Sumit---------END
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        public void IDD_MeasurementDlg_SizeChanged(object sender, EventArgs e)
        {


            this.IDC_BackBtn.Size = new Size(112, 42);
            this.IDC_BackBtn.Location = new Point(350, 600);

            this.IDC_NextBtn.Size = new Size(112, 42);
            this.IDC_NextBtn.Location = new Point(800, 600);

            this.IDC_ID.Size = new Size(182, 47);
            this.IDC_ID.Location = new Point(445 + 10, 70 + 50 - 10);

            this.IDC_Name.Size = new Size(305, 47);
            this.IDC_Name.Location = new Point(445 + 10, 140 + 50 - 10);

            this.IDC_COMBO_GENDER.Size = new Size(102, 35);
            this.IDC_COMBO_GENDER.Location = new Point(445 + 10, 200 + 50 - 10);

            this.IDC_BirthYear.Size = new Size(77, 41);
            this.IDC_BirthYear.Location = new Point(445 + 10, 270 + 50 - 10);

            this.IDC_COMBO_MONTH.Size = new Size(70, 35);
            this.IDC_COMBO_MONTH.Location = new Point(580 + 10 - 8, 270 + 50 - 10);

            this.IDC_COMBO_DAY.Size = new Size(70, 35);
            this.IDC_COMBO_DAY.Location = new Point(700 + 10 - 8, 270 + 50 - 10);

            this.IDC_Height.Size = new Size(77, 41);
            this.IDC_Height.Location = new Point(445 + 10, 330 + 50 - 10);


            //--to centre the picture box while resizing the form
            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            //picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2;
            picturebox1.Top = 0;
            //--end

            IDC_ID.Left = (this.Width / 2 - IDC_ID.Left) + IDC_ID.Width + 50;
            IDC_Name.Left = (this.Width / 2 - IDC_Name.Left) + IDC_Name.Width - 74;
            IDC_COMBO_GENDER.Left = (this.Width / 2 - IDC_COMBO_GENDER.Left) + IDC_COMBO_GENDER.Width + 130;
            IDC_BirthYear.Left = (this.Width / 2 - IDC_BirthYear.Left) + IDC_BirthYear.Width + 150;
            IDC_COMBO_MONTH.Left = IDC_BirthYear.Left + IDC_COMBO_MONTH.Width + 60;
            IDC_COMBO_DAY.Left = IDC_COMBO_MONTH.Left + IDC_COMBO_DAY.Width + 50 + 4;
            IDC_Height.Left = this.Width / 2 - IDC_Height.Left + 230;

            IDC_BackBtn.Left = this.Width / 2 - IDC_BackBtn.Left;
            IDC_NextBtn.Left = IDC_BackBtn.Left + IDC_NextBtn.Width + 400;

        }

        private void IDC_Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void IDC_BackBtn_Click(object sender, EventArgs e)
        {
            

            DialogResult dialogResult = MessageBox.Show(
                /*"if return to title view, current data will be lost. return to title view OK?"*/
                Yugamiru.Properties.Resources.WARNING1, "Yugamiru", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                // CloseForm(EventArgs.Empty); // triggerring the event, to send it to form1 which is base form - step3
                //this.Close();
                m_GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
                this.Visible = false;
                m_GetDocument.GetInitialScreen().Visible = true;
                m_GetDocument.GetInitialScreen().RefreshForms();
                m_GetDocument.m_SideImageBytes = null;
                m_GetDocument.m_FrontKneedownImageBytes = null;
                m_GetDocument.m_FrontStandingImageBytes = null;
                IDC_ID.Clear();
                IDC_Name.Clear();
                IDC_COMBO_GENDER.SelectedIndex = 0;
                IDC_COMBO_MONTH.SelectedIndex = 0;
                
                IDC_COMBO_DAY.SelectedIndex = 0;
                
                //this.DisposeControls();

            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }


        }
        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_NextBtn_Click(object sender, EventArgs e)
        {
            m_GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
            string y = string.Empty, m = string.Empty, d = string.Empty;

            y = IDC_BirthYear.Value.ToString();
            m = IDC_COMBO_MONTH.GetItemText(this.IDC_COMBO_MONTH.SelectedItem).ToString();
            d = IDC_COMBO_DAY.GetItemText(IDC_COMBO_DAY.SelectedItem).ToString();
            /*         if (!CheckDate(y, m, d))
                     {
                         MessageBox("birthday is incorrect", "error", MB_ICONEXCLAMATION | MB_OK);
                         return;
                     }*/
            string strID = IDC_ID.Text;
            if (strID != "")
                m_GetDocument.SetDataID(strID);
            string strName = IDC_Name.Text;
            if (strName != "")
                m_GetDocument.SetDataName(strName);

            string gender = IDC_COMBO_GENDER.GetItemText(this.IDC_COMBO_GENDER.SelectedItem).ToString();
            
           
            if (gender == "-")
                m_GetDocument.SetDataGender(0);
            if (gender == "Male" || gender == "男" || gender == "남성")
                m_GetDocument.SetDataGender(1);
            if (gender == "Female" || gender == "女" || gender == "여성")
                m_GetDocument.SetDataGender(2);

            m_GetDocument.SetDataDoB(y, m, d);    // ¶”NŒŽ“ú
            float m_Height = (float)IDC_Height.Value;
            m_GetDocument.SetDataHeight(m_Height);  // g’·	*/

            switch (m_GetDocument.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        m_GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
                        this.Visible = false;
                        m_GetDocument.GetMeasurementStart().Visible = true;
                        m_GetDocument.GetMeasurementStart().RefreshForm();
                        m_GetDocument.GetMeasurementStart().reload();
                        /* CloseFormToStartNextScreen(EventArgs.Empty);
                         this.Close();
                         this.DisposeControls();*/
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        //GetDocument.ChangeToResultView();
                        this.Visible = false;
                        FuctiontoStartfromMeasurementViewtoResultView(EventArgs.Empty);
                        //this.Close();
                        //this.DisposeControls();
                    }
                    break;
                default:
                    break;
            }



        }
        public event EventHandler EventToStartNextScreen; // creating event handler - step1
        public void CloseFormToStartNextScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartNextScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventfromMeasurementViewtoResultView; // creating event handler - step1
        public void FuctiontoStartfromMeasurementViewtoResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventfromMeasurementViewtoResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDD_MeasurementDlg_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(bmBack1, (this.Width - bmBack1.Width) / 2, 0, bmBack1.Width, bmBack1.Height );
        }

        private void IDC_COMBO_MONTH_KeyDown(object sender, KeyEventArgs e)
        {
            // comboBox is readonly
            e.SuppressKeyPress = true;
        }

        private void IDC_COMBO_DAY_KeyDown(object sender, KeyEventArgs e)
        {
            // comboBox is readonly
            e.SuppressKeyPress = true;

        }
        public void RefreshForm()
        {
            IDC_ID.Focus();
            int Combo_Count = IDC_COMBO_GENDER.Items.Count;
            for (int i  = 0; i < Combo_Count; i++)
            {
                IDC_COMBO_GENDER.Items.RemoveAt(0);
            }
            
            IDC_COMBO_GENDER.Items.Add("-");
            if (m_GetDocument.GetLanguage() == "Japanese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if (m_GetDocument.GetLanguage() == "Chinese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if (m_GetDocument.GetLanguage() == "Korean")
            {
                IDC_COMBO_GENDER.Items.Add("남성");
                IDC_COMBO_GENDER.Items.Add("여성");
            }
            if (m_GetDocument.GetLanguage() == "English" || m_GetDocument.GetLanguage() == "Thai")
            {
                IDC_COMBO_GENDER.Items.Add("Male");
                IDC_COMBO_GENDER.Items.Add("Female");
            }
            
            //string gender = IDC_COMBO_GENDER.GetItemText(this.IDC_COMBO_GENDER.SelectedItem).ToString();

            if(m_GetDocument.GetDataGender() == 0)
            {
                IDC_COMBO_GENDER.SelectedIndex = 0;
            }
            else if(m_GetDocument.GetDataGender() == 1)
            {
                IDC_COMBO_GENDER.SelectedIndex = 1;
            }
            else if(m_GetDocument.GetDataGender() == 2)
            {
                IDC_COMBO_GENDER.SelectedIndex = 2;
            }
            picturebox1.Image = Yugamiru.Properties.Resources.Mainpic2;
            m_GetDocument.GetMainScreen().RefreshMenuStrip(false);
            switch (m_GetDocument.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                        IDC_BackBtn.Visible = true;
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        if(m_GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                        {
                            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                            IDC_BackBtn.Visible = true;
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                            IDC_NextBtn.Visible = true;
                        }
                        else
                        {
                            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                            IDC_BackBtn.Visible = false;
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_down;
                            IDC_NextBtn.Visible = true;

                        }
                        
                       
                    }
                    break;
                default:
                    break;
            }

        }

        private void IDC_ID_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void IDC_ID_TextChanged(object sender, EventArgs e)
       {
            

        }

        private void IDD_MeasurementDlg_KeyDown(object sender, KeyEventArgs e)
        {
           
        }
    }
}
