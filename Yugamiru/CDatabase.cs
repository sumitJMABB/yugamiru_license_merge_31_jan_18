﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    class CDatabase
    {
        private SQLiteConnection sqlite;

        public CDatabase(string FilePath)
        {
            SQLiteConnection.CreateFile(FilePath);
        }
        public CDatabase()
        {
            sqlite = new SQLiteConnection("Data Source="+Constants.db_file+ "/Yugamiru.sqlite");
        }

        public DataTable selectQuery(string query)
        {
            SQLiteDataAdapter ad;
            DataTable dt = new DataTable();

            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);                    
            }
            sqlite.Close();
            return dt;
        }
        public void ExecuteQuery(string txtQuery)
        {
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();
                cmd = sqlite.CreateCommand();
                cmd.CommandText = txtQuery;
                cmd.ExecuteNonQuery();
                sqlite.Close();
            }
            catch(SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
            
        }

    }
}
